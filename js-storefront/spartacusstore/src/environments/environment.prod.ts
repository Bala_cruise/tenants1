export const environment = {
  production: true,
  hostName: 'https://api.c050ygx6-obeikanin2-d1-public.model-t.cc.commerce.ondemand.com/',
  headerEndpoint: 'osanedcommercewebservices/v2/osaned/cms/pages?pageType=ContentPage&pageLabelOrId=headerpage',
  logoUrl: 'medias/site-logo.png?context=bWFzdGVyfGltYWdlc3wxMTEyOXxpbWFnZS9wbmd8aDNjL2g4Ny84ODAyOTQyMDU4NTI2L3NpdGUtbG9nby5wbmd8NzE5YjQwOTYxNzM0M2EzN2Y5MTViNDE2NzE3NGNhMDdjYTRkYzkzYjhjNWY1MWRhZWY1MDMxOWYwOGEyY2I4ZQ',
  homePageEndPoint: '/osanedcommercewebservices/v2/osaned/cms/pages?pageType=ContentPage&pageLabelOrId=homepage',
  oAuthAPI: 'authorizationserver/oauth/token',
  registerUserAPI: 'osanedcommercewebservices/v2/osaned/users',
  footerEndPoint: 'osanedcommercewebservices/v2/osaned/cms/pages?pageType=ContentPage&pageLabelOrId=footerPage',
  tokenEndpoint: 'authorizationserver/oauth/token',
  loginEndpoint: 'osanedcommercewebservices/v2/osaned/users/',
  pdpEndPoint: 'osanedcommercewebservices/v2/osaned/products/',
  usersEndpoint: 'osanedcommercewebservices/v2/osaned/users/',
  cartAPI: 'osanedcommercewebservices/v2/osaned/users/',
  addToCartAPI: 'osanedcommercewebservices/v2/osaned/users/bala@gmail.com/carts/00001008/entries',
  aboutUS: 'osanedcommercewebservices/v2/osaned/cms/pages?pageType=ContentPage&pageLabelOrId=aboutUsPage',
  adminDownloadApi: 'osanedcommercewebservices/v2/osaned/cms/pages?pageType=ContentPage&pageLabelOrId=hrservicepage',
  superAdmin: {
    hrPortalAPI: 'osanedcommercewebservices/v2/osaned/cms/pages?pageType=ContentPage&pageLabelOrId=hrportalpage',                 
    hrGetCustomerAPI: 'osanedcommercewebservices/v2/osaned/hr-portal/users/',
    hrCreateUserAPI: 'osanedcommercewebservices/v2/osaned/hr-portal/users/',
    financePortalAPI: 'osanedcommercewebservices/v2/osaned/cms/pages?pageType=ContentPage&pageLabelOrId=financeportalpage',
    financeGetCustomerAPI: 'osanedcommercewebservices/v2/osaned/finance-portal/users/',
    financeCreateUserAPI: 'osanedcommercewebservices/v2/osaned/finance-portal/users/',
    financeManagerAPI: 'osanedcommercewebservices/v2/osaned/cms/pages?pageType=ContentPage&pageLabelOrId=financemanagerservicepage'
   },
   verificationAdmin:{
    subEmpApi: 'osanedcommercewebservices/v2/osaned/finance-portal/users',
  }
};
