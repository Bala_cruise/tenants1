import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VerificationAdminTemplateComponent } from './verification-admin-template.component';

describe('VerificationAdminTemplateComponent', () => {
  let component: VerificationAdminTemplateComponent;
  let fixture: ComponentFixture<VerificationAdminTemplateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VerificationAdminTemplateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VerificationAdminTemplateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
