import { Component, OnInit } from '@angular/core';
import { CommonUtilityService } from '../../services/common-utility-service';
import { Router  } from '@angular/router';

@Component({
  selector: 'app-verification-admin-template',
  templateUrl: './verification-admin-template.component.html',
  styleUrls: ['./verification-admin-template.component.scss']
})
export class VerificationAdminTemplateComponent implements OnInit {

  userType: any

  constructor(
    private utilityService: CommonUtilityService,
    private router: Router) {

    this.userType = this.utilityService.getCookie('adminType')
    console.log("verification page",this.userType)
    if(this.userType === undefined || this.userType === '') {
      this.router.navigate(['/'])
    }
   }

  ngOnInit() {
  }

}
