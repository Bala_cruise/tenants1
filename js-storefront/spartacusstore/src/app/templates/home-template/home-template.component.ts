import { Component, OnInit } from '@angular/core';
import { CommonUtilityService } from '../../services/common-utility-service';
import { Router  } from '@angular/router';

@Component({
  selector: 'app-home-template',
  templateUrl: './home-template.component.html',
  styleUrls: ['./home-template.component.scss']
})
export class HomeTemplateComponent implements OnInit {

  userType: any

  constructor(
    private utilityService: CommonUtilityService,
    private router: Router
  ) { 
    this.userType = this.utilityService.getCookie('adminType')

    if(!!this.userType) {
      this.router.navigate(['/admin/hrms'])
    }
  }

  ngOnInit() {
  }

}
