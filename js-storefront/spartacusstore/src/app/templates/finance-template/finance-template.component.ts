import { Component, OnInit } from '@angular/core';
import { CommonUtilityService } from '../../services/common-utility-service';
import { Router  } from '@angular/router';

@Component({
  selector: 'app-finance-template',
  templateUrl: './finance-template.component.html',
  styleUrls: ['./finance-template.component.scss']
})
export class FinanceTemplateComponent implements OnInit {

  userType: any

  constructor(
    private utilityService: CommonUtilityService,
    private router: Router) {

    this.userType = this.utilityService.getCookie('adminType')

    if(this.userType === undefined || this.userType === '') {
      this.router.navigate(['/'])
    }
   }

  ngOnInit() {
  }

}
