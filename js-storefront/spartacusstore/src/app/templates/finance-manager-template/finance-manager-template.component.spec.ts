import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FinanceManagerTemplateComponent } from './finance-manager-template.component';

describe('FinanceManagerTemplateComponent', () => {
  let component: FinanceManagerTemplateComponent;
  let fixture: ComponentFixture<FinanceManagerTemplateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FinanceManagerTemplateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FinanceManagerTemplateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
