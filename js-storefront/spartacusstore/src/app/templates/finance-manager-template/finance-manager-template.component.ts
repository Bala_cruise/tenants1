import { Component, OnInit } from '@angular/core';
import { CommonUtilityService } from '../../services/common-utility-service';
import { Router  } from '@angular/router';

@Component({
  selector: 'app-finance-manager-template',
  templateUrl: './finance-manager-template.component.html',
  styleUrls: ['./finance-manager-template.component.scss']
})
export class FinanceManagerTemplateComponent implements OnInit {

  userType: any

  constructor(
    private utilityService: CommonUtilityService,
    private router: Router) {

    this.userType = this.utilityService.getCookie('adminType')
    console.log("verification page",this.userType)
    if(this.userType === undefined || this.userType === '') {
      this.router.navigate(['/'])
    }
   }

  ngOnInit() {
  }
}