import { Component, OnInit } from '@angular/core';
import { CommonUtilityService } from '../../services/common-utility-service';
import { Router  } from '@angular/router';

@Component({
  selector: 'app-hrms-template',
  templateUrl: './hrms-template.component.html',
  styleUrls: ['./hrms-template.component.scss']
})
export class HrmsTemplateComponent implements OnInit {

  userType: any

  constructor(
    private utilityService: CommonUtilityService,
    private router: Router) {

    this.userType = this.utilityService.getCookie('adminType')

    if(this.userType === undefined || this.userType === '') {
      this.router.navigate(['/'])
    }
   }

  ngOnInit() {
  }

}
