import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HrmsTemplateComponent } from './hrms-template.component';

describe('HrmsTemplateComponent', () => {
  let component: HrmsTemplateComponent;
  let fixture: ComponentFixture<HrmsTemplateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HrmsTemplateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HrmsTemplateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
