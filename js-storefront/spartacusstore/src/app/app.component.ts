import { Component, OnInit } from '@angular/core';
import { CommonUtilityService } from './services/common-utility-service'
import { LoginService } from './services/login.service'
import { Router, NavigationStart, NavigationEnd } from '@angular/router';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit  {
  title = 'ng-Freelance';
  userContext: any

  constructor(
    private utilityService: CommonUtilityService,
    private loginService: LoginService,
    private router: Router) {
    this.utilityService.getAuth();
    this.loginService.userContext.subscribe(context => {
      this.userContext = context;
    });
  }

  ngOnInit() {
    if(this.utilityService.getCookie('isAuthenticated')) {
      this.userContext.displayUID = this.utilityService.getCookie('userName')
      this.userContext.displayName = this.utilityService.getCookie('displayUid')
      this.userContext.isAuthenticated = this.utilityService.getCookie('isAuthenticated')

      this.loginService.changeUserContext(this.userContext)
    }

    this.router.events.subscribe(event => {
      // Scroll to top if accessing a page, not via browser history stack
      if (event instanceof NavigationEnd) {
        window.scrollTo(0, 0);
      }
    });
  }
}
