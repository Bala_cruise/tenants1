import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminNavMoleculeComponent } from './admin-nav-molecule.component';

describe('AdminNavMoleculeComponent', () => {
  let component: AdminNavMoleculeComponent;
  let fixture: ComponentFixture<AdminNavMoleculeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminNavMoleculeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminNavMoleculeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
