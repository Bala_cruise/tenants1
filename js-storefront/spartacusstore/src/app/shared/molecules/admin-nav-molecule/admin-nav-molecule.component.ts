import { Component, OnInit, Input } from '@angular/core';
import { environment } from '../../../../environments/environment';
import { AdminService, ADMIN_NAV_LINKS} from '../../../services/admin.service'

@Component({
  selector: 'app-admin-nav-molecule',
  templateUrl: './admin-nav-molecule.component.html',
  styleUrls: ['./admin-nav-molecule.component.scss']
})
export class AdminNavMoleculeComponent implements OnInit {

  @Input() userType: any
  logoUrl: any = environment.logoUrl;
  hostName: string = environment.hostName;
  adminNavLinks: any

  constructor(private adminService: AdminService) {
    this.adminNavLinks = ADMIN_NAV_LINKS
   }

  ngOnInit() {
    console.log("usertypeeee",this.userType)
  }

 
}
