import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormGroup,
  Validators,
  FormControl,
} from '@angular/forms';
import {
  CheckoutService,
  CARD_TYPES,
  MONTH_LIST,
  ACCEPT_TERMS_MSG,
} from '../../../services/checkout.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute , Router} from '@angular/router';
import { CommonUtilityService } from '../../../services/common-utility-service';
@Component({
  selector: 'app-payment-molecule',
  templateUrl: './payment-molecule.component.html',
  styleUrls: ['./payment-molecule.component.scss'],
})
export class PaymentMoleculeComponent implements OnInit {
  paymentDetails: any;
  defaultPayment: boolean;
  paymentForm: FormGroup;
  checkoutBreadcrumb: any;
  termsAndConditions: any = false;
  validationMessages = {
    cardType: [{ type: 'required', message: 'Please select any option' }],
    name: [
      { type: 'required', message: 'name is required' },
      { type: 'name', message: 'Enter a valid name' },
    ],
    cardNumber: [
      { type: 'required', message: 'cardNumber is required' },
      { type: 'pattern', message: 'Please enter a valid card number' },
    ],
    cvvNumber: [
      { type: 'required', message: 'cvvNumber is required' },
      { type: 'pattern', message: 'Please enter a valid cvv number' },
    ],
    message: [{ type: 'required', message: 'Please enter some message' }],
    expMonth: [{ type: 'required', message: 'Please select any option' }],
    expYear: [{ type: 'required', message: 'Please select any option' }],
  };

  selectedValue: string;
  cardType: any = CARD_TYPES;
  setAsDefault: any = false;
  expMonth: any = MONTH_LIST;
  expYear: any;

  constructor(
    private fb: FormBuilder,
    private checkoutService: CheckoutService,
    private matSnack: MatSnackBar,
    private route: ActivatedRoute,
    private router: Router,
    private utilityService: CommonUtilityService,
  ) {
    this.checkoutService.paymentContext.subscribe(context => {
      if (this.paymentForm !== undefined && this.paymentForm.valid) {
        this.checkoutService.setPaymentDetails = this.paymentForm;
        this.paymentDetails = context;
        this.defaultPayment = this.checkoutService.getDefaultPayment;
        this.paymentDetails = this.checkoutService.getPaymentDetails;
      }
    });

    this.paymentDetails = this.checkoutService.getPaymentDetails;
    this.defaultPayment = this.checkoutService.getDefaultPayment;
  }

  ngOnInit() {
    let year = new Date().getFullYear();
    let range = [];

    for (let i = 0; i < 10; i++) {
      range.push(parseInt(String(year + i)));
      this.expYear = range;
    }

    const CARD_PATTERN = '^[0-9]{16}$';
    const CVV_PATTERN = '^[0-9]{3}$';

    this.paymentForm = this.fb.group({
      cardType: [null, Validators.compose([Validators.required])],
      name: [null, Validators.compose([Validators.required])],
      cardNumber: [
        null,
        Validators.compose([
          Validators.required,
          Validators.pattern(CARD_PATTERN),
        ]),
      ],
      cvvNumber: [
        null,
        Validators.compose([
          Validators.required,
          Validators.pattern(CVV_PATTERN),
        ]),
      ],
      expMonth: [null, Validators.compose([Validators.required])],
      expYear: [null, Validators.compose([Validators.required])],
    });
  }

  setTerms(e) {
    this.termsAndConditions = e.checked;
  }

  onSubmitPaymentDetails(data) {
    if (this.termsAndConditions) {
      this.checkoutService.placeOrder(this.termsAndConditions).subscribe(
        (res: any) => {
          this.utilityService.setCookie('orderId', res.code);
          this.router.navigateByUrl('/order-confirmation');
          console.log(res);
        },
        err => {}
      );
    } else {
      this.matSnack.open(ACCEPT_TERMS_MSG, 'close', {
        duration: 5000,    verticalPosition: 'top'
      });
    }
  }
}
