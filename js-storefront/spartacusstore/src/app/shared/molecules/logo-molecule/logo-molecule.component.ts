import { Component, OnInit, Input } from '@angular/core';
import { environment } from '../../../../environments/environment';

@Component({
  selector: 'app-logo-molecule',
  templateUrl: './logo-molecule.component.html',
  styleUrls: ['./logo-molecule.component.scss']
})
export class LogoMoleculeComponent implements OnInit {

  @Input() displayType: string;
  @Input() logoUrl: string;

  hostName: string = environment.hostName;
  constructor() { }

  ngOnInit() {
  }

}
