import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LogoMoleculeComponent } from './logo-molecule.component';

describe('LogoMoleculeComponent', () => {
  let component: LogoMoleculeComponent;
  let fixture: ComponentFixture<LogoMoleculeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LogoMoleculeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LogoMoleculeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
