import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormGroup,
  Validators,
  FormControl,
} from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';

import { 
  AdminService,
  GENDERS,
  COUNTRIES,
  MARITAL_STATUS,
  RELIGIOUS,
  VALIDATION_MESSAGES } from '../../../services/admin.service'

@Component({
  selector: 'app-hrms-form-molecule',
  templateUrl: './hrms-form-molecule.component.html',
  styleUrls: ['./hrms-form-molecule.component.scss']
})
export class HrmsFormMoleculeComponent implements OnInit {

  genders: any
  countries: any
  maritalStatus: any
  religions: any
  personalInfo:any
  contactInfo: any
  employmentInfo: any
  contractDetails: any
  bankDetails: any
  validationMessages = VALIDATION_MESSAGES

  constructor(
    private fb: FormBuilder,
    private adminService: AdminService,
    private matSnack: MatSnackBar,
    ) { 
    this.genders = GENDERS
    this.countries = COUNTRIES
    this.maritalStatus = MARITAL_STATUS
    this.religions = RELIGIOUS
  }

  ngOnInit() {
    this.personalInfo = this.fb.group({
      fullName: [null, Validators.compose([Validators.required])],
      Fathername: [null, Validators.compose([Validators.required])],
      gender: [null, Validators.compose([Validators.required])],
      IqamaNo: [null, Validators.compose([Validators.required])],
      DOB: [null, Validators.compose([Validators.required])],
      religion: [null, Validators.compose([Validators.required])],
      maritalStatus: [null, Validators.compose([Validators.required])],
      nationality: [null, Validators.compose([Validators.required])]
    });

    this.contactInfo = this.fb.group({
      phoneNo: [null, Validators.compose([Validators.required])],
      email: [null, Validators.compose([Validators.required, Validators.email])]
    });

    this.employmentInfo = this.fb.group({
      empNo: [null, Validators.compose([Validators.required])],
      joiningDate: [null, Validators.compose([Validators.required])],
      jobTitle: [null, Validators.compose([Validators.required])],
      grade: [null, Validators.compose([Validators.required])],
      department: [null, Validators.compose([Validators.required])],
      division: [null, Validators.compose([Validators.required])],
      supEmpId: [null, Validators.compose([Validators.required])],
      location: [null, Validators.compose([Validators.required])],
      iqamaProfession: [null, Validators.compose([Validators.required])]
    });

    this.contractDetails = this.fb.group({
      basicSalary: [null, Validators.compose([Validators.required])],
      contractStatus: [null, Validators.compose([Validators.required])]
    });

    this.bankDetails = this.fb.group({
      bankName: [null, Validators.compose([Validators.required])],
      bankCode: [null, Validators.compose([Validators.required])],
      IBAN: [null, Validators.compose([Validators.required])]
    });
  }

  changeAccordian() {
    
  }

  onSubmitForm = () => {

    if(
      this.contactInfo.valid &&
      this.employmentInfo.valid &&
      this.personalInfo.valid &&
      this.bankDetails.valid &&
      this.contractDetails.valid) {
      const request = {
        'personalInfo' :
            {
                'firstName' : this.personalInfo.value.fullName,
                'lastName' : this.personalInfo.value.fullName,
                'fatherName': this.personalInfo.value.Fathername,
                'gender' : this.personalInfo.value.gender,
                'iqamaNo' : this.personalInfo.value.IqamaNo,
                'nationality': this.personalInfo.value.nationality,
                'dateofBirth': `${this.personalInfo.value.DOB.getDate()}/${this.personalInfo.value.DOB.getMonth()}/${this.personalInfo.value.DOB.getFullYear()}`,
                'maritalStatus': this.personalInfo.value.maritalStatus,
                'religion': this.personalInfo.value.religion
            },
    
        'employmentInfo' :
            {
                'employeeNumber': this.employmentInfo.value.empNo,
                'joiningDate': `${this.employmentInfo.value.joiningDate.getDate()}/${this.employmentInfo.value.joiningDate.getMonth()}/${this.employmentInfo.value.joiningDate.getFullYear()}`,
                'professionOnIqama': this.employmentInfo.value.iqamaProfession,
                'jobTilte': this.employmentInfo.value.jobTitle,
                'grade': this.employmentInfo.value.grade,
                'department': this.employmentInfo.value.department,
                'division': this.employmentInfo.value.division,
                'supervisorEmpID': this.employmentInfo.value.supEmpId,
                'location': this.employmentInfo.value.location
            },
        'contractDetails' :
            { 
                'basicSalary': this.contractDetails.value.basicSalary,
                'contractStatus': this.contractDetails.value.contractStatus
            },
        
        'contact' : 
            {
                'mobileNumber': this.contactInfo.value.phoneNo,
                'emailAddress': this.contactInfo.value.email
            },
    
        'bankDetails' :
            {
            'bankName': this.bankDetails.value.bankName,
            'bankCode': this.bankDetails.value.bankCode,
            'iBAN': this.bankDetails.value.IBAN
            }
    }

    this.adminService.submitForm(request).subscribe((res: any) => {
      console.log(res)
      if(res.hrMasterDataSentToCPI) {
        this.matSnack.open('Form submitted successfully', 'close', {
          duration: 5000,    verticalPosition: 'top'
        });
      } else {
        this.matSnack.open('Please try after sometime', 'close', {
          duration: 5000,    verticalPosition: 'top'
        });
      }
    },
    err => {
      this.matSnack.open('Please try after sometime', 'close', {
        duration: 5000,    verticalPosition: 'top'
      });
    })

    }
  }
}