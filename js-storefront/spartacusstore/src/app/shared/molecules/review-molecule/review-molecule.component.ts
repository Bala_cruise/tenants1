import { Component, OnInit } from '@angular/core';
import { CheckoutService } from '../../../services/checkout.service'

@Component({
  selector: 'app-review-molecule',
  templateUrl: './review-molecule.component.html',
  styleUrls: ['./review-molecule.component.scss']
})
export class ReviewMoleculeComponent implements OnInit {

  checkoutBreadcrumb: any;

  constructor(private checkoutService: CheckoutService) {
     }

  ngOnInit() {
  }

}
