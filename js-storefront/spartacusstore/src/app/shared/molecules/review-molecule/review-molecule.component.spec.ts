import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReviewMoleculeComponent } from './review-molecule.component';

describe('ReviewMoleculeComponent', () => {
  let component: ReviewMoleculeComponent;
  let fixture: ComponentFixture<ReviewMoleculeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReviewMoleculeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReviewMoleculeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
