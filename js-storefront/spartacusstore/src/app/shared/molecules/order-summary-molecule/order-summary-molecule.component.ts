import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-order-summary-molecule',
  templateUrl: './order-summary-molecule.component.html',
  styleUrls: ['./order-summary-molecule.component.scss']
})
export class OrderSummaryMoleculeComponent implements OnInit {

  @Input() totalPrice: any;
  @Input() totalTax: any;
  @Input() isCart: any

  constructor() { }

  ngOnInit() {
  }

}
