import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HeaderNavMoleculeComponent } from './header-nav-molecule.component';

describe('HeaderNavMoleculeComponent', () => {
  let component: HeaderNavMoleculeComponent;
  let fixture: ComponentFixture<HeaderNavMoleculeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HeaderNavMoleculeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderNavMoleculeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
