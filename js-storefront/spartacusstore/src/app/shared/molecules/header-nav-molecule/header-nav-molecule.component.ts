import { Component, OnInit, Input } from '@angular/core';
import { CommonUtilityService } from '../../../services/common-utility-service';
import { EmitterService } from '../../../services/emitter.service';
@Component({
  selector: 'cx-header-nav-molecule',
  templateUrl: './header-nav-molecule.component.html',
  styleUrls: ['./header-nav-molecule.component.css']
})
export class HeaderNavMoleculeComponent implements OnInit {

  @Input() headerNavLinks: String[]
  @Input() logoUrl: String
  isSuperAdmin: any = false;
  constructor(private utilityService: CommonUtilityService, private emitter: EmitterService) {
    this.emitter.superAdminSource.subscribe((data) => {
      if (data) {
        this.isSuperAdmin = this.utilityService.getCookie('isSuperAdmin');
      }
    })
  }
  ngOnInit() {
    this.isSuperAdmin = this.utilityService.getCookie('isSuperAdmin');
  }

  closeNav = () => {
    document.getElementById("mySidenav").style.width = "0";
  }

  openNav = () => {
    document.getElementById("mySidenav").style.width = "150px";
  }
}
