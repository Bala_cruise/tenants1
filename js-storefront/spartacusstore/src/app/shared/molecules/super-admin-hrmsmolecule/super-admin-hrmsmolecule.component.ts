import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { CommonUtilityService } from '../../../services/common-utility-service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';

@Component({
  selector: 'app-super-admin-hrmsmolecule',
  templateUrl: './super-admin-hrmsmolecule.component.html',
  styleUrls: ['./super-admin-hrmsmolecule.component.scss']
})
export class SuperAdminHRMSMoleculeComponent implements OnInit {
  createUserForm: FormGroup;
  hrPortalData: any;
  subCustomersData: any;
  hostName = environment.hostName;
  disableCreateUserForm = false;
  validationMessages = {
    name: [{ type: 'required', message: 'Full name is required' }],
    email: [
      { type: 'required', message: 'Email is required' },
      { type: 'email', message: 'Enter a valid email' }
    ],
    employeeId: [
      { type: 'required', message: 'Employee ID is required' },
      { type: 'pattern', message: 'Please enter a valid Employee ID' }
    ],
    iqamaId: [
      { type: 'required', message: 'Iqama Id is required' },
      { type: 'pattern', message: 'Please enter a valid Iqama Id' }
    ],
    customerRole: [{ type: 'required', message: 'Please select any option' }]
  };
  errMsg = 'Sorry ! Please try again later';
  succMsg = 'User Created successfully !';
  options: any;
  constructor(private fb: FormBuilder, private router: Router,
              private utilService: CommonUtilityService, private matSnack: MatSnackBar) {
                const hrUrl = environment.superAdmin.hrPortalAPI;
                const httpHeaders = new HttpHeaders()
                .set('Content-Type', 'application/json')
                .set('Authorization', `Bearer ${this.utilService.getCookie('AuthToken')}`);
                this.options = {
                  headers: httpHeaders
                };
                this.utilService.getRequest(hrUrl, this.options).subscribe((data: any) => {
                  this.hrPortalData = data.contentSlots.contentSlot.components.component;
                  console.log('hrPortalData', this.hrPortalData);
                });
                this.getSubCustomers();
              }

  ngOnInit() {
    const numberPattern = '/^-?(0|[1-9]\d*)?$/';
    this.createUserForm = this.fb.group({
      name: [null, Validators.required],
      emailId: [null, Validators.compose([
        Validators.required,
        Validators.email
      ])],
      employeeId: [null, Validators.compose([
        Validators.required,
       // Validators.pattern(numberPattern)
      ])],
      iqamaId: [null, Validators.compose([
        Validators.required,
       // Validators.pattern(numberPattern)
      ])],
      customerRole: [null, Validators.required]
    });
  }
 getSubCustomers() {
  const customerUrl = environment.superAdmin.hrGetCustomerAPI + this.utilService.getCookie('displayUid') + '/customerInfo';
  this.utilService.getRequest(customerUrl, this.options).subscribe((data: any) => {
    if (data.subEmployeesInfo && data.subEmployeesInfo.subEmployeesInfoList) {
      this.subCustomersData = data.subEmployeesInfo.subEmployeesInfoList;
      this.subCustomersData.length > 1 ? this.disableCreateUserForm = true: this.disableCreateUserForm = false;
      console.log('customerData', this.subCustomersData);
    }
  });
 }
  createAdmin(user) {
    const { name, emailId, employeeId, iqamaId, customerRole } = user;
    const url = environment.superAdmin.hrCreateUserAPI + this.utilService.getCookie('displayUid') + '/register';
    const body = {
      emailId,
      name,
      employeeId,
      iqamaId,
      customerRole
    };
    const httpHeaders = new HttpHeaders()
    .set('Content-Type', 'application/x-www-form-urlencoded;')
    .set('Authorization', `Bearer ${this.utilService.getCookie('AuthToken')}`);
    const options = {
      headers: httpHeaders
    };
    const request: HttpParams = this.utilService.buildQueryParams(body);
    this.utilService.postRequest(url, request.toString(), options).subscribe((res) => {
      this.getSubCustomers();
      this.matSnack.open(this.succMsg, 'close', {
        duration: 5000,   verticalPosition: 'top'
      });
    }, (err) => {
      this.matSnack.open(this.errMsg, 'close', {
        duration: 5000,   verticalPosition: 'top'
      });
    });
  }
  retrigger(emailId) {
    const url = environment.superAdmin.hrCreateUserAPI + this.utilService.getCookie('displayUid') + '/retriggerEmail';
    const body = {emailId};
    const httpHeaders = new HttpHeaders()
    .set('Content-Type', 'application/x-www-form-urlencoded;')
    .set('Authorization', `Bearer ${this.utilService.getCookie('AuthToken')}`);
    const options = {
      headers: httpHeaders
    };
    const request: HttpParams = this.utilService.buildQueryParams(body);
    this.utilService.postRequest(url, request.toString(), options).subscribe((res) => {
      this.matSnack.open('Mail has been triggerred successfully', 'close', {
        duration: 5000,   verticalPosition: 'top'
      });
    }, (err) => {
      this.matSnack.open(this.errMsg, 'close', {
        duration: 5000,   verticalPosition: 'top'
      });
    });
  }
}
