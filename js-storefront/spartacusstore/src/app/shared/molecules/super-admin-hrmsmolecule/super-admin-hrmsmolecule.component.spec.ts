import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SuperAdminHRMSMoleculeComponent } from './super-admin-hrmsmolecule.component';

describe('SuperAdminHRMSMoleculeComponent', () => {
  let component: SuperAdminHRMSMoleculeComponent;
  let fixture: ComponentFixture<SuperAdminHRMSMoleculeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SuperAdminHRMSMoleculeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SuperAdminHRMSMoleculeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
