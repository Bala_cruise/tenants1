import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoaderMoleculeComponent } from './loader-molecule.component';

describe('LoaderMoleculeComponent', () => {
  let component: LoaderMoleculeComponent;
  let fixture: ComponentFixture<LoaderMoleculeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoaderMoleculeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoaderMoleculeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
