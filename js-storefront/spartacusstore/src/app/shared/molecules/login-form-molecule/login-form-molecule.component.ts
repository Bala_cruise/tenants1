import { Component, OnInit, Inject } from '@angular/core';
import {
  FormBuilder,
  FormGroup,
  Validators,
  FormControl,
} from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { Router, ActivatedRoute  } from '@angular/router';
import { environment } from '../../../../environments/environment';
import { PdpService } from '../../../services/pdp.service'
import {
  LoginService,
  INVALID_CREDENTIAL,
  INVALID_OTP,
  EMAIL_LINK_EXPIRED,
  USER_NOT_FOUND } from '../../../services/login.service';
import { CommonUtilityService } from '../../../services/common-utility-service';
import { EmitterService } from '../../../services/emitter.service';
@Component({
  selector: 'app-login-form-molecule',
  templateUrl: './login-form-molecule.component.html',
  styleUrls: ['./login-form-molecule.component.scss'],
})
export class LoginFormMoleculeComponent implements OnInit {

  userContext: any;
  loginError: boolean = false;
  userLoginForm: FormGroup;
  userOTPForm: FormGroup;
  userEmailForm: FormGroup;
  returnUrl: any;
  validationMessages = {
    email: [
      { type: 'required', message: 'Email is required' },
      { type: 'email', message: 'Enter a valid email' },
    ],
    password: [{ type: 'required', message: 'Password is required' }],
    otp: [{type: 'required', message: 'Otp is required' }],
    message: [{ type: 'required', message: 'Please enter some message' }],
  };
  userName: any = "";
  userToken: any;
  buttonType: string;
  otp: any;
  loginForm: FormGroup = this.userEmailForm;
  isLoggedIn: boolean = false;

  constructor(
    private fb: FormBuilder,
    private loginService: LoginService,
    private router: Router, private route: ActivatedRoute ,
    private utilityService: CommonUtilityService,
    private pdpService: PdpService,
    private matSnack: MatSnackBar,
    public dialog: MatDialog,
    private emitter: EmitterService
  ) {
    this.loginService.userContext.subscribe(context => {
      this.userContext = context;
    });
  }

  ngOnInit() {
    this.returnUrl = this.route.snapshot.queryParams.returnUrl || '/';
    this.userLoginForm = this.fb.group({
      password: [null, Validators.compose([Validators.required])],
    });
    this.userOTPForm = this.fb.group({
      email: [
        null,
        Validators.compose([Validators.required, Validators.email]),
      ],
      otp: [null, Validators.compose([Validators.required])],
    });
    this.userEmailForm = this.fb.group({
      email: [
        null,
        Validators.compose([Validators.required, Validators.email]),
      ]
    });

    this.loginForm = this.userEmailForm;
  }

  onSubmitLoginDetails(userData, type: any) {
    if(!type) {
        if (!!userData && !!userData.email) {
          this.loginService.fetchToken(userData).subscribe((res: any) => {
            if (!!res && res.access_token) {
              const token = res.access_token
              const userId = userData.email
              this.userToken = token;
              this.fetchCustomerDetails(token, userId)
            } else {
              this.showErrorMsg(INVALID_CREDENTIAL)
            }
          },
          err => {
            this.showErrorMsg(INVALID_CREDENTIAL)
          });
      }
    } else if(type === "password") {
          userData.email = this.userName
          this.loginService.userLogin(userData).subscribe((res: any) => {
            this.isLoggedIn = true;
            this.fetchCustomerDetails(this.userToken, this.userName)
          },
          err => {
            if(err && err.error && err.error.error === 'invalid_grant') {
              this.showErrorMsg(INVALID_CREDENTIAL)
            }
          })
    } else if (type === 'otp') {
      this.loginService.activateProfile(this.userToken, this.userName, userData.otp).subscribe((res: any) => {
        if(res.active) {
          
          this.fetchCustomerDetails(this.userToken, this.userName)

        } else {

          this.showErrorMsg(EMAIL_LINK_EXPIRED)

        }
      },
      err => {

        if(err && err.error && err.error.errors[0].message === EMAIL_LINK_EXPIRED){

          this.showErrorMsg(EMAIL_LINK_EXPIRED)

        } else if (err && err.error && err.error.errors[0].message === 'Invalid OTP') {

          this.showErrorMsg(INVALID_OTP)

        } else {

          this.showErrorMsg(INVALID_OTP)

        }
        
      })
    }
  }

  showErrorMsg = (errMsg) => {
    this.matSnack.open(errMsg, 'close', {
      duration: 5000,    verticalPosition: 'top'
    });
  }

  openOtpDialog(token, userId): void {
    const dialogRef = this.dialog.open(OtpLoginMolecule, {
      width: '250px',
      data: {otp: ''}
    });

    dialogRef.afterClosed().subscribe(result => {
      this.otp = result;

      this.loginService.activateProfile(token, userId, this.otp).subscribe((res: any) => {
        if(res.active) {

          this.fetchCustomerDetails(token, userId)

        } else {

          this.showErrorMsg(EMAIL_LINK_EXPIRED)

        }
      },
      err => {

        if(err && err.error && err.error.errors[0].message === EMAIL_LINK_EXPIRED){

          this.showErrorMsg(EMAIL_LINK_EXPIRED)

        } else if (err && err.error && err.error.errors[0].message === 'Invalid OTP') {

          this.showErrorMsg(INVALID_OTP)

        } else {

          this.showErrorMsg(INVALID_OTP)

        }
        
      })
    });
  }

  fetchCustomerDetails = (token, userId) => {

    this.loginService.fetchUserDetails(token, userId).subscribe((res: any) => {
      if(res.active) {
        this.userName = userId;
        this.loginForm = this.userLoginForm;
        this.buttonType = 'password';
      } else {
        this.userName = userId;
        this.buttonType = 'otp';
        this.loginForm = this.userOTPForm;
      }

      if(this.isLoggedIn) {
        this.userContext.displayUID = res.displayUid;
        this.userContext.displayName = res.firstName;
        this.userContext.isAuthenticated = true;

        this.utilityService.setCookie('userName', this.userContext.displayName);
        this.utilityService.setCookie('displayUid', this.userContext.displayUID);
        this.utilityService.setCookie('isAuthenticated', true);

        this.loginService.changeUserContext(this.userContext);

        if(res && res.customerRole === 'SuperAdmin') {
          this.utilityService.removeCookie('adminType')
          this.fetchOrderDetails(token, userId)
        } else if(res && res.customerRole === 'HRAdmin') {
          this.utilityService.setCookie('adminType', res.customerRole);
          this.router.navigate(['/admin/hrms'])
        }else if(res && res.customerRole === 'Admin') {
          this.utilityService.setCookie('adminType', res.customerRole);
          this.router.navigate(['/admin/hrms'])
        }
        else if(res && res.customerRole === 'VerificationUser') {
        this.utilityService.setCookie('adminType', res.customerRole);
        this.router.navigate(['/verification-admin'])
        }
        else if(res && res.customerRole === 'FinanceManager') {
          this.utilityService.setCookie('adminType', res.customerRole);
          this.utilityService.setCookie('customerList', JSON.stringify(res.subEmployeesInfo.subEmployeesInfoList));
          this.router.navigate(['/financeManager'])
        }
       else {
          this.router.navigate(['/'])
        }
      }

    },
    err => {
      //this.router.navigate(['/'])
      this.showErrorMsg(USER_NOT_FOUND)
    })
  }

  fetchOrderDetails = (token, userId) => {
    this.loginService.fetchOrderDetails(token, userId).subscribe((res: any) => {
      if(res && res.pagination && res.pagination.totalResults === 0) {

        // should redirect to pdp
        this.router.navigateByUrl(this.returnUrl);
      } else {
        this.utilityService.setCookie('isSuperAdmin', true);
        this.utilityService.setCookie('superAdminOrders', JSON.stringify(res.orders));
        this.emitter.superAdminSource.next(true);
        // should redirect to saned service
        this.router.navigateByUrl(this.returnUrl);
      }
    },
    err => {
      this.router.navigateByUrl(this.returnUrl);
    })
  }

  fetchCartId = userId => {
    const url = environment.hostName + environment.cartAPI + userId + '/carts';

    this.pdpService.checkCartExist(url).subscribe((data: any) => {

      const response = JSON.parse(JSON.stringify(data))
      if (response && response.carts && response.carts.length > 0 && response.carts[0].code) {
        this.utilityService.setCookie('cartId', response.carts[0].code)
      }
      this.router.navigateByUrl(this.returnUrl);
    },
    err => {
      this.router.navigateByUrl(this.returnUrl);
    })
  }

}

@Component({
  selector: 'otp-login-molecule',
  templateUrl: './otp-login-molecule.component.html',
})
export class OtpLoginMolecule {

  constructor(
    public dialogRef: MatDialogRef<OtpLoginMolecule>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) {}

  onNoClick(): void {
    this.dialogRef.close();
  }

}

interface DialogData {
  otp: any;
}
