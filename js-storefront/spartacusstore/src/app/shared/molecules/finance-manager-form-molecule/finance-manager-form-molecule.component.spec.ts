import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FinanceManagerFormMoleculeComponent } from './finance-manager-form-molecule.component';

describe('FinanceManagerFormMoleculeComponent', () => {
  let component: FinanceManagerFormMoleculeComponent;
  let fixture: ComponentFixture<FinanceManagerFormMoleculeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FinanceManagerFormMoleculeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FinanceManagerFormMoleculeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
