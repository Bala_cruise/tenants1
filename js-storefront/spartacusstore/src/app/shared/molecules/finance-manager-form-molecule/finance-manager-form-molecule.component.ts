import { Component, OnInit, OnChanges, Input } from '@angular/core';
import {
  FormBuilder,
  FormGroup,
  Validators,
  FormControl,
} from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import {
  SuperAdminService,
  VALIDATION_MESSAGES
} from '../../../services/super-admin.service';
@Component({
  selector: 'app-finance-manager-form-molecule',
  templateUrl: './finance-manager-form-molecule.component.html',
  styleUrls: ['./finance-manager-form-molecule.component.scss']
})
export class FinanceManagerFormMoleculeComponent implements OnInit, OnChanges {
  @Input() excelValues: any;
  @Input() userId: any;
  itemUpload: FormGroup;
  bpTransUpload: FormGroup;
  openingBalance: FormGroup;
  buttonName: any = 'Submit';
  constructor(private fb: FormBuilder,
              private superAdminService: SuperAdminService,
              private matSnack: MatSnackBar) { }

  ngOnInit() {
    this.itemUpload = this.fb.group({
      itemCode: [null],
      itemDescription: [null],
      itemGroup: [null],
      warehouse: [null],
      inventoryUnit: [null],
      quantity: [null],
      price: [null]
    });

    this.bpTransUpload = this.fb.group({
      documentNumber: [null],
      documentDate: [null],
      businessPartner: [null],
      invoiceNumber: [null],
      currency: [null],
      amountInInvoiceCurrency: [null],
      amountInHC: [null],
      balanceAmount: [null],
      reference: [null],
    });

    this.openingBalance = this.fb.group({
      date: [null],
      ledger: [null],
      dr: [null],
      amount: [null],
      reference: [null]
    });
  }

  ngOnChanges() {
    if (this.excelValues) {
      console.log('exceValues', this.excelValues);
      this.setItemUpload();
      this.setBpTransUpload();
      this.setOpeningBalance();
    }
  }
  setItemUpload() {
    const { itemCode, itemDescription, itemGroup, warehouse, inventoryUnit, quantity, price } = this.excelValues.itemUpload[0];
    this.itemUpload.setValue({
      itemCode: this.checkValues(itemCode),
      itemDescription: this.checkValues(itemDescription),
      itemGroup: this.checkValues(itemGroup),
      warehouse: this.checkValues(warehouse),
      inventoryUnit: this.checkValues(inventoryUnit),
      quantity: this.checkValues(quantity),
      price: this.checkValues(price)
    });
  }
  setBpTransUpload() {
    const { documentNumber, documentDate, businessPartner, invoiceNumber, currency, amountInInvoiceCurrency, amountInHC,
      balanceAmount, reference } = this.excelValues.bpTransUpload[0];
    this.bpTransUpload.setValue({
      documentNumber: this.checkValues(documentNumber),
      documentDate: this.checkValues(documentDate),
      businessPartner: this.checkValues(businessPartner),
      invoiceNumber: this.checkValues(invoiceNumber),
      currency: this.checkValues(currency),
      amountInInvoiceCurrency: this.checkValues(amountInInvoiceCurrency),
      amountInHC: this.checkValues(amountInHC),
      balanceAmount: this.checkValues(balanceAmount),
      reference: this.checkValues(reference)
    });
  }
  setOpeningBalance() {
    const { date, ledger, dr, amount, reference } = this.excelValues.openingBalance[0];
    this.openingBalance.setValue({
      date: this.checkValues(date),
      ledger: this.checkValues(ledger),
      dr: this.checkValues(dr),
      amount: this.checkValues(amount),
      reference: this.checkValues(reference)
    });
  }
  checkValues(val) {
    return val ? val + '' : '';
  }
  onSubmitForm = () => {

    if (
      this.itemUpload.valid &&
      this.bpTransUpload.valid &&
      this.openingBalance.valid) {
      const itemInput = this.itemUpload.value;
      const bpInput = this.bpTransUpload.value;
      const obInput = this.openingBalance.value;
      const request = {
        itemUpload:
        {
          itemCode: this.checkValues(itemInput.itemCode),
          itemDescription: this.checkValues(itemInput.itemDescription) ,
          itemGroup: this.checkValues(itemInput.itemGroup),
          warehouse: this.checkValues(itemInput.warehouse),
          inventoryUnit: this.checkValues(itemInput.inventoryUnit),
          quantity: this.checkValues(itemInput.quantity),
          price: this.checkValues(itemInput.price),
        },
        bpTransUpload:
        {
          documentNumber: this.checkValues(bpInput.documentNumber),
          documentDate: this.checkValues(bpInput.documentDate),
          businessPartner: this.checkValues(bpInput.businessPartner),
          invoiceNumber: this.checkValues(bpInput.invoiceNumber),
          currency: this.checkValues(bpInput.currency),
          amountInInvoiceCurrency: this.checkValues(bpInput.amountInInvoiceCurrency),
          amountInHC: this.checkValues(bpInput.amountInHC),
          balanceAmount: this.checkValues(bpInput.balanceAmount),
          reference: this.checkValues(bpInput.reference),
        },
        openingBalance:
        {
          date: this.checkValues(obInput.date),
          ledger: this.checkValues(obInput.ledger),
          dr: this.checkValues(obInput.dr),
          amount: this.checkValues(obInput.amount),
          reference: this.checkValues(obInput.reference)
        }

      };

      this.superAdminService.submitFinanceManagerForm(request, this.userId).subscribe((res: any) => {
        if (res) {
          this.matSnack.open('Form submitted successfully', 'close', {
            duration: 5000, verticalPosition: 'top'
          });
        } else {
          this.matSnack.open('Please try after sometime', 'close', {
            duration: 5000, verticalPosition: 'top'
          });
        }
      },
        err => {
          this.matSnack.open('Please try after sometime', 'close', {
            duration: 5000, verticalPosition: 'top'
          });
        });

    }
  }

}
