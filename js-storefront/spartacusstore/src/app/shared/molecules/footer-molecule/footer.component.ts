import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-cx-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss'],
})
export class FooterComponent implements OnInit {
  @Input() footerColumnSlot: any;
  @Input() copyRightSlot: any;

  constructor(private route: ActivatedRoute) {}

  ngOnInit() {
  }
}
