import { Component, OnInit, Input } from '@angular/core';
import { environment } from '../../../../environments/environment';

@Component({
  selector: 'app-cart-molecule',
  templateUrl: './cart-molecule.component.html',
  styleUrls: ['./cart-molecule.component.scss']
})
export class CartMoleculeComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
