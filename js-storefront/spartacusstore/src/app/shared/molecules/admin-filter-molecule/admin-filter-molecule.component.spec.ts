import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminFilterMoleculeComponent } from './admin-filter-molecule.component';

describe('AdminFilterMoleculeComponent', () => {
  let component: AdminFilterMoleculeComponent;
  let fixture: ComponentFixture<AdminFilterMoleculeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminFilterMoleculeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminFilterMoleculeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
