import { Component, OnInit } from '@angular/core';
import { REPORTS_FILTER } from '../../../services/admin.service'

@Component({
  selector: 'app-admin-filter-molecule',
  templateUrl: './admin-filter-molecule.component.html',
  styleUrls: ['./admin-filter-molecule.component.scss']
})
export class AdminFilterMoleculeComponent implements OnInit {

  reportsFilter: any
  showValue: any = 'Report 1'
  groupByValue: any = 'Report 1'
  viewByValue: any = 'Report 1'

  constructor() {
    this.reportsFilter = REPORTS_FILTER
   }

  ngOnInit() {
  }

}
