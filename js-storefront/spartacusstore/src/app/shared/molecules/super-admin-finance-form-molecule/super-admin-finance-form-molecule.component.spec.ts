import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SuperAdminFinanceFormMoleculeComponent } from './super-admin-finance-form-molecule.component';

describe('SuperAdminFinanceFormMoleculeComponent', () => {
  let component: SuperAdminFinanceFormMoleculeComponent;
  let fixture: ComponentFixture<SuperAdminFinanceFormMoleculeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SuperAdminFinanceFormMoleculeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SuperAdminFinanceFormMoleculeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
