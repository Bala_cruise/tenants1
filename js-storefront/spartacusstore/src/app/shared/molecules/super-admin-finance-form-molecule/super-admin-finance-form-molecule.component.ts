import { Component, OnInit, OnChanges, Input } from '@angular/core';
import {
  FormBuilder,
  FormGroup,
  Validators,
  FormControl,
} from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';

import {
  SuperAdminService,
  VALIDATION_MESSAGES
} from '../../../services/super-admin.service';

@Component({
  selector: 'app-super-admin-finance-form-molecule',
  templateUrl: './super-admin-finance-form-molecule.component.html',
  styleUrls: ['./super-admin-finance-form-molecule.component.scss']
})
export class SuperAdminFinanceFormMoleculeComponent implements OnInit, OnChanges {
  @Input() excelValues: any;
  genders: any;
  countries: any;
  maritalStatus: any;
  religions: any;

  customerInfo: FormGroup;
  supplierInfo: FormGroup;
  itemInfo: FormGroup;
  validationMessages = VALIDATION_MESSAGES;
  buttonName: any = 'Submit';
  constructor(private fb: FormBuilder,
              private superAdminService: SuperAdminService,
              private matSnack: MatSnackBar) { }

  ngOnInit() {
    this.customerInfo = this.fb.group({
      bpCode: [null, Validators.compose([Validators.required])],
      name: [null, Validators.compose([Validators.required])],
      address: [null, Validators.compose([Validators.required])],
      vatNumber: [null, Validators.compose([Validators.required])],
      currency: [null, Validators.compose([Validators.required])],
      financialGroup: [null, Validators.compose([Validators.required])],
    });

    this.supplierInfo = this.fb.group({
      bpCode: [null, Validators.compose([Validators.required])],
      name: [null, Validators.compose([Validators.required])],
      address: [null, Validators.compose([Validators.required])],
      vatNumber: [null, Validators.compose([Validators.required])],
      currency: [null, Validators.compose([Validators.required])],
      financialGroup: [null, Validators.compose([Validators.required])],
    });

    this.itemInfo = this.fb.group({
      itemCode: [null, Validators.compose([Validators.required])],
      description: [null, Validators.compose([Validators.required])],
      itemType: [null, Validators.compose([Validators.required])],
      itemGroup: [null, Validators.compose([Validators.required])],
      unitSet: [null, Validators.compose([Validators.required])],
      inventoryUnit: [null, Validators.compose([Validators.required])],
      currency: [null, Validators.compose([Validators.required])],
    });
  }
  ngOnChanges() {
    if (this.excelValues) {
       this.setCustomer();
       this.setSupplier();
       this.setItem();
       this.buttonName = 'Review and Submit';
    } else {
      this.buttonName = 'Submit';
    }
  }
  setCustomer() {
    const {bpCode, name, address, vatNumber, currency, financialGroup } = this.excelValues.Customer[0];
    this.customerInfo.setValue({
      bpCode, name, address, vatNumber, currency, financialGroup
    });
  }
  setSupplier() {
    const {bpCode, name, address, vatNumber, currency, financialGroup } = this.excelValues.Supplier[0];
    this.supplierInfo.setValue({
      bpCode, name, address, vatNumber, currency, financialGroup
    });
  }
  setItem() {
    const {itemCode, description, itemType, itemGroup, unitSet, inventoryUnit, currency } = this.excelValues.Item[0];
    this.itemInfo.setValue({
      itemCode, description, itemType, itemGroup, unitSet, inventoryUnit, currency
    });
  }

  onSubmitForm = () => {

    if (
      this.customerInfo.valid &&
      this.supplierInfo.valid &&
      this.itemInfo.valid) {
      const request = {
        customer:
        {
          bpCode: this.customerInfo.value.bpCode + '',
          name: this.customerInfo.value.name + '',
          address: this.customerInfo.value.address + '',
          vatNumber: this.customerInfo.value.vatNumber + '',
          currency: this.customerInfo.value.currency + '',
          financialGroup: this.customerInfo.value.financialGroup + ''
        },
        supplier:
        {
          bpCode: this.supplierInfo.value.bpCode + '',
          name: this.supplierInfo.value.name + '',
          address: this.supplierInfo.value.address + '',
          vatNumber: this.supplierInfo.value.vatNumber + '',
          currency: this.supplierInfo.value.currency + '',
          financialGroup: this.supplierInfo.value.financialGroup + ''
        },
        item:
        {
          itemCode: this.itemInfo.value.itemCode + '',
          description: this.itemInfo.value.description + '',
          itemType: this.itemInfo.value.itemType + '',
          itemGroup: this.itemInfo.value.itemGroup + '',
          unitSet: this.itemInfo.value.unitSet + '',
          inventoryUnit: this.itemInfo.value.inventoryUnit + '',
          currency: this.itemInfo.value.currency + ''
        }

      };

      this.superAdminService.submitForm(request).subscribe((res: any) => {
        if (res) {
          this.matSnack.open('Form submitted successfully', 'close', {
            duration: 5000, verticalPosition: 'top'
          });
        } else {
          this.matSnack.open('Please try after sometime', 'close', {
            duration: 5000, verticalPosition: 'top'
          });
        }
      },
        err => {
          this.matSnack.open('Please try after sometime', 'close', {
            duration: 5000, verticalPosition: 'top'
          });
        });

    }
  }

}
