import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { environment } from '../../../../environments/environment';

@Component({
  selector: 'app-cx-client-logo',
  templateUrl: './client-logo.component.html',
  styleUrls: ['./client-logo.component.scss'],
})
export class ClientLogoComponent implements OnInit {

  clientLogos: any;
  hostName: string = environment.hostName;
  constructor(private route: ActivatedRoute) {}

  ngOnInit() {
    this.route.data.subscribe((data: { homeData: any }) => {
      const response = data.homeData;
      if (
        response &&
        response.contentSlots &&
        response.contentSlots.contentSlot
      ) {
        for (const content of response.contentSlots.contentSlot) {
          if (content.slotId === 'ValuableClientSlot') {
            this.clientLogos = content.components.component;
          }
        }
      }
    });
  }
}
