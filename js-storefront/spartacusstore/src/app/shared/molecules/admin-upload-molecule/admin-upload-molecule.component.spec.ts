import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminUploadMoleculeComponent } from './admin-upload-molecule.component';

describe('AdminUploadMoleculeComponent', () => {
  let component: AdminUploadMoleculeComponent;
  let fixture: ComponentFixture<AdminUploadMoleculeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminUploadMoleculeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminUploadMoleculeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
