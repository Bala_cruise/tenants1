import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminChartMoleculeComponent } from './admin-chart-molecule.component';

describe('AdminChartMoleculeComponent', () => {
  let component: AdminChartMoleculeComponent;
  let fixture: ComponentFixture<AdminChartMoleculeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminChartMoleculeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminChartMoleculeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
