import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminHeaderMoleculeComponent } from './admin-header-molecule.component';

describe('AdminHeaderMoleculeComponent', () => {
  let component: AdminHeaderMoleculeComponent;
  let fixture: ComponentFixture<AdminHeaderMoleculeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminHeaderMoleculeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminHeaderMoleculeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
