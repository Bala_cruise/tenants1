import { Component, OnInit, Input } from '@angular/core';
import { AdminService } from '../../../services/admin.service'
@Component({
  selector: 'app-admin-header-molecule',
  templateUrl: './admin-header-molecule.component.html',
  styleUrls: ['./admin-header-molecule.component.scss']
})
export class AdminHeaderMoleculeComponent implements OnInit {

  @Input() userType: any
  hrImage: any = '../../assets/images/hr.png'
  dpImage: any = '../../assets/images/hrms.png'

  constructor(private adminService: AdminService) { }

  ngOnInit() {
  }

  onLogout = e => {
    e.preventDefault()

    this.adminService.onLogout()
  }

}
