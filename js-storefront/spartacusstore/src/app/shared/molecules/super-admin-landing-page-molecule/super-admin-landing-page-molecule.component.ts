import { Component, OnInit, Input, Output, EventEmitter, OnChanges} from '@angular/core';
import { Router, NavigationEnd  } from '@angular/router';
@Component({
  selector: 'app-super-admin-landing-page-molecule',
  templateUrl: './super-admin-landing-page-molecule.component.html',
  styleUrls: ['./super-admin-landing-page-molecule.component.scss']
})
export class SuperAdminLandingPageMoleculeComponent implements OnInit, OnChanges {
  @Input() products: any;
  @Output() urlChange = new EventEmitter();
  isFinance: any = false;
  isHRMS: any = false;
  isSupplyChain: any = false;
  isManufacturing: any = false;
  hideProductLinks: any = false;
  constructor(private route: Router) {
    this.checkUrl(this.route);
    this.routeEvent(this.route);
   }
   routeEvent(router: Router) {
    router.events.subscribe(e => {
      if (e instanceof NavigationEnd) {
        this.checkUrl(e);
      }
    });
  }
  checkUrl(e) {
    if (e.url === '/superAdmin/hrms' || e.url === '/superAdmin/finance') {
      this.hideProductLinks = true;
   } else {
      this.hideProductLinks = false;
   }
    this.urlChange.emit(this.hideProductLinks);
  }
  ngOnInit() {
    this.products.filter((product, index) => this.products.indexOf(product) === index);
    this.products.map((code) => {
      if (code.toUpperCase() === 'FINANCE'){
          this.isFinance = true;
      } else if (code.toUpperCase() === 'HRMS'){
        this.isHRMS = true;
      }  else if (code.toUpperCase() === 'SUPPLY CHAIN'){
      this.isSupplyChain = true;
      } else if (code.toUpperCase() === 'MANUFACTURING'){
        this.isManufacturing = true;
      }
    })
  }
  ngOnChanges() {
    this.products.filter((product, index) => this.products.indexOf(product) === index);
    this.products.map((code) => {
      if (code.toUpperCase() === 'FINANCE'){
          this.isFinance = true;
      } else if (code.toUpperCase() === 'HRMS'){
        this.isHRMS = true;
      }  else if (code.toUpperCase() === 'SUPPLY CHAIN'){
      this.isSupplyChain = true;
      } else if (code.toUpperCase() === 'MANUFACTURING'){
        this.isManufacturing = true;
      }
    })
  }

}
