import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SuperAdminLandingPageMoleculeComponent } from './super-admin-landing-page-molecule.component';

describe('SuperAdminLandingPageMoleculeComponent', () => {
  let component: SuperAdminLandingPageMoleculeComponent;
  let fixture: ComponentFixture<SuperAdminLandingPageMoleculeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SuperAdminLandingPageMoleculeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SuperAdminLandingPageMoleculeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
