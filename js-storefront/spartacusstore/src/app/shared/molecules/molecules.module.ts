import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '../../material.module';
import { ServicesModule } from '../../services/services.module';
import { DirectivesModule } from '../directives/directives.module';
import {MatDialogModule} from '@angular/material';
import { PipesModule } from '../pipes/pipes.module';
import { HeaderLinksMoleculeComponent } from './header-links-molecule/header-links-molecule.component';
import { HeaderNavMoleculeComponent } from './header-nav-molecule/header-nav-molecule.component';
import { CarousalMoleculeComponent } from './carousal-molecule/carousal-molecule.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { LogoMoleculeComponent } from './logo-molecule/logo-molecule.component';
import { ContactUsMoleculeComponent } from './contact-us-molecule/contact-us-molecule.component';
import { ClientLogoComponent } from './client-logo-molecule/client-logo.component';
import { SanedSectionMoleculeComponent } from './saned-section-molecule/saned-section-molecule.component';
import { RegisterMoleculeComponent } from './register-molecule/register-molecule.component';
import { SignUpComponent } from './sign-up-molecule/sign-up.component';
import { FooterComponent } from './footer-molecule/footer.component';
import { LoginFormMoleculeComponent } from './login-form-molecule/login-form-molecule.component';
import { AppRoutingModule } from '../../app-routing.module';
import { PaymentMoleculeComponent } from './payment-molecule/payment-molecule.component';
import { OrderSummaryMoleculeComponent } from './order-summary-molecule/order-summary-molecule.component';
import { ReviewMoleculeComponent } from './review-molecule/review-molecule.component';
import { CartSummaryComponent } from './cart-summary/cart-summary.component';
import { CheckoutBreadcrumbMoleculeComponent } from './checkout-breadcrumb-molecule/checkout-breadcrumb-molecule.component'
import { CartMoleculeComponent } from './cart-molecule/cart-molecule.component';
import { PageHeaderMoleculeComponent } from './page-header-molecule/page-header-molecule.component';
import { CartCouponMoleculeComponent } from './cart-coupon-molecule/cart-coupon-molecule.component';
import { LoaderMoleculeComponent } from './loader-molecule/loader-molecule.component';
import { OtpLoginMolecule } from './login-form-molecule/login-form-molecule.component';
import { AboutUsMoleculeComponent } from './about-us-molecule/about-us-molecule.component';
import { SuperAdminLandingPageMoleculeComponent } from './super-admin-landing-page-molecule/super-admin-landing-page-molecule.component';
import { SuperAdminHRMSMoleculeComponent } from './super-admin-hrmsmolecule/super-admin-hrmsmolecule.component';
import { SuperAdminFinanceMoleculeComponent } from './super-admin-finance-molecule/super-admin-finance-molecule.component';
import { AdminNavMoleculeComponent } from './admin-nav-molecule/admin-nav-molecule.component';
import { AdminHeaderMoleculeComponent } from './admin-header-molecule/admin-header-molecule.component';
import { AdminFilterMoleculeComponent } from './admin-filter-molecule/admin-filter-molecule.component';
import { HrmsFormMoleculeComponent } from './hrms-form-molecule/hrms-form-molecule.component';
import { AdminNavMobileMoleculeComponent } from './admin-nav-mobile-molecule/admin-nav-mobile-molecule.component';
import { AdminUploadMoleculeComponent } from './admin-upload-molecule/admin-upload-molecule.component';
import { AdminChartMoleculeComponent } from './admin-chart-molecule/admin-chart-molecule.component';
import { SuperAdminFinanceFormMoleculeComponent } from './super-admin-finance-form-molecule/super-admin-finance-form-molecule.component';
import { FinanceAdminHeaderMoleculeComponent } from './finance-admin-header-molecule/finance-admin-header-molecule.component';
import { FinanceManagerFormMoleculeComponent } from './finance-manager-form-molecule/finance-manager-form-molecule.component';

const components = [
    HeaderLinksMoleculeComponent,
    HeaderNavMoleculeComponent,
    CarousalMoleculeComponent,
    LogoMoleculeComponent,
    ContactUsMoleculeComponent,
    ClientLogoComponent,
    SanedSectionMoleculeComponent,
    RegisterMoleculeComponent,
    SignUpComponent,
    FooterComponent,
    LoginFormMoleculeComponent,
    PaymentMoleculeComponent,
    OrderSummaryMoleculeComponent,
    ReviewMoleculeComponent,
    CartSummaryComponent,
    CheckoutBreadcrumbMoleculeComponent,
    CartMoleculeComponent,
    PageHeaderMoleculeComponent,
    CartCouponMoleculeComponent,
    LoaderMoleculeComponent,
    OtpLoginMolecule,
    AboutUsMoleculeComponent,
    SuperAdminLandingPageMoleculeComponent,
    SuperAdminHRMSMoleculeComponent,
    SuperAdminFinanceMoleculeComponent,
    SuperAdminFinanceFormMoleculeComponent,
    AdminNavMoleculeComponent,
    AdminHeaderMoleculeComponent,
    AdminFilterMoleculeComponent,
    HrmsFormMoleculeComponent,
    AdminNavMobileMoleculeComponent,
    AdminUploadMoleculeComponent,
    AdminChartMoleculeComponent,
    FinanceAdminHeaderMoleculeComponent,
    FinanceManagerFormMoleculeComponent
];

@NgModule({
  imports: [
ServicesModule,
    DirectivesModule,
    PipesModule,
    NgbModule,
    MatDialogModule,
    CommonModule,
    RouterModule,
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    AppRoutingModule
  ],
  exports: [...components, RouterModule],
  declarations: [ ...components  ],
  entryComponents: [OtpLoginMolecule],
  providers: [],
})

export class MoleculesModule {}
