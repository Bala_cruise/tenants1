import { Component, OnInit, Input, ElementRef, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { CommonUtilityService } from '../../../services/common-utility-service';
import * as XLSX from 'xlsx';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';

@Component({
  selector: 'app-super-admin-finance-molecule',
  templateUrl: './super-admin-finance-molecule.component.html',
  styleUrls: ['./super-admin-finance-molecule.component.scss']
})
export class SuperAdminFinanceMoleculeComponent implements OnInit {
  selected = new FormControl(0);
  createUserForm: FormGroup;
  financePortalData: any;
  subCustomersData: any;
  hostName = environment.hostName;
  disableCreateUserForm = false;
  matSubCustomersData: any;
  validationMessages = {
    name: [{ type: 'required', message: 'Full name is required' }],
    email: [
      { type: 'required', message: 'Email is required' },
      { type: 'email', message: 'Enter a valid email' }
    ],
    employeeId: [
      { type: 'required', message: 'Employee ID is required' },
      { type: 'pattern', message: 'Please enter a valid Employee ID' }
    ],
    iqamaId: [
      { type: 'required', message: 'Iqama Id is required' },
      { type: 'pattern', message: 'Please enter a valid Iqama Id' }
    ],
    customerRole: [{ type: 'required', message: 'Please select any option' }],
    verificationId: [{ type: 'required', message: 'Verification Id is required' }],
  };
  errMsg = 'Sorry ! Please try again later';
  succMsg = 'User Created successfully !';
  options: any;
  hrImage: any = '../../assets/images/icn_masterdataicn_masterdata_blacksmall.png';
  verificationId: any;
  dropdownList: any = [];
  toggle: any = [];
  approver1: any; approver2: any; approver3: any;
  downloadxl: any;
  fileSelected: any = '';
  fileToUpload: any;
  formGroup: FormGroup;
  fileName: any;
  excelValues: any;
  constructor(private fb: FormBuilder, private router: Router, private el: ElementRef,
              private utilService: CommonUtilityService, private matSnack: MatSnackBar ) {
    const hrUrl = environment.superAdmin.financePortalAPI;
    const httpHeaders = new HttpHeaders()
      .set('Content-Type', 'application/json')
      .set('Authorization', `Bearer ${this.utilService.getCookie('AuthToken')}`);
    this.options = {
      headers: httpHeaders
    };
    this.utilService.getRequest(hrUrl, this.options).subscribe((data: any) => {
      const contentSlot = data.contentSlots.contentSlot;
      if (contentSlot.length > 0) {
        contentSlot.map(each => {
          if (each.slotId === 'FinancePortalVideoSlot') {
            this.financePortalData = each.components.component;
          } else if (each.slotId === 'FinanceVerificationUserSlot') {
            this.verificationId = each.components.component[0].content;
          } else if (each.slotId === 'FinancePortalExcelSlot') {
            this.downloadxl = each.components.component[0].media.url;
          }
        });
        console.log('financePortalData', this.financePortalData);
      }
      this.constructForm();
    });
    this.getSubCustomers();
  }

  ngOnInit() {
    this.formGroup = this.fb.group({
      file: [null, Validators.required]
    });
  }
  constructForm() {
    this.createUserForm = this.fb.group({
      name: [null, Validators.required],
      emailId: [null, Validators.compose([
        Validators.required,
        Validators.email
      ])],
      employeeId: [null, Validators.compose([
        Validators.required,
      ])],
      iqamaId: [null, Validators.compose([
        Validators.required,
      ])],
      customerRole: [null, Validators.required],
      verificationId: [this.verificationId, Validators.required]
    });
  }
  getSubCustomers() {
    this.toggle = [];
    const customerUrl = environment.superAdmin.hrGetCustomerAPI + this.utilService.getCookie('displayUid') + '/customerInfo';
    this.utilService.getRequest(customerUrl, this.options).subscribe((data: any) => {
      if (data.subEmployeesInfo && data.subEmployeesInfo.subEmployeesInfoList) {
        this.subCustomersData = data.subEmployeesInfo.subEmployeesInfoList;
        this.subCustomersData.map(each => { this.dropdownList.push(each.emailId), this.toggle.push(false) });
        console.log('customerData', this.subCustomersData);
      }
    });
  }
  getMatCustomers(id, i) {
    this.approver1 = undefined; this.approver2 = undefined; this.approver3 = undefined;
    const customerUrl = environment.superAdmin.hrGetCustomerAPI + id + '/customerInfo';
    this.utilService.getRequest(customerUrl, this.options).subscribe((data: any) => {
      this.matSubCustomersData = data;
      data.firstApprover ? (this.dropdownList.push(data.firstApprover), this.approver1 = data.firstApprover) : '';
      data.secondApprover ? (this.dropdownList.push(data.secondApprover), this.approver2 = data.secondApprover) : '';
      data.thirdApprover ? (this.dropdownList.push(data.thirdApprover), this.approver3 = data.thirdApprover) : '';
      const unique = new Set(this.dropdownList);
      this.dropdownList = [...unique]
      this.toggleIcon(i, 'add');
    });
  }
  toggleIcon(i, fn) {
    if (fn === 'add') {
      this.toggle.map((each, index) => {
        if (i === index) {
          this.toggle[i] = true;
        } else {
          this.toggle[index] = false;
        }
      });
    } else {
      this.toggle[i] = false;
    }
  }
  createAdmin(user) {
    const { name, emailId, employeeId, iqamaId, customerRole, verificationId } = user;
    const url = environment.superAdmin.financeCreateUserAPI + this.utilService.getCookie('displayUid') + '/register';
    const body = {
      emailId,
      name,
      employeeId,
      iqamaId,
      customerRole,
      verificationUser: verificationId
    };
    const httpHeaders = new HttpHeaders()
      .set('Content-Type', 'application/x-www-form-urlencoded;')
      .set('Authorization', `Bearer ${this.utilService.getCookie('AuthToken')}`);
    const options = {
      headers: httpHeaders
    };
    const request: HttpParams = this.utilService.buildQueryParams(body);
    this.utilService.postRequest(url, request.toString(), options).subscribe((res) => {
      this.getSubCustomers();
      this.matSnack.open(this.succMsg, 'close', {
        duration: 5000, verticalPosition: 'top'
      });
    }, (err) => {
      this.matSnack.open(this.errMsg, 'close', {
        duration: 5000, verticalPosition: 'top'
      });
    });
  }
  retrigger(emailId) {
    const url = environment.superAdmin.financeCreateUserAPI + this.utilService.getCookie('displayUid') + '/retriggerEmail';
    const body = { emailId };
    const httpHeaders = new HttpHeaders()
      .set('Content-Type', 'application/x-www-form-urlencoded;')
      .set('Authorization', `Bearer ${this.utilService.getCookie('AuthToken')}`);
    const options = {
      headers: httpHeaders
    };
    const request: HttpParams = this.utilService.buildQueryParams(body);
    this.utilService.postRequest(url, request.toString(), options).subscribe((res) => {
      this.matSnack.open('Mail has been triggerred successfully', 'close', {
        duration: 5000, verticalPosition: 'top'
      });
    }, (err) => {
      this.matSnack.open(this.errMsg, 'close', {
        duration: 5000, verticalPosition: 'top'
      });
    });
  }
  addApprover(level, id) {
    console.log('approver', this.approver1);
    const url = environment.superAdmin.financeCreateUserAPI + id + '/setAuthMatrix';
    const body = {
      approverLevel1: level === '1' ? this.approver1 : '',
      approverLevel2: level === '2' ? this.approver2 : '',
      approverLevel3: level === '3' ? this.approver3 : ''
    };
    const httpHeaders = new HttpHeaders()
      .set('Content-Type', 'application/x-www-form-urlencoded;')
      .set('Authorization', `Bearer ${this.utilService.getCookie('AuthToken')}`);
    const options = {
      headers: httpHeaders
    };
    const request: HttpParams = this.utilService.buildQueryParams(body);
    this.utilService.putRequest(url, request.toString(), options).subscribe((res) => {
      this.matSnack.open('Approver has been added successfully', 'close', {
        duration: 5000, verticalPosition: 'top'
      });
    }, (err) => {
      this.matSnack.open(this.errMsg, 'close', {
        duration: 5000, verticalPosition: 'top'
      });
    });
  }

  openNextLevel() {
    !this.approver1 ? this.approver1 = 'x' : ((this.approver1 && !this.approver2) ? this.approver2 = 'x' : this.approver3 = 'x');
  }
  downloadFile = e => {
    e.preventDefault();
    const downloadUrl = environment.hostName + this.downloadxl;
    window.open(downloadUrl, '_self')
  }
  fileInputChange(event: any) {
    let workBook = null;
    let jsonData = null;
    const reader = new FileReader();
    const file = event.target.files[0];
    reader.onload = () => {
      const data = reader.result;
      workBook = XLSX.read(data, { type: 'binary' });
      jsonData = workBook.SheetNames.reduce((initial, name) => {
        const sheet = workBook.Sheets[name];
        initial[name] = XLSX.utils.sheet_to_json(sheet);
        return initial;
      }, {});
      jsonData ? this.excelValues = jsonData : '';
      this.selected.setValue(0);
    };
    reader.readAsBinaryString(file);
  }
}
