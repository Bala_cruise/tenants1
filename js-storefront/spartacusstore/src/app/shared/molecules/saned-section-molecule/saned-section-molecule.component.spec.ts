import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SanedSectionMoleculeComponent } from './saned-section-molecule.component';

describe('SanedSectionMoleculeComponent', () => {
  let component: SanedSectionMoleculeComponent;
  let fixture: ComponentFixture<SanedSectionMoleculeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SanedSectionMoleculeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SanedSectionMoleculeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
