import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { environment } from '../../../../environments/environment';
@Component({
  selector: 'app-saned-section-molecule',
  templateUrl: './saned-section-molecule.component.html',
  styleUrls: ['./saned-section-molecule.component.scss']
})
export class SanedSectionMoleculeComponent implements OnInit {
  images: any;
  hostName: string = environment.hostName;
  constructor(private route: ActivatedRoute) {}
  ngOnInit() {
    this.route.data.subscribe((data: { homeData: any }) => {
      const response = data.homeData;
      if (
        response &&
        response.contentSlots &&
        response.contentSlots.contentSlot
      ) {
        for (const content of response.contentSlots.contentSlot) {
          if (content.slotId === 'SanedSolutionSlot') {
            this.images = content.components.component;
            console.log("images", this.images)
          }
        }
      }
    });
  }

}
