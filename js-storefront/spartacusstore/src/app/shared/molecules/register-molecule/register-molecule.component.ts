import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { RegisterUserService } from '../../../services/register-user.service';
import { LoginService } from '../../../services/login.service';
import { CommonUtilityService } from '../../../services/common-utility-service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
@Component({
  selector: 'app-register-molecule',
  templateUrl: './register-molecule.component.html',
  styleUrls: ['./register-molecule.component.scss']
})
export class RegisterMoleculeComponent implements OnInit {
  registerForm: FormGroup;
  userContext: any;
  termsAndConditions: any = false;
  validationMessages = {
    name: [{ type: 'required', message: 'Full name is required' }],
    email: [
      { type: 'required', message: 'Email is required' },
      { type: 'email', message: 'Enter a valid email' }
    ],
    prefix: [
      { type: 'required', message: 'Mobile code is required' },
      { type: 'pattern', message: 'Enter 3 digit code' }
    ],
    mobile: [
      { type: 'required', message: 'Mobile number is required' },
      { type: 'pattern', message: 'Please enter a valid mobile number' }
    ],
    company: [{ type: 'required', message: 'Company name is required' }],
    //size: [{ type: 'required', message: 'Please select any option' }],
    commercial: [{ type: 'required', message: 'Commercial registration is required' }],
    confirmCommercial: [
      { type: 'required', message: 'confirm Commercial registration is required' },
      { type: 'notEquivalent', message: 'Commercial registration does not match' }
    ]

  };
  errMsg = 'Sorry ! Please try again later';
  succMsg = 'User Registerd successfully !';
  constructor(private fb: FormBuilder, private regUserService: RegisterUserService, private router: Router,
    private matSnack: MatSnackBar, private utilService: CommonUtilityService, private loginService: LoginService) {
    this.loginService.userContext.subscribe(context => {
      this.userContext = context;
    });
  }

  ngOnInit() {
    const MOBILE_PATTERN = '^((\\+[0-9]{2}-?)|0)?[0-9]{9}$';
    const PREFIX_PATTERN = '^[0-9]{3}$';
    this.registerForm = this.fb.group({
      name: [null, Validators.required],
      email: [null, Validators.compose([
        Validators.required,
        Validators.email
      ])],
      prefix: [null, Validators.compose([
        Validators.required,
        Validators.pattern(PREFIX_PATTERN)
      ])],
      mobile: [null, Validators.compose([
        Validators.required,
        Validators.pattern(MOBILE_PATTERN)
      ])],
      company: [null, Validators.required],
      //size: [null, Validators.required],
      commercial: [null, Validators.required],
      confirmCommercial: [null, Validators.required],
    }, { validator: this.pwdConfirming('commercial', 'confirmCommercial') });
  }
  pwdConfirming(key: string, confirmationKey: string) {
    return (group: FormGroup) => {
      const input = group.controls[key];
      const confirmationInput = group.controls[confirmationKey];
      return confirmationInput.setErrors(
        input.value !== confirmationInput.value ? { notEquivalent: true } : null
      );
    };
  }
  setTerms(e) {
    this.termsAndConditions = e.checked;
  }
  registerUser(user) {
    user.mobile = user.prefix + user.mobile;
    const { name, email, mobile, company, commercial } = user;

    if (name && email && mobile && company && commercial) {
      const token = this.utilService.getCookie('AuthToken');
      if (token) {
        this.regUserService.registerUserDetails(token, user).subscribe((result: any) => {
          this.matSnack.open(this.succMsg, 'close', {
            duration: 5000, verticalPosition: 'top'
          });
          this.router.navigate(['/login']);
        }, (err) => {
          this.matSnack.open(err.error.errors[0].message, 'close', {
            duration: 5000, verticalPosition: 'top'
          });
        });
      }
    }
  }
}
