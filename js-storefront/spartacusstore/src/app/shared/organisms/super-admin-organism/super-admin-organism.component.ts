import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CommonUtilityService } from '../../../services/common-utility-service';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
@Component({
  selector: 'app-super-admin-organism',
  templateUrl: './super-admin-organism.component.html',
  styleUrls: ['./super-admin-organism.component.scss']
})
export class SuperAdminOrganismComponent implements OnInit {
  superAdminData: any;
  products = [];
  hideProductLinks: any = false;
  constructor(private route: ActivatedRoute, private router: Router, private utilityService: CommonUtilityService) {

    if (this.router.url === '/superAdmin/hrms' || this.router.url === '/superAdmin/finance') {
      this.hideProductLinks = true;
    } else {
      this.hideProductLinks = false;
    }

  }

  ngOnInit() {
    this.superAdminData = [];
    const orders = JSON.parse(this.utilityService.getCookie('superAdminOrders'));
    const orderAPI = environment.usersEndpoint + this.utilityService.getCookie('displayUid') + '/orders/';
    orders.map((order, i) => {
      const url = orderAPI + order.code;
      const httpHeaders = new HttpHeaders()
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${this.utilityService.getCookie('AuthToken')}`);

      const options = {
        headers: httpHeaders
      };
      this.utilityService.getRequest(url, options).subscribe((res) => {
        this.superAdminData.push(res);
        if (i + 1 === orders.length) {
          this.getProducts(this.superAdminData);
        }
      });
    });
  }
  getProducts(data) {
    data.map((each) => {
      this.products.push(each.entries[0].product.code);
    });
  }
  urlChange(event) {
    this.hideProductLinks = event;
  }
}
