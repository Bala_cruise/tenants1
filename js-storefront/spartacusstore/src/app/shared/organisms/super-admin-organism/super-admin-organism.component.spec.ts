import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SuperAdminOrganismComponent } from './super-admin-organism.component';

describe('SuperAdminOrganismComponent', () => {
  let component: SuperAdminOrganismComponent;
  let fixture: ComponentFixture<SuperAdminOrganismComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SuperAdminOrganismComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SuperAdminOrganismComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
