import { Component, OnInit, Input, ElementRef, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { CommonUtilityService } from '../../../services/common-utility-service';
import * as XLSX from 'xlsx';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';


@Component({
  selector: 'app-finance-manger-organism',
  templateUrl: './finance-manger-organism.component.html',
  styleUrls: ['./finance-manger-organism.component.scss']
})
export class FinanceMangerOrganismComponent implements OnInit {
  downloadForm: any;
  subCustomers: any;
  fileName: any;
  excelValues: any = {};
  selected = new FormControl(0);

  constructor(private utilService: CommonUtilityService) {
    const url = environment.superAdmin.financeManagerAPI;
    const httpHeaders = new HttpHeaders()
    .set('Content-Type', 'application/json')
    .set('Authorization', `Bearer ${this.utilService.getCookie('AuthToken')}`);
    const options = {
      headers: httpHeaders
    };
    this.utilService.getRequest(url, options).subscribe((data: any) => {
      this.downloadForm = data.contentSlots.contentSlot.components.component[0].media.url;
      });
    this.subCustomers = JSON.parse(this.utilService.getCookie('customerList'));
   }

  ngOnInit() {
  }

  downloadFile = e => {
    e.preventDefault();
    const downloadUrl = environment.hostName + this.downloadForm;
    window.open(downloadUrl, '_self')
  }
  fileInputChange(event: any, i) {
    let workBook = null;
    let jsonData = null;
    const reader = new FileReader();
    const file = event.target.files[0];
    reader.onload = () => {
      const data = reader.result;
      workBook = XLSX.read(data, { type: 'binary' });
      jsonData = workBook.SheetNames.reduce((initial, name) => {
        const sheet = workBook.Sheets[name];
        initial[name] = XLSX.utils.sheet_to_json(sheet);
        return initial;
      }, {});
      jsonData ? this.excelValues[i] = jsonData : '';
      this.selected.setValue(0);
    };
    reader.readAsBinaryString(file);
  }
}
