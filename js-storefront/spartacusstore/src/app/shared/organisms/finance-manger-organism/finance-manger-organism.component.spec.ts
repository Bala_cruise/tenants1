import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FinanceMangerOrganismComponent } from './finance-manger-organism.component';

describe('FinanceMangerOrganismComponent', () => {
  let component: FinanceMangerOrganismComponent;
  let fixture: ComponentFixture<FinanceMangerOrganismComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FinanceMangerOrganismComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FinanceMangerOrganismComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
