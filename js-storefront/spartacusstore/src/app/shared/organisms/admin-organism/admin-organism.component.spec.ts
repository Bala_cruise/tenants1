import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminOrganismComponent } from './admin-organism.component';

describe('AdminOrganismComponent', () => {
  let component: AdminOrganismComponent;
  let fixture: ComponentFixture<AdminOrganismComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminOrganismComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminOrganismComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
