import { Component, OnInit } from '@angular/core';
import { SETUP_NAV_LINKS } from '../../../services/admin.service'

@Component({
  selector: 'app-admin-organism',
  templateUrl: './admin-organism.component.html',
  styleUrls: ['./admin-organism.component.scss']
})
export class AdminOrganismComponent implements OnInit {

  setupNavLinks: any
  panelOpenState = true;
  hrImage: any = '../../assets/images/icn_masterdataicn_masterdata_blacksmall.png'
  

  constructor() { 
    this.setupNavLinks = SETUP_NAV_LINKS
  }

  ngOnInit() {
  }

  changeAccordian = () => {
   // this.contactInfo = true
  }

}
