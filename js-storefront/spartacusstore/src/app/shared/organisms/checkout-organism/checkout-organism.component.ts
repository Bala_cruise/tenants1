import { Component, OnInit } from '@angular/core';
import { CheckoutService, REVIEW_PATH, PAYMENT_PATH } from '../../../services/checkout.service'
import { PdpService } from '../../../services//pdp.service'
import { Router, ActivatedRoute  } from '@angular/router';
import { environment } from '../../../../environments/environment'
import { CommonUtilityService } from '../../../services/common-utility-service'
@Component({
  selector: 'app-checkout-organism',
  templateUrl: './checkout-organism.component.html',
  styleUrls: ['./checkout-organism.component.scss']
})
export class CheckoutOrganismComponent implements OnInit {
  paymentContext: any
  checkoutBreadcrumb: any
  nextPage: any = REVIEW_PATH
  previousPage: any
  activePage: any
  cartData: any
  checkoutPage:boolean = false;

  constructor(
    private checkoutService: CheckoutService,
    private router: Router,
    private route: ActivatedRoute,
    private pdpService: PdpService,
    private utilityService: CommonUtilityService
    ) {
    // this.checkoutBreadcrumb = PAYMENT_PATH
    // this.checkoutService.changeActivePage(this.checkoutBreadcrumb)

    // this.checkoutService.checkoutBreadcrumb.subscribe(context => {
    //   this.activePage = context
    //   if(this.activePage === REVIEW_PATH) {
    //     this.nextPage = null
    //     this.previousPage = PAYMENT_PATH
    //   } else if(this.activePage === PAYMENT_PATH) {
    //     this.nextPage = REVIEW_PATH
    //     this.previousPage = null
    //   }
    // });
    // this.router.navigate([PAYMENT_PATH], {relativeTo: this.activatedRoute})
   }

  ngOnInit() {
  //   this.activatedRoute.data.subscribe((data: { cartData: any }) => {
  //     this.cartData = data.cartData;
  //     // if(data.cartData === false) {
  //     //   this.router.navigate(['/'])
  //     // }
  //  });
  const url = `${ environment.hostName}${environment.cartAPI}${this.utilityService.getCookie('displayUid')}/carts`; 

  this.pdpService.checkCartExist(url).subscribe((res: any) => {

    const response = JSON.parse(JSON.stringify(res));

      if (response && response.carts && response.carts.length > 0 && response.carts[0].code) {

        const cartId = response.carts[0].code

        this.checkoutService.getSummaryDetails(cartId).subscribe((res: any) => {
          this.cartData = res
        },
        err => {
          this.router.navigate(['/'])
        })
      }
  },
  err => {
    this.router.navigate(['/'])
  })

   this.route.data.subscribe((data: { cartData: any }) => {
    this.cartData = data.cartData;
 });
  }

  onChangePage(page) {
    if(page === REVIEW_PATH) {
      this.paymentContext = true
      this.checkoutService.changePaymentContext(this.paymentContext);
      if(this.checkoutService.getPaymentDetails !== null) {
      this.checkoutBreadcrumb = page
      this.checkoutService.changeActivePage(this.checkoutBreadcrumb)
      this.router.navigate([page], {relativeTo: this.route})
      }
    } else {
      this.checkoutBreadcrumb = page
      this.checkoutService.changeActivePage(this.checkoutBreadcrumb)
      this.router.navigate([page], {relativeTo: this.route})
    }
    
  }

}
