import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CheckoutOrganismComponent } from './checkout-organism.component';

describe('CheckoutOrganismComponent', () => {
  let component: CheckoutOrganismComponent;
  let fixture: ComponentFixture<CheckoutOrganismComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CheckoutOrganismComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CheckoutOrganismComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
