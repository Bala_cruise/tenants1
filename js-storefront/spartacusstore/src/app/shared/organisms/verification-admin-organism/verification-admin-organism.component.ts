import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { CommonUtilityService } from '../../../services/common-utility-service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { VerifyAdminService } from '../../../services/verify-admin.service'

@Component({
  selector: 'app-verification-admin-organism',
  templateUrl: './verification-admin-organism.component.html',
  styleUrls: ['./verification-admin-organism.component.scss']
})
export class VerificationAdminOrganismComponent implements OnInit {

  options: any;
  subEmployees: any;
  subEmployeesDetails: any;
  customer: any;
  item: any;
  supplier: any;
  customerForm: any;
  supplierForm: any;
  itemForm: any;
  supEmployeeId: any

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private verifyAdminService: VerifyAdminService,
    private utilService: CommonUtilityService,
    private matSnack: MatSnackBar
    ) {

    //this.getSubCustomers();
  }

  ngOnInit() {
    const customerUrl = environment.superAdmin.hrGetCustomerAPI + this.utilService.getCookie('displayUid') + '/customerInfo';
    const httpHeaders = new HttpHeaders()
      .set('Content-Type', 'application/json')
      .set('Authorization', `Bearer ${this.utilService.getCookie('AuthToken')}`);
    this.options = {
      headers: httpHeaders
    };
    this.utilService.getRequest(customerUrl, this.options).subscribe((data: any) => {
      this.subEmployees = data.subEmployeesInfo.subEmployeesInfoList;
      console.log("emppp",this.subEmployees)
    });

    this.customerForm = this.fb.group({
      address: [null, Validators.required],
      bpCode: [null, Validators.required],
      currency: [null, Validators.required],
      financialGroup: [null, Validators.required],
      name: [null, Validators.required],
      vatNumber: [null, Validators.required]
    })

    this.supplierForm = this.fb.group({
      address: [null, Validators.required],
      bpCode: [null, Validators.required],
      currency: [null, Validators.required],
      financialGroup: [null, Validators.required],
      name: [null, Validators.required],
      vatNumber: [null, Validators.required]
    })

    this.itemForm = this.fb.group({
      currency: [null, Validators.required],
      description: [null, Validators.required],
      inventoryUnit: [null, Validators.required],
      itemCode: [null, Validators.required],
      itemGroup: [null, Validators.required],
      itemType: [null, Validators.required],
      unitSet: [null, Validators.required]
    })
  }

  getSubEmpDetails(id) {

    this.supEmployeeId = id
    let isResponded = false
    if(!isResponded) {
      const empUrl = environment.verificationAdmin.subEmpApi + '/' + id + '/customermasterdatasetup';
    this.utilService.getRequest(empUrl, this.options).subscribe((data: any) => {
      
      this.customerForm.setValue(data.customer, {onlySelf: true})
      this.supplierForm.setValue(data.supplier, {onlySelf: true})
      this.itemForm.setValue(data.item, {onlySelf: true})
      isResponded = true
    });
    }
  }

onSubmitForm = () => {

  if(this.supplierForm.valid && this.supplierForm.valid && this.itemForm.valid) {
    const request = {
      'customer': this.customerForm.value,
      'supplier': this.supplierForm.value,
      'item': this.itemForm.value
    }
    const empUrl = environment.verificationAdmin.subEmpApi + '/' + this.supEmployeeId + '/customermasterdatasetup';

    this.verifyAdminService.submitForm(empUrl, request).subscribe((res: any) => {
      this.matSnack.open('Details updated successfully', 'close', {
        duration: 5000,    verticalPosition: 'bottom'
      });
    },
    err => {
      this.matSnack.open('Please try after sometime', 'close', {
        duration: 5000,    verticalPosition: 'bottom'
      });
    })
  }

}

}
