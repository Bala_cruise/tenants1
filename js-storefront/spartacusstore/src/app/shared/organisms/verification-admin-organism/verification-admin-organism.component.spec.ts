import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VerificationAdminOrganismComponent } from './verification-admin-organism.component';

describe('VerificationAdminOrganismComponent', () => {
  let component: VerificationAdminOrganismComponent;
  let fixture: ComponentFixture<VerificationAdminOrganismComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VerificationAdminOrganismComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VerificationAdminOrganismComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
