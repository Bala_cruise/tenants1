import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CartOrganismComponent } from './cart-organism.component';

describe('CartOrganismComponent', () => {
  let component: CartOrganismComponent;
  let fixture: ComponentFixture<CartOrganismComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CartOrganismComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CartOrganismComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
