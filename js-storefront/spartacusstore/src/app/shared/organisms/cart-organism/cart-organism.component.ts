import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import { CartService, CART_UPDATE_FAILURE_MSG, UPDATE_CART } from '../../../services/cart.service'

@Component({
  selector: 'app-cart-organism',
  templateUrl: './cart-organism.component.html',
  styleUrls: ['./cart-organism.component.scss']
})
export class CartOrganismComponent implements OnInit {

  cartData: any;
  constructor(
    private route: ActivatedRoute,
    private cartService: CartService,
    private matSnack: MatSnackBar
    ) {
      this.cartService.cartContext.subscribe(context => {
        if(context) {
          this.updateCart()
        }
      });
     }

  ngOnInit() {
    this.route.data.subscribe((data: { cartData: any }) => {
       this.cartData = data.cartData;
    });
  }

  updateCart = () => {
    this.cartService.getCart().subscribe((res: any) => {
      this.cartData = res
    },
    err => {
      this.matSnack.open(CART_UPDATE_FAILURE_MSG, 'close', {
        duration: 5000,
      });
    })
  }
}
