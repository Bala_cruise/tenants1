import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ServicesModule } from '../../services/services.module';
import { MoleculesModule } from '../molecules/molecules.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DirectivesModule } from '../directives/directives.module';
import { PipesModule } from '../pipes/pipes.module';
import { MaterialModule } from '../../material.module';
import { HeaderOrganismComponent } from './header-organism/header-organism.component';
import { FooterOrganismComponent } from './footer-organism/footer-organism.component';
import { HomeOrganismComponent } from './home-organism/home-organism.component';
import { NotFoundOrganismComponent } from './not-found-organism/not-found-organism.component';
import { LoginOrganismComponent } from './login-organism/login-organism.component';
import { CheckoutOrganismComponent } from './checkout-organism/checkout-organism.component';
import { PdpOrganismComponent } from './pdp-organism/pdp-organism.component';
import { CartOrganismComponent } from './cart-organism/cart-organism.component';
import { OrderConfirmationOrganismComponent } from './order-confirmation-organism/order-confirmation-organism.component';
import { SuperAdminOrganismComponent } from './super-admin-organism/super-admin-organism.component';
import { AdminOrganismComponent } from './admin-organism/admin-organism.component';
import { FinanceAdminOrganismComponent } from './finance-admin-organism/finance-admin-organism.component';
import { VerificationAdminOrganismComponent } from './verification-admin-organism/verification-admin-organism.component';
import { FinanceMangerOrganismComponent } from './finance-manger-organism/finance-manger-organism.component';

const components = [
    HeaderOrganismComponent,
    FooterOrganismComponent,
    HomeOrganismComponent,
    LoginOrganismComponent,
    NotFoundOrganismComponent,
    CheckoutOrganismComponent,
    PdpOrganismComponent,
    CartOrganismComponent,
    OrderConfirmationOrganismComponent,
    SuperAdminOrganismComponent,
    AdminOrganismComponent,
    FinanceAdminOrganismComponent,
    VerificationAdminOrganismComponent,
    FinanceMangerOrganismComponent
]

@NgModule({
    imports: [
        CommonModule,
        ServicesModule,
        MoleculesModule,
        FormsModule, ReactiveFormsModule,
        DirectivesModule,
        PipesModule,
        MaterialModule,

    ],
    exports: [
        ...components
    ],
    declarations: [
        ...components,
    ],
    providers: [],
})
export class OrganismsModule { }
