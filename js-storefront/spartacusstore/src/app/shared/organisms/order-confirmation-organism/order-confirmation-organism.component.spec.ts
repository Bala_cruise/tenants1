import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderConfirmationOrganismComponent } from './order-confirmation-organism.component';

describe('OrderConfirmationOrganismComponent', () => {
  let component: OrderConfirmationOrganismComponent;
  let fixture: ComponentFixture<OrderConfirmationOrganismComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrderConfirmationOrganismComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderConfirmationOrganismComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
