import { Component, OnInit } from '@angular/core';
import { CommonUtilityService } from '../../../services/common-utility-service';

@Component({
  selector: 'app-order-confirmation-organism',
  templateUrl: './order-confirmation-organism.component.html',
  styleUrls: ['./order-confirmation-organism.component.scss']
})
export class OrderConfirmationOrganismComponent implements OnInit {
  orderId: any;
  constructor(private utilityService: CommonUtilityService) { }

  ngOnInit() {
     this.orderId = this.utilityService.getCookie('orderId');
  }

}
