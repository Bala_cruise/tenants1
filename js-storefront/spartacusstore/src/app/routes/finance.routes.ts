import { NotFoundOrganismComponent } from '../shared/organisms/not-found-organism/not-found-organism.component';
import { FinanceAdminOrganismComponent } from '../shared/organisms/finance-admin-organism/finance-admin-organism.component';
export const FINANCE_ROUTES = [
    { path: '', redirectTo: 'finance', pathMatch: 'full' },
    {
        path: 'finance',
        component: FinanceAdminOrganismComponent,
        data: [{
          pageName: 'Finance Page',
        }],
      }
]