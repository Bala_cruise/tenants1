import { NotFoundOrganismComponent } from '../shared/organisms/not-found-organism/not-found-organism.component';
import { VerificationAdminOrganismComponent } from '../shared/organisms/verification-admin-organism/verification-admin-organism.component';
export const VERIFICATION_ADMIM_ROUTES = [
    { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
    {
        path: 'dashboard',
        component: VerificationAdminOrganismComponent,
        data: [{
          pageName: 'verification admin Page',
        }],
      }
]