import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable, BehaviorSubject } from 'rxjs';
import { CommonUtilityService } from '../services/common-utility-service';
import { environment } from '../../environments/environment';
import { map, catchError } from 'rxjs/operators';
import { PdpService } from './pdp.service'

@Injectable({
  providedIn: 'root',
})
@Injectable()
export class CheckoutService {
  public paymentDetails: any = null;
  public defaultPayment: any;

  private paymentContextData = new BehaviorSubject<paymentContext>(
    new paymentContext(false)
  );
  paymentContext = this.paymentContextData.asObservable();

  private activePageData = new BehaviorSubject<checkoutBreadcrumb>(
    new checkoutBreadcrumb(PAYMENT_PATH)
  );
  checkoutBreadcrumb = this.activePageData.asObservable();

  constructor(
    private http: HttpClient,
    private utilityService: CommonUtilityService,
    private pdpService: PdpService
  ) {}

  changePaymentContext(data: paymentContext) {
    this.paymentContextData.next(data);
  }

  changeActivePage(page: checkoutBreadcrumb) {
    this.activePageData.next(page);
  }

  getSummaryDetails = cartId => {
    
    const cartApi = `${environment.cartAPI + this.utilityService.getCookie('displayUid')}/carts/${cartId}`;

        const httpHeaders = new HttpHeaders()
          .set('Content-Type', 'application/json')
          .set('Authorization', `Bearer${this.utilityService.getCookie('AuthToken')}`);

        const options = {
          headers: httpHeaders
        };

        return this.utilityService.getRequest(cartApi, options);
  }

  placeOrder = terms => {
    const request = new HttpParams()
      .set('cartId', this.utilityService.getCookie('cartId'))
      .set('termsAndCondition', terms);

    let httpHeaders = new HttpHeaders().set(
      'Authorization',
      `Bearer ${this.utilityService.getCookie('AuthToken')}`
    );

    let options = {
      headers: httpHeaders,
    };

    let url = `${environment.cartAPI +
      this.utilityService.getCookie('displayUid')}/orders`;

    return this.utilityService.postRequest(url, request, options);
  };

  set setPaymentDetails(data) {
    if (data && data.valid) {
      this.paymentDetails = data.value;
    } else {
      this.paymentDetails = null;
    }
  }
  get getPaymentDetails() {
    return this.paymentDetails;
  }
  set setDefaultPayment(flag) {
    this.defaultPayment = flag;
    console.log(this.defaultPayment);
  }
  get getDefaultPayment() {
    return this.defaultPayment;
  }

  setBreadcrumbToDefault() {
    return [
      {
        sequence: 1,
        pageName: '1.Payment Details',
        isActive: false,
        isCurrentPage: false,
        routePath: PAYMENT_PATH,
      },
      {
        sequence: 2,
        pageName: '2. Order Review',
        isActive: false,
        isCurrentPage: false,
        routePath: REVIEW_PATH,
      },
    ];
  }
}

export class paymentContext {
  constructor(public triggerChange: boolean) {
    this.triggerChange = triggerChange;
  }
}

export class checkoutBreadcrumb {
  constructor(public activePage: any) {
    this.activePage = activePage;
  }
}

export const MOBILE_DEVICE_SIZE = 768;
export const REVIEW_PATH = 'review';
export const PAYMENT_PATH = 'payment';
export const CARD_TYPES = ['visa', 'Master', 'American Express'];
export const MONTH_LIST = [
  'Jan',
  'Feb',
  'Mar',
  'Apr',
  'May',
  'Jun',
  'Jul',
  'Aug',
  'Sep',
  'Oct',
  'Nov',
  'Dec',
];
export const ACCEPT_TERMS_MSG = 'please accept TermsAndCondition';
