import { Injectable } from '@angular/core';
import { HttpClient , HttpHeaders , HttpParams} from '@angular/common/http';
import { CommonUtilityService } from '../services/common-utility-service';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class RegisterUserService {
  constructor(private http: HttpClient, private utilService: CommonUtilityService) {
   }
   registerUserDetails(token, req) {
    const authToken = 'Bearer ' + token;
    const httpHeaders = new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded;',
      'Authorization': authToken
    });
    const size = "PRICERANGE1";
    const body = {
      name :  req.name,
      email: req.email,
      contact: req.mobile,
      company: req.company,
      sizeRange: size,
      commercialRegistration: req.commercial
    };
    const request: HttpParams = this.utilService.buildQueryParams(body);
    const url = environment.hostName + environment.registerUserAPI;
    return this.http.post(url, request.toString(), {headers: httpHeaders});
   }
}
