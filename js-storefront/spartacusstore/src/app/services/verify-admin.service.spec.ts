import { TestBed } from '@angular/core/testing';

import { VerifyAdminService } from './verify-admin.service';

describe('VerifyAdminService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: VerifyAdminService = TestBed.get(VerifyAdminService);
    expect(service).toBeTruthy();
  });
});
