import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Router  } from '@angular/router';
import { CommonUtilityService } from '../services/common-utility-service';
import { environment } from '../../environments/environment';
import { LoginService } from './login.service'
import { EmitterService } from './emitter.service'
@Injectable({
  providedIn: 'root'
})
export class SuperAdminService {

  constructor( private utilityService: CommonUtilityService,
    private loginService: LoginService,
    private emitter: EmitterService,
    private router: Router) { }
  
  submitForm = request => {
    // const request = new HttpParams()
    // .set('cartId', this.utilityService.getCookie('cartId'))

    

    let httpHeaders = new HttpHeaders()
    .set(
    'Authorization',
    `Bearer ${this.utilityService.getCookie('AuthToken')}`
    )
    .set('Content-Type', 'application/json');

    let options = {
    headers: httpHeaders,
    };

    let url = `${environment.superAdmin.financeGetCustomerAPI + this.utilityService.getCookie('displayUid')}/customermasterdatasetup`;

    return this.utilityService.postRequest(url, request, options);
}
submitFinanceManagerForm = (request, id) => {
  // const request = new HttpParams()
  // .set('cartId', this.utilityService.getCookie('cartId'))

  

  let httpHeaders = new HttpHeaders()
  .set(
  'Authorization',
  `Bearer ${this.utilityService.getCookie('AuthToken')}`
  )
  .set('Content-Type', 'application/json');

  let options = {
  headers: httpHeaders,
  };

  let url = `${environment.superAdmin.financeGetCustomerAPI + id}/finance-openbalance`;

  return this.utilityService.postRequest(url, request, options);
}

}
export const VALIDATION_MESSAGES = {
  bpCode: [{ type: 'required', message: 'BP code is required' }], 
  name: [{ type: 'required', message: 'Name is required' }],
  address: [{ type: 'required', message: 'Address is required' }],
  vatNumber: [{ type: 'required', message: 'Vat number is required' }],
  currency: [{ type: 'required', message: 'Currency is required' }],
  financialGroup: [{ type: 'required', message: 'Financial group is required' }],

  itemCode: [{ type: 'required', message: 'Item code is required' }],
  description: [{ type: 'required', message: 'Description is required' }],
  itemType: [{ type: 'required', message: 'Item type is required' }],
  itemGroup: [{ type: 'required', message: 'Item group Of Iqama is required' }],
  unitSet: [{ type: 'required', message: 'Unit set is required' }],
  inventoryUnit: [{ type: 'required', message: 'Inventory unit is required' }],
};
