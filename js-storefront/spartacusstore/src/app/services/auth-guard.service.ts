import {CanActivate} from "@angular/router";
import { Injectable } from '@angular/core';
import { Router  } from '@angular/router';
import { CommonUtilityService } from './common-utility-service'

@Injectable()

export class AuthGuard implements CanActivate {

    constructor(
        private utilityService: CommonUtilityService,
        private router: Router) {}

    canActivate() {
       if(this.utilityService.getCookie('isAuthenticated')) {
           return true
       }
       this.router.navigate(['/'])
    }
  }