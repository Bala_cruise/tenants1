import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable, BehaviorSubject } from 'rxjs';
import { CommonUtilityService } from '../services/common-utility-service';
import { environment } from '../../environments/environment';
import { map, catchError } from 'rxjs/operators';
@Injectable({
  providedIn: 'root',
})
@Injectable()

export class LoginService {

    private userContextData = new BehaviorSubject<UserContext>(
        new UserContext('', '', false));
      userContext = this.userContextData.asObservable();
    
    constructor(
        private http: HttpClient,
        private utilityService: CommonUtilityService
      ) {}

    changeUserContext(data: UserContext) {
    this.userContextData.next(data);
  }

  fetchToken(req) {

    const request  = new HttpParams()
        .set('client_id', 'trusted_client')
        .set('client_secret', 'secret')
        .set('grant_type', 'client_credentials')
        .set('scope', 'extended')
        .set('username', req.email)
        //.set('password', req.password);

    let httpHeaders = new HttpHeaders()
        .set('Content-Type', 'application/x-www-form-urlencoded')
        .set('Accept-Charset', 'UTF-8'); 

    let options = {
        headers: httpHeaders
    };
    let tokenUrl = environment.tokenEndpoint;

    return this.utilityService.postRequest(tokenUrl, request, options)
  }

  userLogin(req) {

     const request  = new HttpParams()
        .set('client_id', 'trusted_client')
        .set('client_secret', 'secret')
        .set('grant_type', 'password')
        .set('scope', 'extended')
        .set('username', req.email)
        .set('password', req.password);

     let httpHeaders = new HttpHeaders()
        .set('Content-Type', 'application/x-www-form-urlencoded')
        .set('Accept-Charset', 'UTF-8'); 

     let options = {
         headers: httpHeaders
    };
    let tokenUrl = environment.tokenEndpoint

    return this.utilityService.postRequest(tokenUrl, request, options)
  }

  fetchUserDetails = (token, username) => {

    let httpHeaders = new HttpHeaders()
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer${token}`)

    let options = {
        headers: httpHeaders
    };
    let loginUrl = environment.loginEndpoint + username + '?'

    return this.utilityService.getRequest(loginUrl, options)
  }

  activateProfile = (token, username, otp) => {

    let httpHeaders = new HttpHeaders()
        .set('Content-Type', 'application/x-www-form-urlencoded')
        .set('Authorization', `Bearer${token}`)

    let options = {
        headers: httpHeaders
    };

    const request  = new HttpParams()
        .set('otp', otp)

    let url = `${environment.loginEndpoint}${username}/profile-activate`

    return this.utilityService.postRequest(url, request, options)
  }

  fetchOrderDetails = (token, username) => {

    let httpHeaders = new HttpHeaders()
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer${token}`)

    let options = {
        headers: httpHeaders
    };
    let loginUrl = environment.loginEndpoint + username + '/orders'

    return this.utilityService.getRequest(loginUrl, options)
  }
}

export class UserContext {
    constructor(public displayUID: string, public displayName: string, public isAuthenticated: boolean) {
      this.displayUID = displayUID;
      this.displayName = displayName;
      this.isAuthenticated = isAuthenticated;
    }
  }

  export const INVALID_CREDENTIAL = 'Invalid User Or Password';
  export const INVALID_OTP = 'Invalid OTP';
  export const EMAIL_LINK_EXPIRED = 'Email link expired';
  export const USER_NOT_FOUND = 'Cannot find the User';
