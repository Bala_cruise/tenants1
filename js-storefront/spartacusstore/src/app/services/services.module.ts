import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { CommonUtilityService } from './common-utility-service';
import { HomePageService } from './home-page.service';
import { HomeResolverService } from './home-resolver.service';
import { CartResolverService } from './cart-resolver.service';
import { RegisterUserService } from './register-user.service';
import { LoginService } from './login.service';
import { CheckoutService } from './checkout.service';
import { PdpService } from './pdp.service';
import {AuthGuard } from './auth-guard.service'
import { CartService } from './cart.service';
import { LoaderService } from './loader.service';
import { EmitterService } from './emitter.service';
import { VerifyAdminService } from './verify-admin.service';
import { SuperAdminService } from './super-admin.service';

@NgModule({
    imports: [
        CommonModule,
        HttpClientModule,
        RouterModule
    ],
    exports: [],
    declarations: [],
    providers: [
        CommonUtilityService,
        HomePageService,
        HomeResolverService,
        CartResolverService,
        RegisterUserService,
        LoginService,
        CheckoutService,
        PdpService,
        AuthGuard,
        CartService,
        LoaderService,
        EmitterService,
        VerifyAdminService,
        SuperAdminService
    ],
})
export class ServicesModule { }
