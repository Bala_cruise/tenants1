import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable, BehaviorSubject } from 'rxjs';
import { CommonUtilityService } from '../services/common-utility-service';
import { environment } from '../../environments/environment';
import { map, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
@Injectable()
export class CartService {
  private cartContextData = new BehaviorSubject<CartContext>(
    new CartContext(false)
  );
  cartContext = this.cartContextData.asObservable();

  constructor(private utilityService: CommonUtilityService) {}

  changeCartContext(data: CartContext) {
    this.cartContextData.next(data);
  }

  getCart = () => {
    let cartApi = `${environment.cartAPI +
      this.utilityService.getCookie(
        'displayUid'
      )}/carts/${this.utilityService.getCookie('cartId')}`;

    let httpHeaders = new HttpHeaders()
      .set('Content-Type', 'application/json')
      .set('Authorization', `Bearer ${this.utilityService.getCookie('AuthToken')}`);

    let options = {
      headers: httpHeaders,
    };

    return this.utilityService.getRequest(cartApi, options);
  };

  updateCart = (qty, cartEntry) => {
    let httpHeaders = new HttpHeaders()
      .set('Content-Type', 'application/x-www-form-urlencoded')
      .set('Authorization', `Bearer ${this.utilityService.getCookie('AuthToken')}`);

    let options = {
      headers: httpHeaders,
    };

    const request = new HttpParams().set('qty', qty);

    let cartApi = `${environment.cartAPI +
      this.utilityService.getCookie(
        'displayUid'
      )}/carts/${this.utilityService.getCookie('cartId')}/entries/${cartEntry}`;

    return this.utilityService.patchRequest(cartApi, request, options);
  };

  deleteCart = entry => {
    let httpHeaders = new HttpHeaders().set(
      'Authorization',
      `Bearer ${this.utilityService.getCookie('AuthToken')}`
    );

    let options = {
      headers: httpHeaders,
    };

    let deleteCartApi = `${environment.cartAPI +
      this.utilityService.getCookie(
        'displayUid'
      )}/carts/${this.utilityService.getCookie('cartId')}/entries/${entry}`;

    return this.utilityService.deleteRequest(deleteCartApi, options);
  };
}

export class CartContext {
  constructor(public updateCart: any) {
    this.updateCart = updateCart;
  }
}

export const CART_UPDATE_FAILURE_MSG =
  'Couldnt update the cart. Please try again';
export const UPDATE_CART = true;
