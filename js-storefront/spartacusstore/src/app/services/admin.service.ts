import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Router  } from '@angular/router';
import { CommonUtilityService } from '../services/common-utility-service';
import { environment } from '../../environments/environment';
import { LoginService } from './login.service'
import { EmitterService } from './emitter.service'

@Injectable({
  providedIn: 'root',
})
@Injectable()
export class AdminService {

    userContext: any

    constructor(
        private utilityService: CommonUtilityService,
        private loginService: LoginService,
        private emitter: EmitterService,
        private router: Router) {

        this.loginService.userContext.subscribe(context => {
            this.userContext = context;
          });
    }

    submitForm = request => {
        // const request = new HttpParams()
        // .set('cartId', this.utilityService.getCookie('cartId'))

        

        let httpHeaders = new HttpHeaders()
        .set(
        'Authorization',
        `Bearer ${this.utilityService.getCookie('AuthToken')}`
        )
        .set('Content-Type', 'application/json');

        let options = {
        headers: httpHeaders,
        };

        let url = `${environment.superAdmin.hrCreateUserAPI + this.utilityService.getCookie('displayUid')}/masterdatasetup`;

        return this.utilityService.postRequest(url, request, options);
    }

    downloadFile = () => {

        let httpHeaders = new HttpHeaders().set(
            'Authorization',
            `Bearer ${this.utilityService.getCookie('AuthToken')}`
        );

        let options = {
            headers: httpHeaders,
        };

        let url = environment.adminDownloadApi

        return this.utilityService.getRequest(url, options)
    }

    onLogout = () => {
        
        this.userContext.displayUID = ''
        this.userContext.displayName = ''
        this.userContext.isAuthenticated = false;
    
        this.utilityService.removeCookie('isAuthenticated');
        this.utilityService.removeCookie('displayUid');
        this.utilityService.removeCookie('userName');
        this.utilityService.removeCookie('cartId');
        this.utilityService.removeCookie('isSuperAdmin');
        this.utilityService.removeCookie('superAdminOrders');
        this.loginService.changeUserContext(this.userContext);
        this.emitter.superAdminSource.next(true);
        this.router.navigateByUrl('/');
      }
  
}

export const ADMIN_NAV_LINKS = [
    {
        'name': 'hrms',
        'imgUrl': '../../assets/images/finance.png',
        'active': true,
        'routerLink': 'hrms'
    },
    {
        'name': 'Finance',
        'imgUrl': '../../assets/images/hr.png',
        'active': true,
        'routerLink': 'finance'
    },
    {
        'name': 'admin',
        'imgUrl': '../../assets/images/finance.png',
        'active': false,
        'routerLink': 'hrms'
    },
    {
        'name': 'admin',
        'imgUrl': '../../assets/images/finance.png',
        'active': false,
        'routerLink': 'finance'
    }
]

export const SETUP_NAV_LINKS = [
    {
        'name': 'Master data Setup',
        'imgUrl': '../../assets/images/icn_masterdata.png',
        'HoverImgUrl': '../../assets/images/icn_masterdataicn_masterdata_black.png',
        'url': '../../assets/images/icn_masterdata.png',
        'active': true,
    },
    {
        'name': 'HR Policies Setup',
        'imgUrl': '../../assets/images/icn_hrpoliciessetup.png',
        'HoverImgUrl': '../../assets/images/icn_hrpoliciessetupicn_hrpoliciessetup_blacksmall.png',
        'url': '../../assets/images/icn_hrpoliciessetup.png',
        'active': true,
    },
    {
        'name': 'Payroll Process',
        'imgUrl': '../../assets/images/icn_payrollprocess.png',
        'HoverImgUrl': '../../assets/images/icn_payrollprocessicn_payrollprocess_black.png',
        'url': '../../assets/images/icn_payrollprocess.png',
        'active': false,
    },
    {
        'name': 'Recruitment',
        'imgUrl': '../../assets/images/icn_recruitment.png',
        'HoverImgUrl': '../../assets/images/icn_recruitmenticn_recruitment_black.png',
        'url': '../../assets/images/icn_recruitment.png',
        'active': false,
    }
]

export const REPORTS_FILTER = [
    'Report 1',
    'Report 2'
]

export const GENDERS = [
    'Male',
    'Female'
]

export const COUNTRIES = [
    'India',
    'Dubai',
    'Saudi Arabia'
]

export const MARITAL_STATUS = [
    'Married',
    'Single',
    'Divorced'
]

export const RELIGIOUS = [
    'Hindu',
    'Muslim',
    'Christian'
]

export const VALIDATION_MESSAGES = {
    fullName: [
        { type: 'required', message: 'Full name is require' },
        { type: 'name', message: 'Enter a valid name' }
    ],

    Fathername: [
      { type: 'required', message: 'Father name is required' },
      { type: 'name', message: 'Enter a valid name' },
    ],
    gender: [
      { type: 'required', message: 'Gender is required' }],
    IqamaNo: [{ type: 'required', message: 'IqamaNo is required' }],

    DOB: [{ type: 'required', message: 'Date of Birth is required' }],
    religion: [{ type: 'required', message: 'Religion is required' }],
    maritalStatus: [{ type: 'required', message: 'Marital Status is required' }],
    phoneNo: [{ type: 'required', message: 'Phone Number is required' }],
    email: [
        { type: 'required', message: 'Email is Required' },
        { type: 'email', message: 'Please enter valid email id' }
    ],
    nationality: [{ type: 'required', message: 'ZNationality is required' }], 
    empNo: [{ type: 'required', message: 'Employee Number is required' }],
    joiningDate: [{ type: 'required', message: 'Joining Date is required' }],
    jobTitle: [{ type: 'required', message: 'Job Title is required' }],
    grade: [{ type: 'required', message: 'Grade is required' }],
    department: [{ type: 'required', message: 'Department is required' }],
    division: [{ type: 'required', message: 'Division is required' }],
    supEmpId: [{ type: 'required', message: 'Supervisor Employee Id is required' }],
    location: [{ type: 'required', message: 'Location is required' }],
    iqamaProfession: [{ type: 'required', message: 'Profession Of Iqama is required' }],
    basicSalary: [{ type: 'required', message: 'Basic Salary is required' }],
    contractStatus: [{ type: 'required', message: 'Contract Status is required' }],
    bankName: [{ type: 'required', message: 'Bank Nmae is required' }],
    bankCode: [{ type: 'required', message: 'Bank Code is required' }],
    IBAN: [{ type: 'required', message: 'IBAN Number is required' }],

};