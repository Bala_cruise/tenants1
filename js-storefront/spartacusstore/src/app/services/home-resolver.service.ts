import { Injectable } from '@angular/core';
import { CommonUtilityService } from './common-utility-service';
import { environment } from '../../environments/environment'; 

import { Resolve } from '@angular/router';

@Injectable()
export class HomeResolverService implements Resolve<any> {
  constructor(private utilityService: CommonUtilityService) {}

  resolve() {
    return this.utilityService.getRequest(environment.homePageEndPoint, '');
  }
}