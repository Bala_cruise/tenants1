/**
 *
 */
package de.hybris.osaned.fulfilmentprocess.actions.order;

import de.hybris.osaned.core.event.ApproverEvent;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.commerceservices.event.AbstractCommerceUserEvent;
import de.hybris.platform.commerceservices.i18n.CommerceCommonI18NService;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.action.AbstractProceduralAction;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.event.EventService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.store.services.BaseStoreService;
import de.hybris.platform.task.RetryLaterException;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.util.CollectionUtils;


/**
 * @author balamurugan
 *
 */
public class ApproverProcessAction extends AbstractProceduralAction<OrderProcessModel>
{
	private final Logger LOG = Logger.getLogger(ApproverProcessAction.class);
	@Resource
	private UserService userService;
	@Resource
	private ConfigurationService configurationService;
	@Resource
	private EventService eventService;
	@Resource
	private BaseStoreService baseStoreService;
	@Resource
	private BaseSiteService baseSiteService;
	@Resource
	private CommerceCommonI18NService commerceCommonI18NService;

	@Override
	public void executeAction(final OrderProcessModel orderProcessModel) throws RetryLaterException, Exception
	{
		final String approverEmailId = configurationService.getConfiguration().getString("b2bcustomer.account.approver.emailid");
		final OrderModel orderModel = orderProcessModel.getOrder();
		final UserModel userModel = orderModel.getUser();
		if (userModel instanceof B2BCustomerModel)
		{
			final B2BCustomerModel superAdminCustomer = (B2BCustomerModel) userModel;
			final B2BCustomerModel approverPerson = userService.getUserForUID(approverEmailId, B2BCustomerModel.class);
			if (CollectionUtils.isEmpty(approverPerson.getSuperAdminEmployeeIds()))
			{
				approverPerson.setSuperAdminEmployeeIds(Collections.singleton(superAdminCustomer.getEmail()));
				getModelService().save(approverPerson);
			}
			else
			{
				final Set<String> superAdminEmployeeList = new HashSet<>(approverPerson.getSuperAdminEmployeeIds());
				superAdminEmployeeList.add(superAdminCustomer.getEmail());
				approverPerson.setSuperAdminEmployeeIds(superAdminEmployeeList);
				getModelService().save(approverPerson);
			}
			superAdminCustomer.setOrderApprovalStatus(Boolean.FALSE);
			getModelService().save(superAdminCustomer);
			eventService.publishEvent(initializeEvent(new ApproverEvent(), approverPerson));
		}

	}

	private AbstractCommerceUserEvent initializeEvent(final AbstractCommerceUserEvent event, final B2BCustomerModel customerModel)
	{
		event.setBaseStore(baseStoreService.getBaseStoreForUid("osaned"));
		event.setSite(baseSiteService.getBaseSiteForUID("osaned"));
		event.setCustomer(customerModel);
		event.setLanguage(commerceCommonI18NService.getCurrentLanguage());
		event.setCurrency(commerceCommonI18NService.getCurrentCurrency());
		return event;
	}

}
