/**
 *
 */
package de.hybris.osaned.v2.controller;

import de.hybris.osaned.dto.customer.CustomerWsDTO;
import de.hybris.osaned.facades.customer.OsanedFinanceB2BCustomerFacade;
import de.hybris.platform.commercefacades.customer.CustomerFacade;
import de.hybris.platform.commercefacades.user.data.B2BCustomerData;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.commercefacades.user.data.CustomerFinanceMasterSetupData;
import de.hybris.platform.commercefacades.user.data.FinanceManagerOpenBalanceData;
import de.hybris.platform.commerceservices.customer.DuplicateUidException;
import de.hybris.platform.webservicescommons.cache.CacheControl;
import de.hybris.platform.webservicescommons.cache.CacheControlDirective;
import de.hybris.platform.webservicescommons.swagger.ApiBaseSiteIdAndUserIdParam;
import de.hybris.platform.webservicescommons.swagger.ApiFieldsParam;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;


/**
 * @author balamurugan
 *
 */
@Controller
@RequestMapping(value = "/{baseSiteId}/finance-portal/users")
@CacheControl(directive = CacheControlDirective.PRIVATE)
@Api(tags = "Finance-Portal")
public class OsanedFinancePortalController extends BaseCommerceController
{
	private static final Logger LOG = Logger.getLogger(OsanedFinancePortalController.class);
	@Resource(name = "wsCustomerFacade")
	private CustomerFacade customerFacade;
	@Resource
	private OsanedFinanceB2BCustomerFacade osanedFinanceB2BCustomerFacade;

	@Secured(
	{ "ROLE_CUSTOMERGROUP", "ROLE_TRUSTED_CLIENT", "ROLE_CUSTOMERMANAGERGROUP" })
	@RequestMapping(value = "{userId}/register", method = RequestMethod.POST)
	@ApiOperation(nickname = "create-HRCustomer", value = "Create HR Customer", notes = "Returns customer data.")
	@ApiBaseSiteIdAndUserIdParam
	@ResponseBody
	public CustomerWsDTO createHRUser(@ApiParam(value = "Email Id", required = true)
	@RequestParam
	final String emailId, @ApiParam(value = "Name", required = true)
	@RequestParam
	final String name, @ApiParam(value = "Employee Id ", required = true)
	@RequestParam
	final String employeeId, @ApiParam(value = "Iqama Id", required = true)
	@RequestParam
	final String iqamaId, @ApiParam(value = "Customer Role", required = true)
	@RequestParam
	final String customerRole, @ApiParam(value = "Finance User Id", required = true)
	@PathVariable
	final String userId, @ApiParam(value = "Verification User Id", required = true)
	@RequestParam
	final String verificationUser, @ApiFieldsParam
	@RequestParam(defaultValue = DEFAULT_FIELD_SET)
	final String fields) throws DuplicateUidException
	{
		Assert.hasText(emailId, "The field [emailId] cannot be empty");
		Assert.hasText(name, "The field [name] cannot be empty");
		Assert.hasText(employeeId, "The field [employeeId] cannot be empty");
		Assert.hasText(iqamaId, "The field [iqamaId] cannot be empty");
		Assert.hasText(customerRole, "The field [customerRole] cannot be empty");
		Assert.hasText(verificationUser, "The field [verificationUser] cannot be empty");
		final B2BCustomerData b2bCustomerData = createRegisterdata(emailId, name, employeeId, iqamaId, customerRole,
				verificationUser, userId);
		osanedFinanceB2BCustomerFacade.createFinanceCustomer(b2bCustomerData);
		final CustomerData customerData = customerFacade.getUserForUID(emailId);
		return getDataMapper().map(customerData, CustomerWsDTO.class, fields);
	}

	/**
	 * @param emailId
	 * @param name
	 * @param employeeId
	 * @param iqamaId
	 * @return
	 */
	private B2BCustomerData createRegisterdata(final String emailId, final String name, final String employeeId,
			final String iqamaId, final String customerRole, final String verificationUser, final String financeCustomerId)
	{
		final B2BCustomerData b2bCustomerData = new B2BCustomerData();
		b2bCustomerData.setEmail(emailId);
		b2bCustomerData.setName(name);
		b2bCustomerData.setEmployeeId(employeeId);
		b2bCustomerData.setIqamaId(iqamaId);
		b2bCustomerData.setCompany(b2bCustomerData.getEmployeeId());
		b2bCustomerData.setRole(customerRole);
		b2bCustomerData.setFinaceCustomerId(financeCustomerId);
		b2bCustomerData.setVerificationUser(verificationUser);
		return b2bCustomerData;
	}

	@Secured(
	{ "ROLE_CUSTOMERGROUP", "ROLE_TRUSTED_CLIENT", "ROLE_CUSTOMERMANAGERGROUP" })
	@RequestMapping(value = "{userId}/setAuthMatrix", method = RequestMethod.PUT)
	@ApiOperation(nickname = "Set Authorization Matrix for User", value = " Authorization Matrix", notes = "Returns customer data.")
	@ApiBaseSiteIdAndUserIdParam
	@ResponseBody
	public CustomerWsDTO setAuthMatrixForUser(@ApiParam(value = "Level-1 Approver", required = false)
	@RequestParam
	final String approverLevel1, @ApiParam(value = "Level-2 Approver", required = false)
	@RequestParam
	final String approverLevel2, @ApiParam(value = "Level-3 Approver", required = false)
	@RequestParam
	final String approverLevel3, @ApiParam(value = "userId", required = true)
	@PathVariable
	final String userId, @ApiFieldsParam
	@RequestParam(defaultValue = DEFAULT_FIELD_SET)
	final String fields) throws DuplicateUidException
	{
		osanedFinanceB2BCustomerFacade.setAuthMatrixForUser(userId, approverLevel1, approverLevel2, approverLevel3);
		final CustomerData customerData = customerFacade.getUserForUID(userId);
		return getDataMapper().map(customerData, CustomerWsDTO.class, fields);
	}

	@Secured(
	{ "ROLE_CLIENT", "ROLE_TRUSTED_CLIENT", "ROLE_CUSTOMERMANAGERGROUP" })
	@RequestMapping(value = "/{userId}/customermasterdatasetup", method = RequestMethod.POST, consumes =
	{ MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
	@ResponseStatus(value = HttpStatus.CREATED)
	@ResponseBody
	@ApiOperation(nickname = "FinanceCustomerMasterDataSetupToCPI", value = "Finance SuperAdmin submits the data to CPI", notes = "To send data to CPI . Requires the following "
			+ "Customer : bpCode,name,address,vatNumber,currency,financialGroup"
			+ "Supplier : bpCode,name,address,vatNumber,currency,financialGroup"
			+ "tem : itemCode,description,itemType,itemGroup,unitSet,inventoryUnit,currency")
	@ApiBaseSiteIdAndUserIdParam
	public CustomerWsDTO customerFinanceMasterSetup(@ApiParam(value = "userId", required = true)
	@PathVariable
	final String userId, @ApiParam(value = "Customer FinanceMasterSetup Object", required = true)
	@RequestBody
	final CustomerFinanceMasterSetupData customerFinanceMasterSetupData, @ApiFieldsParam
	@RequestParam(defaultValue = DEFAULT_FIELD_SET)
	final String fields, final HttpServletRequest httpRequest, final HttpServletResponse httpResponse)
	{
		osanedFinanceB2BCustomerFacade.setCustomerMasterData(customerFinanceMasterSetupData, userId);
		final CustomerData customerData = customerFacade.getUserForUID(userId);
		return getDataMapper().map(customerData, CustomerWsDTO.class, fields);
	}

	@Secured(
	{ "ROLE_CLIENT", "ROLE_TRUSTED_CLIENT", "ROLE_CUSTOMERMANAGERGROUP" })
	@RequestMapping(value = "/{userId}/customermasterdatasetup", method = RequestMethod.GET)
	@ResponseBody
	@ApiOperation(nickname = "GET FinanceCustomerMasterDataSetup", value = " Retrieve Finance SuperAdmin", notes = "Get FinanceCustomerMasterDataSetup from Finane SuperAdmin ")
	@ApiBaseSiteIdAndUserIdParam
	public CustomerFinanceMasterSetupData getFinanceMasterSetup(@ApiParam(value = "userId", required = true)
	@PathVariable
	final String userId, @ApiFieldsParam
	@RequestParam(defaultValue = DEFAULT_FIELD_SET)
	final String fields, final HttpServletRequest httpRequest, final HttpServletResponse httpResponse)
	{
		final CustomerFinanceMasterSetupData customerFinanceMasterSetupData = osanedFinanceB2BCustomerFacade
				.getCustomerMasterData(userId);
		return getDataMapper().map(customerFinanceMasterSetupData, CustomerFinanceMasterSetupData.class, fields);
	}

	@Secured(
	{ "ROLE_CLIENT", "ROLE_TRUSTED_CLIENT", "ROLE_CUSTOMERMANAGERGROUP" })
	@RequestMapping(value = "/{userId}/customermasterdatasetup", method = RequestMethod.PUT, consumes =
	{ MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
	@ResponseStatus(value = HttpStatus.CREATED)
	@ResponseBody
	@ApiOperation(nickname = "Update FinanceCustomerMasterDataSetupToCPI", value = "Update FinanceCustomerMasterDataSetupToCPI", notes = "To send data to CPI . Requires the following "
			+ "Customer : bpCode,name,address,vatNumber,currency,financialGroup"
			+ "Supplier : bpCode,name,address,vatNumber,currency,financialGroup"
			+ "Item : itemCode,description,itemType,itemGroup,unitSet,inventoryUnit,currency")
	@ApiBaseSiteIdAndUserIdParam
	public CustomerWsDTO updateFinanceMasterSetup(@ApiParam(value = "userId", required = true)
	@PathVariable
	final String userId, @ApiParam(value = "Customer FinanceMasterSetup Object", required = true)
	@RequestBody
	final CustomerFinanceMasterSetupData customerFinanceMasterSetupData, @ApiFieldsParam
	@RequestParam(defaultValue = DEFAULT_FIELD_SET)
	final String fields, final HttpServletRequest httpRequest, final HttpServletResponse httpResponse)
	{
		osanedFinanceB2BCustomerFacade.updateCustomerMasterData(customerFinanceMasterSetupData, userId);
		final CustomerData customerData = customerFacade.getUserForUID(userId);
		return getDataMapper().map(customerData, CustomerWsDTO.class, fields);
	}

	@Secured(
	{ "ROLE_CLIENT", "ROLE_TRUSTED_CLIENT", "ROLE_CUSTOMERMANAGERGROUP" })
	@RequestMapping(value = "/{userId}/finance-openbalance", method = RequestMethod.POST, consumes =
	{ MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
	@ResponseStatus(value = HttpStatus.CREATED)
	@ResponseBody
	@ApiOperation(nickname = "FinanceOpenBalanceToCPI", value = "FinanceManager submits the data to CPI", notes = "To send data to CPI . Requires the following "
			+ "ItemUpload : itemCode,itemDescription,itemGroup,warehouse,inventoryUnit,quantity,price"
			+ "BPTransUpload : documentNumber,documentDate,businessPartner,invoiceNumber,currency,amountInInvoiceCurrency,ammountInHC,balanceAmount,reference"
			+ "OpeningBalance -> date, ledger,dr,amount,reference")
	@ApiBaseSiteIdAndUserIdParam
	public CustomerWsDTO createFinanceOpenBalance(@ApiParam(value = "userId", required = true)
	@PathVariable
	final String userId, @ApiParam(value = "Employe MasterDataSetup Object", required = true)
	@RequestBody
	final FinanceManagerOpenBalanceData financeManagerOpenBalanceData, @ApiFieldsParam
	@RequestParam(defaultValue = DEFAULT_FIELD_SET)
	final String fields, final HttpServletRequest httpRequest, final HttpServletResponse httpResponse)
	{
		osanedFinanceB2BCustomerFacade.setFinanceOpenBalance(financeManagerOpenBalanceData, userId);
		final CustomerData customerData = customerFacade.getUserForUID(userId);
		return getDataMapper().map(customerData, CustomerWsDTO.class, fields);
	}

}
