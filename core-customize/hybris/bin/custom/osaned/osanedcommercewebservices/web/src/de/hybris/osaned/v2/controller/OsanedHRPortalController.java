/**
 *
 */
package de.hybris.osaned.v2.controller;

import de.hybris.osaned.core.exception.CPIConnectionFailureException;
import de.hybris.osaned.dto.customer.CustomerWsDTO;
import de.hybris.osaned.facades.customer.OsanedB2BCustomerFacade;
import de.hybris.osaned.facades.customer.OsanedHRB2BCustomerFacade;
import de.hybris.platform.commercefacades.customer.CustomerFacade;
import de.hybris.platform.commercefacades.user.data.B2BCustomerData;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.commercefacades.user.data.EmployeeMasterSetupData;
import de.hybris.platform.commerceservices.customer.DuplicateUidException;
import de.hybris.platform.commercewebservicescommons.dto.user.UserWsDTO;
import de.hybris.platform.webservicescommons.cache.CacheControl;
import de.hybris.platform.webservicescommons.cache.CacheControlDirective;
import de.hybris.platform.webservicescommons.swagger.ApiBaseSiteIdAndUserIdParam;
import de.hybris.platform.webservicescommons.swagger.ApiBaseSiteIdParam;
import de.hybris.platform.webservicescommons.swagger.ApiFieldsParam;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;


/**
 * @author Bala Murugan
 *
 */
@Controller
@RequestMapping(value = "/{baseSiteId}/hr-portal/users")
@CacheControl(directive = CacheControlDirective.PRIVATE)
@Api(tags = "HR-Portal")
public class OsanedHRPortalController extends BaseCommerceController
{
	private static final Logger LOG = Logger.getLogger(OsanedHRPortalController.class);
	@Resource(name = "wsCustomerFacade")
	private CustomerFacade customerFacade;
	@Resource
	private OsanedHRB2BCustomerFacade osanedHRB2BCustomerFacade;
	@Resource
	private OsanedB2BCustomerFacade osanedB2BCustomerFacade;


	@Secured(
	{ "ROLE_CUSTOMERGROUP", "ROLE_TRUSTED_CLIENT", "ROLE_CUSTOMERMANAGERGROUP" })
	@RequestMapping(value = "{userId}/register", method = RequestMethod.POST)
	@ApiOperation(nickname = "create-HRCustomer", value = "Create HR Customer", notes = "Returns all customer data.")
	@ApiBaseSiteIdAndUserIdParam
	@ResponseBody
	public CustomerWsDTO createHRUser(@ApiParam(value = "Email Id", required = true)
	@RequestParam
	final String emailId, @ApiParam(value = "Name", required = true)
	@RequestParam
	final String name, @ApiParam(value = "Employee Id ", required = true)
	@RequestParam
	final String employeeId, @ApiParam(value = "Iqama Id", required = true)
	@RequestParam
	final String iqamaId, @ApiParam(value = "Customer Role", required = true)
	@RequestParam
	final String customerRole, @ApiParam(value = "Hr User Id", required = true)
	@PathVariable
	final String userId, @ApiFieldsParam
	@RequestParam(defaultValue = DEFAULT_FIELD_SET)
	final String fields) throws DuplicateUidException
	{
		Assert.hasText(emailId, "The field [emailId] cannot be empty");
		Assert.hasText(name, "The field [name] cannot be empty");
		Assert.hasText(employeeId, "The field [employeeId] cannot be empty");
		Assert.hasText(iqamaId, "The field [iqamaId] cannot be empty");
		Assert.hasText(customerRole, "The field [iqamaId] cannot be empty");
		final B2BCustomerData b2bCustomerData = createRegisterdata(emailId, name, employeeId, iqamaId, customerRole, userId);
		osanedHRB2BCustomerFacade.registerHRCustomer(b2bCustomerData);
		final CustomerData customerData = customerFacade.getUserForUID(emailId);
		return getDataMapper().map(customerData, CustomerWsDTO.class, fields);
	}

	/**
	 * @param emailId
	 * @param name
	 * @param employeeId
	 * @param iqamaId
	 * @return
	 */
	private B2BCustomerData createRegisterdata(final String emailId, final String name, final String employeeId,
			final String iqamaId, final String customerRole, final String hrCustomerId)
	{
		final B2BCustomerData b2bCustomerData = new B2BCustomerData();
		b2bCustomerData.setEmail(emailId);
		b2bCustomerData.setName(name);
		b2bCustomerData.setEmployeeId(employeeId);
		b2bCustomerData.setIqamaId(iqamaId);
		b2bCustomerData.setCompany(b2bCustomerData.getEmployeeId());
		b2bCustomerData.setRole(customerRole);
		b2bCustomerData.setHrCustomerId(hrCustomerId);
		return b2bCustomerData;
	}

	@Secured(
	{ "ROLE_CLIENT", "ROLE_TRUSTED_CLIENT", "ROLE_CUSTOMERMANAGERGROUP" })
	@RequestMapping(value = "/{userId}/masterdatasetup", method = RequestMethod.POST, consumes =
	{ MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
	@ResponseStatus(value = HttpStatus.CREATED)
	@ResponseBody
	@ApiOperation(nickname = "MasterDataSetupToCPI", value = "HRAdmin/Admin submits the data to CPI", notes = "To send data to CPI . Requires the following "
			+ "personalInfo :firstName,lastName,fatherName,gender,iqamaNo,nationality,dateofBirth,maritalStatus,religion"
			+ "employmentInfo :employeeNumber,joiningDate,professionOnIqama,jobTilte,grade,department,division,supervisorEmpID,location"
			+ "contractDetails : bassicSalary,contractStatus" + "contact : mobileNumber,emailAddress"
			+ "contractDetails : bassicSalary,contractStatus" + "bankDetails : bankName,bankCode,iBAN")
	@ApiBaseSiteIdAndUserIdParam
	public CustomerWsDTO createUser(@ApiParam(value = "userId", required = true)
	@PathVariable
	final String userId, @ApiParam(value = "Employe MasterDataSetup Object", required = true)
	@RequestBody
	final EmployeeMasterSetupData employeeMasterSetupData, @ApiFieldsParam
	@RequestParam(defaultValue = DEFAULT_FIELD_SET)
	final String fields, final HttpServletRequest httpRequest, final HttpServletResponse httpResponse)
	{
		osanedHRB2BCustomerFacade.sendEmployeeMasterDataSetupToCPI(employeeMasterSetupData, userId);
		final CustomerData customerData = customerFacade.getUserForUID(userId);
		return getDataMapper().map(customerData, CustomerWsDTO.class, fields);
	}

	@Secured(
	{ "ROLE_CLIENT", "ROLE_TRUSTED_CLIENT", "ROLE_CUSTOMERMANAGERGROUP" })
	@RequestMapping(value = "/{userId}/retriggerEmail", method = RequestMethod.POST)
	@ResponseBody
	@ApiOperation(nickname = "ReTrigger Email to InactiveUser", value = "send mail", notes = "send mail to user to activate profile")
	@ApiBaseSiteIdAndUserIdParam
	public UserWsDTO retriggerEmail(@ApiParam(value = "userId", required = true)
	@PathVariable
	final String userId, @ApiFieldsParam
	@RequestParam(defaultValue = DEFAULT_FIELD_SET)
	final String fields)
	{
		osanedHRB2BCustomerFacade.retriggerEmailtoEmployee(userId);
		final CustomerData customerData = customerFacade.getUserForUID(userId);
		return getDataMapper().map(customerData, CustomerWsDTO.class, fields);
	}

	@Secured(
	{ "ROLE_CUSTOMERGROUP", "ROLE_TRUSTED_CLIENT", "ROLE_CUSTOMERMANAGERGROUP" })
	@RequestMapping(value = "/{userId}/customerInfo", method = RequestMethod.GET)
	@ResponseBody
	@ApiOperation(nickname = "getUser", value = "Get customer profile", notes = "Returns customer profile.")
	@ApiBaseSiteIdAndUserIdParam
	public CustomerWsDTO getUser(@ApiParam(value = "userId", required = true)
	@PathVariable
	final String userId, @ApiFieldsParam
	@RequestParam(defaultValue = DEFAULT_FIELD_SET)
	final String fields)
	{
		final CustomerData customerData = customerFacade.getUserForUID(userId);
		return getDataMapper().map(customerData, CustomerWsDTO.class, fields);
	}

	@Secured(
	{ "ROLE_CLIENT", "ROLE_TRUSTED_CLIENT", "ROLE_CUSTOMERMANAGERGROUP" })
	@RequestMapping(value = "/{userId}/update-customer", method = RequestMethod.PUT)
	@ResponseBody
	@ApiOperation(hidden = true, value = " Update a customer", notes = "Update a customer.it requires "
			+ "the following parameters: email,name,contact,company,sizeRange,commercialRegistration")
	@ApiBaseSiteIdParam
	public CustomerWsDTO createUser(@ApiParam(value = "Customer's Id.")
	@RequestParam(required = true)
	final String customerId, @ApiParam(value = "Customer's Id.")
	@RequestParam(required = true)
	final String email, @ApiParam(value = "Customer's name.", required = true)
	@RequestParam
	final String name, @ApiParam(value = "Customer's contact ")
	@RequestParam(required = false)
	final String contact, @ApiParam(value = "Customer's company.")
	@RequestParam(required = false)
	final String company, @ApiParam(value = "Customer's sizeRange.")
	@RequestParam(required = false)
	final String sizeRange, @ApiParam(value = "Customer's commercialRegistration.")
	@RequestParam(required = false)
	final String commercialRegistration, @ApiParam(value = "userId", required = true)
	@PathVariable
	final String userId, @ApiParam(value = "Guest order's guid.")
	@RequestParam(required = false)
	final String guid, @ApiFieldsParam
	@RequestParam(defaultValue = DEFAULT_FIELD_SET)
	final String fields) throws DuplicateUidException, CPIConnectionFailureException
	{
		final B2BCustomerData b2bCustomerData = setCustomerData(customerId, userId, email, name, contact, sizeRange, company,
				commercialRegistration);
		osanedB2BCustomerFacade.updateB2BCustomer(b2bCustomerData);
		final CustomerData customerData = customerFacade.getUserForUID(userId);
		return getDataMapper().map(customerData, CustomerWsDTO.class, fields);
	}

	/**
	 * @param userId
	 * @param email
	 * @param name
	 * @param contact
	 * @param sizeRange
	 * @param commercialRegistration
	 */
	private B2BCustomerData setCustomerData(final String customerId, final String userId, final String email, final String name,
			final String contact, final String sizeRange, final String company, final String commercialRegistration)
	{
		final B2BCustomerData b2bCustomerData = new B2BCustomerData();
		b2bCustomerData.setCustomerId(customerId);
		b2bCustomerData.setUid(userId);
		b2bCustomerData.setEmail(email);
		b2bCustomerData.setName(name);
		b2bCustomerData.setContact(contact);
		b2bCustomerData.setCompany(company);
		b2bCustomerData.setSizeRange(sizeRange);
		b2bCustomerData.setCommercialRegistration(commercialRegistration);
		return b2bCustomerData;
	}
}
