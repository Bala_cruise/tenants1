<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>


<%-- <footer>
    <cms:pageSlot position="Footer" var="feature">
        <cms:component component="${feature}"/>
    </cms:pageSlot>
</footer>--%>
<!---------------------------------------- Footer Starts ----------------------------->
  <!---------------------------------------- Footer Starts ----------------------------->
  <footer class="secondary-bg footer-container color-scheme-light">
    <div class="container main-footer">
      <aside class="col-xs-12 footer-sidebar widget-area" role="complementary">
        <div class="clearfix visible-lg-block"></div>
        <div class="footer-column footer-column-2 col-md-3 col-sm-6">
          <div id="text-18" class="footer-widget  footer-widget-collapse widget_text">
            <h5 class="widget-title primary-color">Quick Links</h5>
            <div class="textwidget">
              <ul class="menu list-unstyled">
                <li><a href="#">Why Saned</a></li>
                <li><a href="#">Intelligent Enterprise</a></li>
                <li><a href="#">Small and Midsize Enterprises</a></li>
                <li><a href="#">Finance</a></li>
                <li><a href="#">Saned Trust Center</a></li>
                <li><a href="#">Saned Community</a></li>
                <li><a href="#">Developer</a></li>
                <li><a href="#">Support Portal</a></li>
              </ul>
            </div>
          </div>
        </div>
        <div class="footer-column footer-column-3 col-md-3 col-sm-6">
          <div id="text-19" class="footer-widget  footer-widget-collapse widget_text">
            <h5 class="widget-title primary-color">About Saned</h5>
            <div class="textwidget">
              <ul class="menu list-unstyled">
                <li><a href="#">Company Information</a></li>
                <li><a href="#">Worldwide Directory Collection</a></li>
                <li><a href="#">Investors Relations</a></li>
                <li><a href="#">Careers</a></li>
                <li><a href="#">News and Press</a></li>
                <li><a href="#">Events</a></li>
                <li><a href="#">Customer Stories</a></li>
                <li><a href="#">Newsletter</a></li>
              </ul>
            </div>
          </div>
        </div>
        <div class="clearfix visible-sm-block"></div>
        <div class="footer-column footer-column-4 col-md-3 col-sm-6">
          <div id="text-20" class="footer-widget  footer-widget-collapse widget_text">
            <h5 class="widget-title primary-color">Site Information</h5>
            <div class="textwidget">
              <ul class="menu list-unstyled">
                <li><a href="#">Privacy</a></li>
                <li><a href="#">Terms of Use</a></li>
                <li><a href="#">Legal Disclosure</a></li>
                <li><a href="#">copyright</a></li>
                <li><a href="#">Trademark</a></li>
                <li><a href="#">Sitemap</a></li>
                <li><a href="#">Text View</a></li>
                <li><a href="#">Cookie Preferences</a></li>
              </ul>
            </div>
          </div>
        </div>
        <div class="footer-column footer-column-5 col-md-3 col-sm-6">
          <div id="text-21" class="footer-widget footer-widget-collapse widget_text">
            <h5 class="widget-title primary-color">Contact US</h5>
            <div class="textwidget">
              <ul class="menu list-unstyled">
                <li class="text-white"><a href="#"><i class="glyphicon glyphicon-map-marker primary-color"></i> Nirit76 Golan Street Nirit Israek</a></li>
                <li class="text-white"><a href="#"><i class="top-icon glyphicon glyphicon-earphone primary-color"></i> +972-77-9322636</a></li>
                <li class="text-white"><a href="#"><i class="top-icon glyphicon glyphicon-envelope primary-color"></i> info@saned.com</a></li>
              </ul>
            </div>
          </div>
        </div>
      </aside>
    </div>
    <div class="copyrights-wrapper text-center">
      <div class="container">
        <div class="min-footer">
          <div class="copyright-text text-center">Copyright � 2020, company name. All Rights Reserved </div>
        </div>
      </div>
    </div>
  </footer>
