<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>

<template:page pageTitle="${pageTitle}">
	 <!-------------------------------------Carousel Starts-------------------------------->
  <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
      <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
      <li data-target="#carousel-example-generic" data-slide-to="1"></li>
      <li data-target="#carousel-example-generic" data-slide-to="2"></li>
    </ol>
  
    <!-- Wrapper for slides -->
     <c:set var="selected" value="1"/>
    <cms:pageSlot position="Section1" var="feature">
    	  <div class="carousel-inner" role="listbox">
	    	<c:if test="${not empty feature.banners}">
	    		<c:forEach items="${feature.banners}" var="banner">
	    			<c:if test="${not empty banner.media and not empty banner.headline and not empty banner.content }">
	    				<c:choose>
	    					<c:when test="${selected eq 1}">
		    					<div class="item active">
						        <img src="${banner.media.url}" alt="slideimage2">
						        <div class="carousel-caption carousel-heading"> 
						          <h1 class="primary-color font-semi-bold">${banner.headline}</h2>
						          <p>${banner.content}</p>
						         <!--  <button type="submit" class="margin-auto btn btn-primary">Learn more</button> -->
						        </div>
						    	</div>
						    	<c:set var="selected" value="0"/>
	    					</c:when>
	    					<c:otherwise>
							   <div class="item">
						        <img src="${banner.media.url}" alt="slideimage2">
						        <div class="carousel-caption carousel-heading"> 
						         <h1 class="primary-color font-semi-bold">${banner.headline}</h2>
						          <p>${banner.content}</p>
<!-- 						         <button type="submit" class="margin-auto btn btn-primary">Learn more</button>
 -->						        </div>
						      </div>
	    					</c:otherwise>
	    				</c:choose>
	    			</c:if>
	    		</c:forEach>
	    	</c:if>
    	</div>
    </cms:pageSlot>
    
    <!-- Controls -->
    <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
  <!-------------------------------------Carousel Starts-------------------------------->

  <div class="secondary-bg">
    <section class="container">
      <div class="row icon_section mb-30">
        <h2 class="text-white text-center font-semi-bold">SANED SOLUTION</h2>
			<cms:pageSlot position="Section2A" var="feature">
				<c:if test="${not empty feature.media}">
					<div class="col-lg-3 col-sm-6 col-xs-6 clearfix">
			          <div class="icon-box text-center">
			            <a href="">
			              <span class="icon-box-icon text-dark">
			                <img src="${feature.media.url}" alt="finance" />
			              </span>
			              <div class="icon-box-content">
			                <h3 class="icon-box-title text-white no-margin">${feature.headline}</h3><!-- End .icon-box-title -->
			              </div><!-- End .icon-box-content -->
			            </a>
			          </div><!-- End .icon-box -->
			        </div><!-- End .col-lg-3 col-sm-6 -->
				</c:if>
		   	</cms:pageSlot>
			<cms:pageSlot position="Section2B" var="feature">
				<c:if test="${not empty feature.media}">
					<div class="col-lg-3 col-sm-6 col-xs-6 clearfix">
			          <div class="icon-box text-center">
			            <a href="">
			              <span class="icon-box-icon text-dark">
			                <img src="${feature.media.url}" alt="finance" />
			              </span>
			              <div class="icon-box-content">
			                <h3 class="icon-box-title text-white no-margin">${feature.headline}</h3><!-- End .icon-box-title -->
			              </div><!-- End .icon-box-content -->
			            </a>
			          </div><!-- End .icon-box -->
			        </div><!-- End .col-lg-3 col-sm-6 -->
				</c:if>
			</cms:pageSlot>
      </div>
    </section>
  </div>

<!------------------------------watch the demo video---------------------->
<%-- <div class="container bg-white mb-30">
  <div class="row">
    <div class='col-md-offset-2 col-md-8 text-center icon_section semi-bold'>
      <h2>Watch the Demo Videos</h2>
    </div>
      <div class="col-xs-12 demo-video">
        <div class=""><img class="img-responsive" src="${themeResourcePath}/images/slideimage.jpg" /></div>
        <div class="play-img"><img class="img-responsive" src="${themeResourcePath}/images/play.png" /></div>

      </div>
  </div>
</div> --%>
<!-------------------------------------Testimonials------------------------------------------------>
<%-- <div class="secondary-bg">
  <div class="container">
    <div class="row icon_section clearfix">
      <div class='col-md-offset-2 col-md-8 text-center'>
        <h2 class="text-white font-semi-bold">TESTIMONIALS</h2>
      </div>
    </div>
    <div class='row'>
      <div class='col-md-offset-2 col-md-8'>
        <div class="carousel slide" data-ride="carousel" id="quote-carousel">
          <!-- Bottom Carousel Indicators -->
          <ol class="carousel-indicators">
            <li data-target="#quote-carousel" data-slide-to="0" class="active"></li>
            <li data-target="#quote-carousel" data-slide-to="1"></li>
            <li data-target="#quote-carousel" data-slide-to="2"></li>
          </ol>

          <!-- Carousel Slides / Quotes -->
          <c:set var="selected" value="1"/>
          <div class="carousel-inner">
			<cms:pageSlot position="Section3" var="feature">
			<c:if test="${not empty feature.banners}">
				<c:forEach items="${feature.banners}" var="banner">
					<c:if test="${not empty banner.media and not empty banner.headline and not empty banner.content}">
						<c:choose>
							<c:when test="${selected eq 1}">
								<div class="item active">
					              <blockquote>
					                <div class="row">
					                  <div class="col-lg-12 col-sm-9 text-center">
					                    <p class="text-white">${banner.content}</p>
					                    <small>${banner.headline}</small>
					                  </div>
					                  <div class="testimonial_img text-center">
					                    <img class="img-circle" src="${banner.media.url}"
					                      style="width: 100px;height:100px;">
					                    
					                  </div>
					                </div>
					              </blockquote>
					            </div>
					            <c:set var="selected" value="0"/>
							</c:when>
							<c:otherwise>
								<div class="item">
					              <blockquote>
					                <div class="row">
					                  <div class="col-lg-12 col-sm-9 text-center">
					                   <p class="text-white">${banner.content}</p>
					                    <small>${banner.headline}</small>
					                  </div>
					                  <div class="testimonial_img text-center">
					                    <img class="img-circle" src="${banner.media.url}"
					                      style="width: 100px;height:100px;">
					                  </div>
					                </div>
					              </blockquote>
					            </div>
							</c:otherwise>
						</c:choose>
					</c:if>
				</c:forEach>
			</c:if>
			</cms:pageSlot>
          </div>

          <!-- Carousel Buttons Next/Prev -->
          <a data-slide="prev" href="#quote-carousel" class="left carousel-control"><i
              class="glyphicon glyphicon-chevron-left"></i></a>
          <a data-slide="next" href="#quote-carousel" class="right carousel-control"><i
              class="glyphicon glyphicon-chevron-right"></i></a>
        </div>
      </div>
    </div>
  </div>
</div>
 --%>  <div class="container">
    <div class="clearfix icon_section mb-30">
      <h2 class="text-center">OUR VALUABLE CLIENTS</h2>
      <div class="row clearfix client-logos mb-50">
      	 <cms:pageSlot position="Section4" var="feature">
      		<c:if test="${not empty feature.media and not empty feature.media.medias}">
      			<c:forEach items="${feature.media.medias}" var="media">
      				<div class="col-xs-4 col-md-2">
          				<img class="img-responsive img-grayscale" src="${media.url}" />
       			 	</div>
      			</c:forEach>
      		</c:if>
      	</cms:pageSlot>
      </div>
      <!-- <img href="#" src="img/Moduleo-logo.png" /> -->
    </div>

  </div>
  <!-----------------Download App Starts------------------>

  <div class="col-xs-12 app_store_dn">
<%--     <div class="row clearfix icon_section mb-30">
      <h2 class="text-white text-center">DOWNLOAD OUR APP</h2>
      <div class="container clearfix">
      <div class="col-xs-12 col-md-6 left_download_section">
        <img class="img-responsive" src="${themeResourcePath}/images/stock.png" style="width: 450px;"/>
      </div>
      <div class="col-xs-12 col-md-6 right_download_section">
        <div class="col-xs-12">
          <h1 class="text-white">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam auctor nec lacus ut tempor</h1>
          <div>
            <p class="text-white">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam auctor nec lacus ut tempor.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam auctor nec lacus ut tempor.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam auctor nec lacus ut tempor</p>
          </div>
          <div class="download_app_img">
            <img class="img_lt img-responsive" src="${themeResourcePath}/images/googleplay.png" alt="" />
            <img class="img_rt img-responsive" src="${themeResourcePath}/images/appstore.png" alt="" />
          </div>
        </div>
      </div>
    </div>
    </div> --%>
    <div class="news-letter-section">
    <div class="clearfix icon_section mb-30">
      <h2 class="text-center text-white">SIGNUP FOR NEWS LETTER</h2>
    </div>
   <!--  <form id="search-form_3">
      <input type="text" class="search_3" placeholder="Enter your email address here.."/>
      <input type="submit" class="submit_3" value="Search" />
      </form> -->
      <div class="row">
        <form id="search-form_3">
        <div class="input-field col s12">
          <input id="email" type="email" class="validate">
          <label for="email">Email</label>
      <button class="btn waves-effect waves-light" type="submit" name="action">Submit
  <!--   <i class="material-icons right">send</i> -->
  </button>
        </div>
        </form>
      </div>
    </div>
  </div>


  <!-----------------Download App Ends------------------>


<!-----------------Contact US starts------------------>
  <div class="container">
    <div class="icon_section mb-30 clearfix">
      <h2 class="col-xs-12 text-center">CONTACT US</h2>
      <div class="col-xs-12 contact_form text-center clearfix">
        <form class="col s12">
        <div class="row">
        <!--   <div class="col-xs-12 col-md-6 form-group">
            <input type="input" class="form-field form-control" id="name" placeholder="Name">
          </div>
          <div class="col-xs-12 col-md-6 form-group">
            <input type="email" class="form-field form-control" id="email" aria-describedby="emailHelp" placeholder="Email">
          </div> -->
          <div class="input-field col s6">
          <input id="first_name" type="text" class="validate">
          <label for="first_name">First Name</label>
        </div>
        <div class="input-field col s6">
          <input id="last_name" type="text" class="validate">
          <label for="last_name">Last Name</label>
        </div>
        </div>
         <!--  <div class="clearfix">
          <div class="form-group">

            <textarea class="form-field form-control" rows="5" placeholder="message"></textarea>
          </div>
        </div> -->
        <div class="row clearfix">
	        <div class="input-field col s12">
	          <textarea id="textarea1" class="materialize-textarea"></textarea>
	          <label for="textarea1">Message</label>
	        </div>

  		</div>
        <button type="submit" class="margin-auto btn btn-primary">Submit</button>
        </form>
      </div>
    </div>
  </div>
  <!-----------------Contact US Ends------------------>
</template:page>
