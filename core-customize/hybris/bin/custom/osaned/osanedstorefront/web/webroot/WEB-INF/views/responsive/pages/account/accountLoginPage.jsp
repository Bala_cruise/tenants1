<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>

<template:page pageTitle="${pageTitle}">
<div class="container">

	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 col-lg-offset-3 mb-30 clearfix">
		<div class="login-section">
		<h1 class="text-center">Please Login Here !!!</h1>
			<!-- <div class="headline text-center">Returning Customer</div> -->

			<form id="loginForm"
				action="/yb2bacceleratorstorefront/powertools/en/USD/j_spring_security_check"
				method="post">
				<div class="form-group">
					<label class="control-label " for="j_username"> Email
						Address</label> <input id="j_username" name="j_username"
						class=" form-control form-border-left" type="text" value="">
				</div>
				<div class="form-group">
					<label class="control-label " for="j_password"> Password</label> <input
						id="j_password" name="j_password" class="form-control form-border-left"
						type="password" value="" autocomplete="off">
				</div>
				<div class="forgotten-password mb-30">
					<a href="#"
						data-link="/yb2bacceleratorstorefront/powertools/en/USD/login/pw/request"
						class="js-password-forgotten" data-cbox-title="Reset Password">
						Forgot your password?</a>
				</div>

				<div class="row login-form-action">
					<div class="col-sm-6 col-sm-push-6">
						<button type="submit" class="btn btn-primary btn-block ">
							Log In</button>
					</div>

					<div class="col-sm-6 col-sm-pull-6">
						<a href="/yb2bacceleratorstorefront/powertools/en/USD/register">
							<button type="button" class="btn btn-default btn-block">
								Register</button>
						</a>
					</div>
				</div>
				<div>
					<input type="hidden" name="CSRFToken"
						value="fd17484b-e52a-4f06-8392-f072f64254fb">
				</div>
			</form>
		</div>
	</div><br><br><br><br><br><br><br><br><br><br><br><br>
	</div>
</template:page>