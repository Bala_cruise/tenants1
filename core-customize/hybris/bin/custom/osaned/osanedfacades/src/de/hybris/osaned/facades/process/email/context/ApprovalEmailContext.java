/**
 *
 */
package de.hybris.osaned.facades.process.email.context;

import de.hybris.platform.acceleratorservices.model.cms2.pages.EmailPageModel;
import de.hybris.platform.acceleratorservices.process.email.context.AbstractEmailContext;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.commerceservices.model.process.StoreFrontCustomerProcessModel;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import javax.annotation.Resource;


/**
 * @author balamurugan
 *
 */
public class ApprovalEmailContext extends AbstractEmailContext<StoreFrontCustomerProcessModel>
{
	@Resource
	private Converter<UserModel, CustomerData> customerConverter;
	private CustomerData customerData;

	@Override
	public void init(final StoreFrontCustomerProcessModel storeFrontCustomerProcessModel, final EmailPageModel emailPageModel)
	{
		super.init(storeFrontCustomerProcessModel, emailPageModel);
		customerData = customerConverter.convert(getCustomer(storeFrontCustomerProcessModel));
		final CustomerModel customerModel = getCustomer(storeFrontCustomerProcessModel);
		if (customerModel instanceof B2BCustomerModel)
		{
			final B2BCustomerModel b2bCustomerModel = (B2BCustomerModel) customerModel;
			final String defaultPassword = getConfigurationService().getConfiguration().getString("b2bcustomer.defaultpassword");
			final String loginPageRedirectUrl = getConfigurationService().getConfiguration()
					.getString("b2bcustomer.profile.activate.redirect.url");
			put("defaultPassword", defaultPassword);
			put("loginPageUrl", loginPageRedirectUrl);
			put("customer", customerData);
		}

	}

	@Override
	protected BaseSiteModel getSite(final StoreFrontCustomerProcessModel businessProcessModel)
	{
		return businessProcessModel.getSite();
	}

	@Override
	protected CustomerModel getCustomer(final StoreFrontCustomerProcessModel businessProcessModel)
	{

		return businessProcessModel.getCustomer();
	}

	@Override
	protected LanguageModel getEmailLanguage(final StoreFrontCustomerProcessModel businessProcessModel)
	{

		return businessProcessModel.getLanguage();
	}

	/**
	 * @return the customerConverter
	 */
	public Converter<UserModel, CustomerData> getCustomerConverter()
	{
		return customerConverter;
	}

	/**
	 * @param customerConverter
	 *           the customerConverter to set
	 */
	public void setCustomerConverter(final Converter<UserModel, CustomerData> customerConverter)
	{
		this.customerConverter = customerConverter;
	}

}
