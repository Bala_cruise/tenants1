/**
 *
 */
package de.hybris.osaned.facades.populators;

import de.hybris.osaned.core.model.FinanceCustomerMasterDataModel;
import de.hybris.platform.commercefacades.user.data.CustomerFinanceMasterSetupData;
import de.hybris.platform.commercefacades.user.data.FinanceCustomerInfoData;
import de.hybris.platform.commercefacades.user.data.FinanceItemInfoData;
import de.hybris.platform.commercefacades.user.data.FinanceSupplierInfoData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;




/**
 * @author balamurugan
 *
 */
public class CustomerFinanceMasterSetupDataPopulator
		implements Populator<FinanceCustomerMasterDataModel, CustomerFinanceMasterSetupData>
{

	@Override
	public void populate(final FinanceCustomerMasterDataModel source, final CustomerFinanceMasterSetupData target)
			throws ConversionException
	{

		final FinanceCustomerInfoData financeCustomerInfoData = new FinanceCustomerInfoData();
		financeCustomerInfoData.setBpCode(source.getCustomerBpCode());
		financeCustomerInfoData.setName(source.getCustomerName());
		financeCustomerInfoData.setAddress(source.getCustomerAddress());
		financeCustomerInfoData.setVatNumber(source.getCustomerVatNumber());
		financeCustomerInfoData.setFinancialGroup(source.getCustomerFinancialGroup());
		financeCustomerInfoData.setCurrency(source.getCustomerCurrency());

		final FinanceSupplierInfoData financeSupplierInfoData = new FinanceSupplierInfoData();
		financeSupplierInfoData.setBpCode(source.getSupplierBpCode());
		financeSupplierInfoData.setName(source.getSupplierName());
		financeSupplierInfoData.setAddress(source.getSupplierAddress());
		financeSupplierInfoData.setVatNumber(source.getSupplierVatNumber());
		financeSupplierInfoData.setFinancialGroup(source.getSupplierFinancialGroup());
		financeSupplierInfoData.setCurrency(source.getSupplierCurrency());

		final FinanceItemInfoData financeItemInfoData = new FinanceItemInfoData();
		financeItemInfoData.setItemCode(source.getItemCode());
		financeItemInfoData.setItemGroup(source.getItemGroup());
		financeItemInfoData.setItemType(source.getType());
		financeItemInfoData.setUnitSet(source.getUnitSet());
		financeItemInfoData.setInventoryUnit(source.getInventoryUnit());
		financeItemInfoData.setDescription(source.getDescription());
		financeItemInfoData.setCurrency(source.getCurrency());

		target.setCustomer(financeCustomerInfoData);
		target.setSupplier(financeSupplierInfoData);
		target.setItem(financeItemInfoData);
	}

}
