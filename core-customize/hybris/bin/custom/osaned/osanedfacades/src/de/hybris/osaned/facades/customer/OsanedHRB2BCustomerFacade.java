/**
 *
 */
package de.hybris.osaned.facades.customer;

import de.hybris.platform.commercefacades.user.data.B2BCustomerData;
import de.hybris.platform.commercefacades.user.data.EmployeeMasterSetupData;
import de.hybris.platform.commerceservices.customer.DuplicateUidException;


/**
 * @author Bala Murugan
 *
 */
public interface OsanedHRB2BCustomerFacade
{
	/**
	 * @param b2bCustomerData
	 * @throws DuplicateUidException
	 */
	public void registerHRCustomer(B2BCustomerData b2bCustomerData) throws DuplicateUidException;


	/**
	 * @param employeeMasterSetupData
	 * @param userId
	 */
	public void sendEmployeeMasterDataSetupToCPI(EmployeeMasterSetupData employeeMasterSetupData, String userId);
	/**
	 * @param emailId
	 */
	public void retriggerEmailtoEmployee(String emailId);
}
