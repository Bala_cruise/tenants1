/**
 *
 */
package de.hybris.osaned.facades.populators;

import static org.junit.Assert.assertNotNull;

import de.hybris.osaned.core.enums.B2BCustomerRoleEnum;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.services.B2BCustomerService;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.commercefacades.user.data.CustomerRoleListData;
import de.hybris.platform.commercefacades.user.data.SubEmployeeInfoData;
import de.hybris.platform.commercefacades.user.data.SubEmployeeInfoDataList;
import de.hybris.platform.commercefacades.user.data.SuperAdminEmailIdData;
import de.hybris.platform.commercefacades.user.data.SuperAdminInfoListData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.enumeration.EnumerationService;
import de.hybris.platform.europe1.enums.UserPriceGroup;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.util.CollectionUtils;


/**
 * @author Bala Murugan
 *
 */
public class OsanedCustomerPopulator implements Populator<UserModel, CustomerData>
{
	@Resource
	private EnumerationService enumerationService;
	@Resource
	private B2BCustomerService b2bCustomerService;

	@Override
	public void populate(final UserModel source, final CustomerData target) throws ConversionException
	{
		assertNotNull("UserModel is null", source);
		if (source instanceof B2BCustomerModel)
		{
			final B2BCustomerModel b2bCustomerModel = (B2BCustomerModel) source;
			target.setEmail(b2bCustomerModel.getEmail());
			if (StringUtils.isNotEmpty(b2bCustomerModel.getContact()))
			{
				target.setContact(b2bCustomerModel.getContact());
			}
			if (StringUtils.isNotEmpty(b2bCustomerModel.getCompany()))
			{

				target.setCompany(b2bCustomerModel.getCompany());
			}
			if (StringUtils.isNotEmpty(b2bCustomerModel.getSizeRange()))
			{
				target.setSizeRangeName(b2bCustomerModel.getSizeRange());
				final UserPriceGroup userPriceGroup = enumerationService.getEnumerationValue(UserPriceGroup.class,
						b2bCustomerModel.getSizeRange());
				target.setSizeRange(enumerationService.getEnumerationName(userPriceGroup));
			}
			if (StringUtils.isNotEmpty(b2bCustomerModel.getCommercialRegistration()))
			{

				target.setCommercialRegistration(b2bCustomerModel.getCommercialRegistration());
			}
			if (StringUtils.isNotEmpty(b2bCustomerModel.getEmployeeId()))
			{
				target.setEmployeeId(b2bCustomerModel.getEmployeeId());
			}
			if (StringUtils.isNotEmpty(b2bCustomerModel.getIqamaId()))
			{
				target.setIqamaId(b2bCustomerModel.getIqamaId());
			}
			target.setCustomerRole(manipulateCustomerRoles(b2bCustomerModel));
			populateSubEmployeesInfo(b2bCustomerModel, target);
			populateSuperEmployeeInfo(b2bCustomerModel, target);
			target.setOrderApprovalStatus(b2bCustomerModel.isOrderApprovalStatus());
			target.setFirstApprover(b2bCustomerModel.getApproverLevel1());
			target.setSecondApprover(b2bCustomerModel.getApproverLevel2());
			target.setThirdApprover(b2bCustomerModel.getApproverLevel3());
			target.setActive(!b2bCustomerModel.isLoginDisabled());
		}

	}

	/**
	 * @param b2bCustomerModel
	 */
	private CustomerRoleListData manipulateCustomerRoles(final B2BCustomerModel b2bCustomerModel)
	{
		if (!CollectionUtils.isEmpty(b2bCustomerModel.getCustomerRole()))
		{
			final CustomerRoleListData CustomerRoleListData = new CustomerRoleListData();
			final Set<String> customeRoleList = new HashSet<>();
			b2bCustomerModel.getCustomerRole().forEach(customerRole -> {
				customeRoleList.add(getCustomerRole(customerRole));
			});
			CustomerRoleListData.setCustomerRoleList(customeRoleList);
			return CustomerRoleListData;
		}
		return null;

	}

	/**
	 * @param customerRole
	 */
	private String getCustomerRole(final B2BCustomerRoleEnum customerRole)
	{
		if (customerRole.equals(B2BCustomerRoleEnum.SUPERADMIN))
		{
			return "SuperAdmin";
		}
		else if (customerRole.equals(B2BCustomerRoleEnum.HRADMIN) || customerRole.equals(B2BCustomerRoleEnum.ADMIN))
		{
			return "HR";
		}
		else if (customerRole.equals(B2BCustomerRoleEnum.CEO) || customerRole.equals(B2BCustomerRoleEnum.CFO)
				|| customerRole.equals(B2BCustomerRoleEnum.FINANCEANALYST)
				|| customerRole.equals(B2BCustomerRoleEnum.FINANCECONTROLLER) || customerRole.equals(B2BCustomerRoleEnum.FINANCEHEAD)
				|| customerRole.equals(B2BCustomerRoleEnum.FINANCEMANAGEREMP))
		{
			return "Finance";
		}
		else
		{
			return customerRole.getCode();
		}

	}

	/**
	 * @param b2bCustomerModel
	 * @param target
	 */
	private void populateSuperEmployeeInfo(final B2BCustomerModel b2bCustomerModel, final CustomerData target)
	{
		if (!CollectionUtils.isEmpty(b2bCustomerModel.getSuperAdminEmployeeIds()))
		{
			final SuperAdminInfoListData superAdminInfoListData = new SuperAdminInfoListData();
			final List<SuperAdminEmailIdData> superAdminEmailIdDataList = new ArrayList<>();
			b2bCustomerModel.getSuperAdminEmployeeIds().forEach(emailId -> {
				final SuperAdminEmailIdData superAdminEmailIdData = new SuperAdminEmailIdData();
				superAdminEmailIdData.setEmailId(emailId);
				superAdminEmailIdDataList.add(superAdminEmailIdData);
			});
			superAdminInfoListData.setSuperAdminInfoList(superAdminEmailIdDataList);
			target.setSuperAdminInfo(superAdminInfoListData);
		}
	}

	/**
	 * @param b2bCustomerModel
	 * @param target
	 */
	private void populateSubEmployeesInfo(final B2BCustomerModel b2bCustomerModel, final CustomerData target)
	{

		if (!CollectionUtils.isEmpty(b2bCustomerModel.getSubEmployeeIds()))
		{
			final SubEmployeeInfoDataList subEmployeeInfoDataList = new SubEmployeeInfoDataList();
			final List<SubEmployeeInfoData> subEmployeeDataList = new ArrayList<SubEmployeeInfoData>();
			b2bCustomerModel.getSubEmployeeIds().forEach(emailId -> {
				final SubEmployeeInfoData subEmployeeInfoData = new SubEmployeeInfoData();
				final Object obj = b2bCustomerService.getUserForUID(emailId);
				if (obj instanceof B2BCustomerModel)
				{
					final B2BCustomerModel subEmployee = (B2BCustomerModel) obj;
					subEmployeeInfoData.setEmailId(emailId);
					subEmployeeInfoData.setActive(!subEmployee.isLoginDisabled());
					subEmployeeInfoData.setCustomerRole(manipulateCustomerRoles(subEmployee));
				}
				subEmployeeDataList.add(subEmployeeInfoData);
			});
			subEmployeeInfoDataList.setSubEmployeesInfoList(subEmployeeDataList);
			target.setSubEmployeesInfo(subEmployeeInfoDataList);
		}

	}

}
