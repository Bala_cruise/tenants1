/**
 *
 */
package de.hybris.osaned.facades.customer;

import de.hybris.osaned.core.exception.CPIConnectionFailureException;
import de.hybris.platform.commercefacades.user.data.B2BCustomerData;
import de.hybris.platform.commerceservices.customer.DuplicateUidException;


/**
 * @author bala.v
 *
 */
public interface OsanedB2BCustomerFacade
{
	/**
	 * @param b2bCustomerData
	 * @throws DuplicateUidException
	 * @throws CPIConnectionFailureException
	 */
	public void registerB2BCustomer(B2BCustomerData b2bCustomerData) throws DuplicateUidException, CPIConnectionFailureException;

	/**
	 * @param userId
	 * @param priceRange
	 */
	public void setPriceRangeInB2BUnit(String userId, String priceRange);

	/**
	 * @param userId
	 * @param otp
	 */
	public void activateCustomerProfile(String userId, String otp);

	/**
	 * @param userId
	 */
	public void acceptTermsAndCondition(String userId);

	/**
	 * @param b2bCustomerData
	 */
	public void updateB2BCustomer(B2BCustomerData b2bCustomerData);
}
