/**
 *
 */
package de.hybris.osaned.facades.customer.impl;

import de.hybris.osaned.core.customer.OsanedHRB2BCustomerService;
import de.hybris.osaned.facades.customer.OsanedHRB2BCustomerFacade;
import de.hybris.platform.b2bacceleratorfacades.customer.impl.DefaultB2BCustomerFacade;
import de.hybris.platform.commercefacades.user.data.B2BCustomerData;
import de.hybris.platform.commercefacades.user.data.EmployeeMasterSetupData;
import de.hybris.platform.commerceservices.customer.DuplicateUidException;


/**
 * @author Bala Murugan
 *
 */
public class DefaultOsanedHRB2BCustomerFacadeImpl extends DefaultB2BCustomerFacade implements OsanedHRB2BCustomerFacade
{
	private OsanedHRB2BCustomerService osanedHRB2BCustomerService;

	@Override
	public void registerHRCustomer(final B2BCustomerData b2bCustomerData) throws DuplicateUidException
	{
		if (!getUserFacade().isUserExisting(b2bCustomerData.getEmail()))
		{
			getOsanedHRB2BCustomerService().registerHRCustomer(b2bCustomerData);
		}
		else
		{
			getOsanedHRB2BCustomerService().updateHRCustomer(b2bCustomerData);
		}

	}

	/**
	 * @return the osanedHRB2BCustomerService
	 */
	public OsanedHRB2BCustomerService getOsanedHRB2BCustomerService()
	{
		return osanedHRB2BCustomerService;
	}

	/**
	 * @param osanedHRB2BCustomerService
	 *           the osanedHRB2BCustomerService to set
	 */
	public void setOsanedHRB2BCustomerService(final OsanedHRB2BCustomerService osanedHRB2BCustomerService)
	{
		this.osanedHRB2BCustomerService = osanedHRB2BCustomerService;
	}

	@Override
	public void sendEmployeeMasterDataSetupToCPI(final EmployeeMasterSetupData employeeMasterSetupData, final String userId)
	{
		osanedHRB2BCustomerService.sendEmployeeMasterDataSetupToCPI(employeeMasterSetupData, userId);
	}

	@Override
	public void retriggerEmailtoEmployee(final String emailId)
	{
		getOsanedHRB2BCustomerService().retriggerEmailtoEmployee(emailId);
	}

}
