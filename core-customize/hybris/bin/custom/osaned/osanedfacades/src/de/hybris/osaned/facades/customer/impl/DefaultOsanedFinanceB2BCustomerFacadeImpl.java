/**
 *
 */
package de.hybris.osaned.facades.customer.impl;

import de.hybris.osaned.core.customer.OsanedFinanceB2BCustomerService;
import de.hybris.osaned.core.model.FinanceCustomerMasterDataModel;
import de.hybris.osaned.facades.customer.OsanedFinanceB2BCustomerFacade;
import de.hybris.platform.commercefacades.user.UserFacade;
import de.hybris.platform.commercefacades.user.data.B2BCustomerData;
import de.hybris.platform.commercefacades.user.data.CustomerFinanceMasterSetupData;
import de.hybris.platform.commercefacades.user.data.FinanceManagerOpenBalanceData;
import de.hybris.platform.commerceservices.customer.DuplicateUidException;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import javax.annotation.Resource;


/**
 * @author balamurugan
 *
 */
public class DefaultOsanedFinanceB2BCustomerFacadeImpl implements OsanedFinanceB2BCustomerFacade
{
	@Resource
	private UserFacade userFacade;
	@Resource
	private OsanedFinanceB2BCustomerService osanedFinanceB2BCustomerService;
	@Resource
	private Converter<FinanceCustomerMasterDataModel, CustomerFinanceMasterSetupData> customerFinanceMasterSetupDataConverter;

	@Override
	public void createFinanceCustomer(final B2BCustomerData b2bCustomerData) throws DuplicateUidException
	{
		if (!userFacade.isUserExisting(b2bCustomerData.getEmail()))
		{
			osanedFinanceB2BCustomerService.createFinanceCustomer(b2bCustomerData);
		}
		else
		{
			osanedFinanceB2BCustomerService.updateFinanceCustomer(b2bCustomerData);
		}
	}

	@Override
	public void setAuthMatrixForUser(final String userId, final String approverLevel1, final String approverLevel2,
			final String approverLevel3)
	{

		osanedFinanceB2BCustomerService.setAuthMatrixForUser(userId, approverLevel1, approverLevel2, approverLevel3);
	}

	@Override
	public void setCustomerMasterData(final CustomerFinanceMasterSetupData customerFinanceMasterSetupData, final String userId)
	{
		osanedFinanceB2BCustomerService.setCustomerMasterData(customerFinanceMasterSetupData, userId);

	}

	@Override
	public CustomerFinanceMasterSetupData getCustomerMasterData(final String userId)
	{
		final FinanceCustomerMasterDataModel financeCustomerMasterDataModel = osanedFinanceB2BCustomerService
				.getCustomerMasterData(userId);
		return customerFinanceMasterSetupDataConverter.convert(financeCustomerMasterDataModel);
	}

	@Override
	public void updateCustomerMasterData(final CustomerFinanceMasterSetupData customerFinanceMasterSetupData, final String userId)
	{
		osanedFinanceB2BCustomerService.updateCustomerMasterData(customerFinanceMasterSetupData, userId);

	}

	@Override
	public void setFinanceOpenBalance(final FinanceManagerOpenBalanceData financeManagerOpenBalanceData, final String userId)
	{
		osanedFinanceB2BCustomerService.setFinanceOpenBalance(financeManagerOpenBalanceData, userId);
	}



}
