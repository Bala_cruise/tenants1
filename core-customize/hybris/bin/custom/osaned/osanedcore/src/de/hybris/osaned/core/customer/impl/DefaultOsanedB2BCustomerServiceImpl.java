/**
 *
 */
package de.hybris.osaned.core.customer.impl;

import de.hybris.osaned.core.customer.OsanedB2BCustomerService;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.b2b.services.impl.DefaultB2BCustomerService;
import de.hybris.platform.commercefacades.user.data.B2BCustomerData;
import de.hybris.platform.commerceservices.security.SecureToken;
import de.hybris.platform.commerceservices.security.SecureTokenService;
import de.hybris.platform.enumeration.EnumerationService;
import de.hybris.platform.europe1.enums.UserPriceGroup;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;


/**
 * @author Bala Murugan
 *
 */
public class DefaultOsanedB2BCustomerServiceImpl extends DefaultB2BCustomerService implements OsanedB2BCustomerService
{
	private ConfigurationService configurationService;
	private SecureTokenService secureTokenService;
	private ModelService modelService;
	@Resource
	private EnumerationService enumerationService;

	@Override
	public void activateCustomerProfile(final String userId, final String otp)
	{
		final int tokenValidity = getConfigurationService().getConfiguration()
				.getInt("b2bcustomer.account.activation.token.validity.seconds");
		final B2BCustomerModel b2bCustomerModel = getUserForUID(userId);
		if (StringUtils.isNotEmpty(b2bCustomerModel.getEmailVerificationToken()))
		{
			final String token = b2bCustomerModel.getEmailVerificationToken();
			final SecureToken data = getSecureTokenService().decryptData(token);
			if (tokenValidity > 0L)
			{
				final long delta = new Date().getTime() - data.getTimeStamp();
				if (delta / 1000 > tokenValidity)
				{
					throw new IllegalArgumentException("Email link expired");
				}
			}
		}
		if (StringUtils.isNotEmpty(b2bCustomerModel.getOtp()) && StringUtils.equals(otp, b2bCustomerModel.getOtp()))
		{
			b2bCustomerModel.setLoginDisabled(Boolean.FALSE);
			b2bCustomerModel.setEmailVerificationToken(StringUtils.EMPTY);
			b2bCustomerModel.setOtp(StringUtils.EMPTY);
			getModelService().save(b2bCustomerModel);
		}
		else
		{
			throw new IllegalArgumentException("Invalid OTP");
		}
	}

	/**
	 * @return the configurationService
	 */
	public ConfigurationService getConfigurationService()
	{
		return configurationService;
	}

	/**
	 * @param configurationService
	 *           the configurationService to set
	 */
	public void setConfigurationService(final ConfigurationService configurationService)
	{
		this.configurationService = configurationService;
	}

	/**
	 * @return the secureTokenService
	 */
	public SecureTokenService getSecureTokenService()
	{
		return secureTokenService;
	}

	/**
	 * @param secureTokenService
	 *           the secureTokenService to set
	 */
	public void setSecureTokenService(final SecureTokenService secureTokenService)
	{
		this.secureTokenService = secureTokenService;
	}

	/**
	 * @return the modelService
	 */
	public ModelService getModelService()
	{
		return modelService;
	}

	/**
	 * @param modelService
	 *           the modelService to set
	 */
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	@Override
	public void acceptTermsAndCondition(final String userId)
	{
		final B2BCustomerModel b2bCustomerModel = getUserForUID(userId);
		b2bCustomerModel.setTermsAndCondition(Boolean.TRUE);
		getModelService().save(b2bCustomerModel);

	}

	@Override
	public void updateB2BCustomer(final B2BCustomerData b2bCustomerData)
	{
		final B2BCustomerModel b2bCustomerModel = getUserForUID(b2bCustomerData.getCustomerId());
		b2bCustomerModel.setEmail(b2bCustomerData.getEmail());
		b2bCustomerModel.setName(b2bCustomerData.getName());
		b2bCustomerModel.setContact(b2bCustomerData.getContact());
		b2bCustomerModel.setCompany(b2bCustomerData.getCompany());
		b2bCustomerModel.setSizeRange(b2bCustomerData.getSizeRange());
		b2bCustomerModel.setCommercialRegistration(b2bCustomerData.getCommercialRegistration());
		b2bCustomerModel.setOrderApprovalStatus(Boolean.TRUE);
		b2bCustomerModel.getGroups().forEach(grp -> {
			if (grp instanceof B2BUnitModel)
			{
				final B2BUnitModel b2bUnitModel = (B2BUnitModel) grp;
				b2bUnitModel.setUserPriceGroup(
						enumerationService.getEnumerationValue(UserPriceGroup.class, b2bCustomerData.getSizeRange()));
				getModelService().save(b2bUnitModel);
			}
		});
		getModelService().save(b2bCustomerModel);
		updateApproverData(b2bCustomerData);
	}

	/**
	 * @param b2bCustomerData
	 */
	private void updateApproverData(final B2BCustomerData b2bCustomerData)
	{
		final B2BCustomerModel approverCustomer = getUserForUID(b2bCustomerData.getUid());
		final Set<String> employeeIds = new HashSet<>(approverCustomer.getSuperAdminEmployeeIds());
		employeeIds.remove(b2bCustomerData.getCustomerId());
		approverCustomer.setSuperAdminEmployeeIds(employeeIds);
		getModelService().save(approverCustomer);

	}

}
