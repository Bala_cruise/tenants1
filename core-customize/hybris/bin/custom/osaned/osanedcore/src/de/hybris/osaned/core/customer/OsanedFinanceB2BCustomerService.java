/**
 *
 */
package de.hybris.osaned.core.customer;

import de.hybris.osaned.core.model.FinanceCustomerMasterDataModel;
import de.hybris.platform.commercefacades.user.data.B2BCustomerData;
import de.hybris.platform.commercefacades.user.data.CustomerFinanceMasterSetupData;
import de.hybris.platform.commercefacades.user.data.FinanceManagerOpenBalanceData;


/**
 * @author balamurugan
 *
 */
public interface OsanedFinanceB2BCustomerService
{
	/**
	 * @param b2bCustomerData
	 */
	public void createFinanceCustomer(B2BCustomerData b2bCustomerData);

	/**
	 * @param b2bCustomerData
	 */
	public void updateFinanceCustomer(B2BCustomerData b2bCustomerData);


	/**
	 * @param userId
	 * @param approverLevel1
	 * @param approverLevel2
	 * @param approverLevel3
	 */
	public void setAuthMatrixForUser(String userId, String approverLevel1, String approverLevel2, String approverLevel3);

	/**
	 * @param customerFinanceMasterSetupData
	 * @param userId
	 */
	public void setCustomerMasterData(CustomerFinanceMasterSetupData customerFinanceMasterSetupData, String userId);

	/**
	 * @param userId
	 * @return FinanceCustomerMasterDataModel
	 */
	public FinanceCustomerMasterDataModel getCustomerMasterData(String userId);

	/**
	 * @param customerFinanceMasterSetupData
	 * @param userId
	 */
	public void updateCustomerMasterData(CustomerFinanceMasterSetupData customerFinanceMasterSetupData, String userId);

	/**
	 * @param FinanceManagerOpenBalanceData
	 * @param userId
	 */
	public void setFinanceOpenBalance(FinanceManagerOpenBalanceData financeManagerOpenBalanceData, String userId);

}
