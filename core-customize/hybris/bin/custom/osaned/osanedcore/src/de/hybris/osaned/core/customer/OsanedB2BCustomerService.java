/**
 *
 */
package de.hybris.osaned.core.customer;

import de.hybris.platform.commercefacades.user.data.B2BCustomerData;


/**
 * @author Bala Murugan
 *
 */
public interface OsanedB2BCustomerService
{
	/**
	 * @param userId
	 * @param otp
	 */
	public void activateCustomerProfile(String userId, String otp);

	/**
	 * @param userId
	 */
	public void acceptTermsAndCondition(final String userId);

	/**
	 * @param b2bCustomerData
	 */
	public void updateB2BCustomer(B2BCustomerData b2bCustomerData);
}
