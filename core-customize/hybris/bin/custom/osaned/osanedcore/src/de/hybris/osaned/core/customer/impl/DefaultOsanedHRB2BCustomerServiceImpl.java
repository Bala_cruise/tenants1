/**
 *
 */
package de.hybris.osaned.core.customer.impl;

import de.hybris.osaned.core.customer.OsanedHRB2BCustomerService;
import de.hybris.osaned.core.enums.B2BCustomerRoleEnum;
import de.hybris.platform.b2b.constants.B2BConstants;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.commercefacades.user.data.B2BCustomerData;
import de.hybris.platform.commercefacades.user.data.EmployeeMasterSetupData;
import de.hybris.platform.commerceservices.event.RegisterEvent;
import de.hybris.platform.commerceservices.security.SecureToken;
import de.hybris.platform.core.model.user.UserGroupModel;

import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.apache.log4j.Logger;
import org.springframework.util.CollectionUtils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import net.minidev.json.JSONObject;


/**
 * @author Bala Murugan
 *
 */
public class DefaultOsanedHRB2BCustomerServiceImpl extends OsanedDefaultB2BCustomerAccountService
		implements OsanedHRB2BCustomerService
{
	private static final Logger LOG = Logger.getLogger(DefaultOsanedHRB2BCustomerServiceImpl.class);

	@Override
	public void registerHRCustomer(final B2BCustomerData b2bCustomerData)
	{
		final B2BCustomerModel b2bCustomerModel = getModelService().create(B2BCustomerModel.class);
		b2bCustomerModel.setUid(b2bCustomerData.getEmail().toLowerCase());
		b2bCustomerModel.setEmail(b2bCustomerData.getEmail().toLowerCase());
		b2bCustomerModel.setName(b2bCustomerData.getName());
		b2bCustomerModel.setEmployeeId(b2bCustomerData.getEmployeeId());
		b2bCustomerModel.setIqamaId(b2bCustomerData.getIqamaId());
		b2bCustomerModel.setPassword(getConfigurationService().getConfiguration().getString("b2bcustomer.defaultpassword"));
		b2bCustomerModel.setLoginDisabled(Boolean.TRUE);
		b2bCustomerModel.setEmailVerificationToken(generateSecureToken(b2bCustomerData));
		b2bCustomerModel.setOtp(generateOtpNumber());
		b2bCustomerModel.setSentToCPI(Boolean.FALSE);
		final UserGroupModel b2bCustomerGroup = getUserService().getUserGroupForUID(B2BConstants.B2BCUSTOMERGROUP);
		b2bCustomerModel.setDefaultB2BUnit(getHrB2BUnit(b2bCustomerData));
		b2bCustomerModel.setGroups(Collections.singleton(b2bCustomerGroup));
		final Set<B2BCustomerRoleEnum> B2BCustomerRoleEnumList = new HashSet<>();
		B2BCustomerRoleEnumList
				.add(getEnumerationService().getEnumerationValue(B2BCustomerRoleEnum.class, b2bCustomerData.getRole()));
		b2bCustomerModel.setCustomerRole(B2BCustomerRoleEnumList);
		getModelService().save(b2bCustomerModel);
		setEmployeeInParentEmployee(b2bCustomerData, b2bCustomerModel);
		setParentEmloyeeInSubEmployee(b2bCustomerData, b2bCustomerModel);
		if (getCpiConnectionUtil().sendDataToCpi(getConvertedJsonCustomerData(b2bCustomerModel)))
		{
			b2bCustomerModel.setSentToCPI(Boolean.TRUE);
			getModelService().save(b2bCustomerModel);
		}
		getEventService().publishEvent(initializeEvent(new RegisterEvent(), b2bCustomerModel));
	}

	/**
	 * @param b2bCustomerData
	 * @param b2bCustomerModel
	 */
	private void setParentEmloyeeInSubEmployee(final B2BCustomerData b2bCustomerData, final B2BCustomerModel b2bCustomerModel)
	{
		if (CollectionUtils.isEmpty(b2bCustomerModel.getSuperAdminEmployeeIds()))
		{
			b2bCustomerModel.setSuperAdminEmployeeIds(Collections.singleton(b2bCustomerData.getHrCustomerId()));
		}
		else
		{
			final Set<String> superEmployeeIdList = new HashSet<>(b2bCustomerModel.getSuperAdminEmployeeIds());
			superEmployeeIdList.add(b2bCustomerData.getHrCustomerId());
			b2bCustomerModel.setSuperAdminEmployeeIds(superEmployeeIdList);
		}
		getModelService().save(b2bCustomerModel);
	}

	/**
	 * @param b2bCustomerData
	 * @param b2bCustomerModel
	 */
	private void setEmployeeInParentEmployee(final B2BCustomerData b2bCustomerData, final B2BCustomerModel b2bCustomerModel)
	{

		final B2BCustomerModel parentEmployee = getUserService().getUserForUID(b2bCustomerData.getHrCustomerId(),
				B2BCustomerModel.class);
		if (CollectionUtils.isEmpty(parentEmployee.getSubEmployeeIds()))
		{
			parentEmployee.setSubEmployeeIds(Collections.singleton(b2bCustomerModel.getEmail()));
		}
		else
		{
			final Set<String> subEmployeeIdList = new HashSet<>(parentEmployee.getSubEmployeeIds());
			subEmployeeIdList.add(b2bCustomerModel.getEmail());
			parentEmployee.setSubEmployeeIds(subEmployeeIdList);
		}
		getModelService().save(parentEmployee);

	}

	/**
	 * @param b2bCustomerModel
	 * @return
	 */
	private String getConvertedJsonCustomerData(final B2BCustomerModel b2bCustomerModel)
	{
		final JSONObject json = new JSONObject();
		json.put("emailId", b2bCustomerModel.getEmail());
		json.put("name", b2bCustomerModel.getName());
		json.put("employeeId", b2bCustomerModel.getEmployeeId());
		json.put("iqamaId", b2bCustomerModel.getIqamaId());
		return json.toString();
	}

	/**
	 * @param b2bCustomerData
	 * @return
	 */
	private B2BUnitModel getHrB2BUnit(final B2BCustomerData b2bCustomerData)
	{
		final B2BCustomerModel parentEmployee = getUserService().getUserForUID(b2bCustomerData.getHrCustomerId(),
				B2BCustomerModel.class);
		return parentEmployee.getDefaultB2BUnit();

	}

	@Override
	public void sendEmployeeMasterDataSetupToCPI(final EmployeeMasterSetupData employeeMasterSetupData, final String UserId)
	{
		final B2BCustomerModel b2bCustomerModel = getUserService().getUserForUID(UserId, B2BCustomerModel.class);
		if (null != b2bCustomerModel.getCustomerRole())
		{
			final ObjectMapper jsonMapper = new ObjectMapper();
			try
			{
				final String jsonData = jsonMapper.writeValueAsString(employeeMasterSetupData);

				if (getCpiConnectionUtil().sendDataToCpi(jsonData))
				{
					setEmployeeInHrSubEmployee(b2bCustomerModel, employeeMasterSetupData);
				}

			}
			catch (final JsonProcessingException e)
			{
				LOG.debug(e.getMessage());
				getModelService().save(b2bCustomerModel);
			}
		}
		else
		{
			throw new IllegalArgumentException("Customer should belong to [HR Admin] or [Admin] Role");
		}
	}

	/**
	 * @param b2bCustomerModel
	 * @param employeeMasterSetupData
	 */
	private void setEmployeeInHrSubEmployee(final B2BCustomerModel b2bCustomerModel,
			final EmployeeMasterSetupData employeeMasterSetupData)
	{

		if (CollectionUtils.isEmpty(b2bCustomerModel.getSubEmployeeIds()))
		{
			b2bCustomerModel.setSubEmployeeIds(Collections.singleton(employeeMasterSetupData.getPersonalInfo().getIqamaNo()));
		}
		else
		{
			b2bCustomerModel.getSubEmployeeIds().forEach(subEmployee -> {
				if (subEmployee.equals(employeeMasterSetupData.getPersonalInfo().getIqamaNo()))
				{
					throw new IllegalArgumentException("Duplicate IqamaID");
				}
			});
			final Set<String> subEmployeeIdList = new HashSet<>(b2bCustomerModel.getSubEmployeeIds());
			subEmployeeIdList.add(employeeMasterSetupData.getPersonalInfo().getIqamaNo());
			b2bCustomerModel.setSubEmployeeIds(subEmployeeIdList);
		}
		getModelService().save(b2bCustomerModel);

	}


	@Override
	public void retriggerEmailtoEmployee(final String emailId)
	{
		final B2BCustomerModel b2bCustomerModel = getUserService().getUserForUID(emailId, B2BCustomerModel.class);
		final SecureToken secureToken = new SecureToken(emailId.toLowerCase(), new Date().getTime());
		b2bCustomerModel.setEmailVerificationToken(getSecureTokenService().encryptData(secureToken));
		b2bCustomerModel.setOtp(generateOtpNumber());
		getModelService().save(b2bCustomerModel);
		getEventService().publishEvent(initializeEvent(new RegisterEvent(), b2bCustomerModel));

	}

	@Override
	public void updateHRCustomer(final B2BCustomerData b2bCustomerData)
	{
		final B2BCustomerModel b2bCustomerModel = getUserService().getUserForUID(b2bCustomerData.getEmail().toLowerCase(),
				B2BCustomerModel.class);
		final Set<B2BCustomerRoleEnum> customerRoles = new HashSet<>(b2bCustomerModel.getCustomerRole());
		customerRoles.add(getEnumerationService().getEnumerationValue(B2BCustomerRoleEnum.class, b2bCustomerData.getRole()));
		b2bCustomerModel.setCustomerRole(customerRoles);
		getModelService().save(b2bCustomerModel);
	}

}
