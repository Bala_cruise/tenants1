/**
 *
 */
package de.hybris.osaned.core.cpi;

import de.hybris.platform.servicelayer.config.ConfigurationService;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Base64;

import javax.net.ssl.HttpsURLConnection;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;


/**
 * @author Bala Murugan
 *
 */
public class CPIConnectionUtil
{
	private static final Logger LOG = Logger.getLogger(CPIConnectionUtil.class);
	private static String USERNAME = StringUtils.EMPTY;
	private static String PASSWORD = StringUtils.EMPTY;
	private static String URL = StringUtils.EMPTY;
	private ConfigurationService configurationService;

	public Boolean sendDataToCpi(final String jsonData)
	{
		LOG.debug("Started to send the Data to CPI : " + jsonData);
		final Boolean success = Boolean.TRUE;
		try
		{
			USERNAME = getConfigurationService().getConfiguration().getString("cpi.connection.username");
			PASSWORD = getConfigurationService().getConfiguration().getString("cpi.connection.password");
			URL = getConfigurationService().getConfiguration().getString("cpi.connection.url");
			final String encoded = Base64.getEncoder().encodeToString((USERNAME + ":" + PASSWORD).getBytes(StandardCharsets.UTF_8));
			final URL url = new URL(URL);
			final HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
			httpURLConnection.setRequestMethod("POST");
			httpURLConnection.setRequestProperty("Authorization", "Basic " + encoded);
			httpURLConnection.setRequestProperty("Content-Type", "application/json");
			httpURLConnection.setRequestProperty("Accept", "application/json");
			httpURLConnection.setConnectTimeout(10000);
			httpURLConnection.setDoOutput(true);
			httpURLConnection.connect();
			//Write
			final OutputStream outputStream = httpURLConnection.getOutputStream();
			final BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));
			writer.write(jsonData);
			writer.close();
			outputStream.close();
			final int responseCode = httpURLConnection.getResponseCode();
			if (responseCode == HttpsURLConnection.HTTP_OK)
			{
				//Read
				final BufferedReader bufferedReader = new BufferedReader(
						new InputStreamReader(httpURLConnection.getInputStream(), "UTF-8"));

				String line = null;
				final StringBuilder sb = new StringBuilder();

				while ((line = bufferedReader.readLine()) != null)
				{
					sb.append(line);
				}

				bufferedReader.close();
				LOG.debug("Data has been successfully sent");
				LOG.debug(sb.toString());
				return success;
			}
			else
			{
				LOG.debug(new String("Data not sent successfully , Responsecode : " + responseCode));
				return Boolean.FALSE;
			}
		}
		catch (

		final MalformedURLException e)
		{
			LOG.debug(e.getMessage());
			return Boolean.FALSE;
		}
		catch (final IOException e)
		{
			LOG.debug(e.getMessage());
			return Boolean.FALSE;
		}

	}

	/**
	 * @return the configurationService
	 */
	public ConfigurationService getConfigurationService()
	{
		return configurationService;
	}

	/**
	 * @param configurationService
	 *           the configurationService to set
	 */
	public void setConfigurationService(final ConfigurationService configurationService)
	{
		this.configurationService = configurationService;
	}

}
