/**
 *
 */
package de.hybris.osaned.core.customer;

import de.hybris.osaned.core.exception.CPIConnectionFailureException;
import de.hybris.platform.commercefacades.user.data.B2BCustomerData;


/**
 * @author bala.v
 *
 */
public interface OsanedB2BCustomerAccountService
{

	/**
	 * @param b2bCustomerData
	 * @throws CPIConnectionFailureException
	 */
	public void registerB2BCustomer(final B2BCustomerData b2bCustomerData) throws CPIConnectionFailureException;

	/**
	 * @param userId
	 * @param priceRange
	 */
	public void setPriceRangeInB2BUnit(String userId, String priceRange);


}
