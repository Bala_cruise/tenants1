/**
 *
 */
package de.hybris.osaned.core.exception;

/**
 * @author Bala Murugan
 *
 */
public class CPIConnectionFailureException extends Exception
{
	public CPIConnectionFailureException(final String message)
	{
		super(message);
	}
}
