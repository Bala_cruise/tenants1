/**
 *
 */
package de.hybris.osaned.core.customer.impl;

import de.hybris.osaned.core.cpi.CPIConnectionUtil;
import de.hybris.osaned.core.customer.OsanedFinanceB2BCustomerService;
import de.hybris.osaned.core.enums.B2BCustomerRoleEnum;
import de.hybris.osaned.core.event.FinanceManagerEvent;
import de.hybris.osaned.core.event.VerificationEvent;
import de.hybris.osaned.core.model.FinanceCustomerMasterDataModel;
import de.hybris.osaned.core.model.FinanceManagerOpenBalanceModel;
import de.hybris.platform.b2b.constants.B2BConstants;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.b2bacceleratorservices.customer.impl.DefaultB2BCustomerAccountService;
import de.hybris.platform.commercefacades.user.data.B2BCustomerData;
import de.hybris.platform.commercefacades.user.data.CustomerFinanceMasterSetupData;
import de.hybris.platform.commercefacades.user.data.FinanceManagerOpenBalanceData;
import de.hybris.platform.commerceservices.event.AbstractCommerceUserEvent;
import de.hybris.platform.commerceservices.event.RegisterEvent;
import de.hybris.platform.commerceservices.security.SecureToken;
import de.hybris.platform.commerceservices.security.SecureTokenService;
import de.hybris.platform.core.model.user.UserGroupModel;
import de.hybris.platform.enumeration.EnumerationService;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.event.EventService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;

import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import net.minidev.json.JSONObject;


/**
 * @author balamurugan
 *
 */
public class DefaultOsanedFinanceB2BCustomerServiceImpl extends DefaultB2BCustomerAccountService
		implements OsanedFinanceB2BCustomerService
{
	@Resource
	private ModelService modelService;
	@Resource
	private ConfigurationService configurationService;
	@Resource
	private SecureTokenService secureTokenService;
	@Resource
	private UserService userService;
	@Resource
	private EnumerationService enumerationService;
	@Resource
	private CPIConnectionUtil cpiConnectionUtil;
	@Resource
	private EventService eventService;
	private final Logger LOG = Logger.getLogger(DefaultOsanedFinanceB2BCustomerServiceImpl.class);

	@Override
	public void createFinanceCustomer(final B2BCustomerData b2bCustomerData)
	{

		final B2BCustomerModel b2bCustomerModel = modelService.create(B2BCustomerModel.class);
		b2bCustomerModel.setUid(b2bCustomerData.getEmail().toLowerCase());
		b2bCustomerModel.setEmail(b2bCustomerData.getEmail().toLowerCase());
		b2bCustomerModel.setName(b2bCustomerData.getName());
		b2bCustomerModel.setEmployeeId(b2bCustomerData.getEmployeeId());
		b2bCustomerModel.setIqamaId(b2bCustomerData.getIqamaId());
		b2bCustomerModel.setPassword(configurationService.getConfiguration().getString("b2bcustomer.defaultpassword"));
		b2bCustomerModel.setLoginDisabled(Boolean.TRUE);
		b2bCustomerModel.setSentToCPI(Boolean.FALSE);
		final UserGroupModel b2bCustomerGroup = userService.getUserGroupForUID(B2BConstants.B2BCUSTOMERGROUP);
		b2bCustomerModel.setDefaultB2BUnit(getFinanceB2BUnit(b2bCustomerData));
		b2bCustomerModel.setGroups(Collections.singleton(b2bCustomerGroup));
		final Set<B2BCustomerRoleEnum> B2BCustomerRoleEnumList = new HashSet<>();
		B2BCustomerRoleEnumList.add(enumerationService.getEnumerationValue(B2BCustomerRoleEnum.class, b2bCustomerData.getRole()));
		b2bCustomerModel.setCustomerRole(B2BCustomerRoleEnumList);
		modelService.save(b2bCustomerModel);
		setEmployeeInParentEmployee(b2bCustomerData, b2bCustomerModel);
		b2bCustomerModel.setVerificationUserId(b2bCustomerData.getVerificationUser());
		if (cpiConnectionUtil.sendDataToCpi(getConvertedJsonCustomerData(b2bCustomerModel)))
		{
			b2bCustomerModel.setSentToCPI(Boolean.TRUE);
			modelService.save(b2bCustomerModel);
		}
	}

	/**
	 * @param b2bCustomerData
	 * @return
	 */
	protected String generateSecureToken(final B2BCustomerData b2bCustomerData)
	{
		final SecureToken secureToken = new SecureToken(b2bCustomerData.getEmail().toLowerCase(), new Date().getTime());
		return secureTokenService.encryptData(secureToken);
	}

	/**
	 * @param b2bCustomerData
	 * @return
	 */
	protected String generateSecureToken(final B2BCustomerModel b2bCustomerModel)
	{
		final SecureToken secureToken = new SecureToken(b2bCustomerModel.getEmail().toLowerCase(), new Date().getTime());
		return secureTokenService.encryptData(secureToken);
	}

	/**
	 * @return
	 */
	protected String generateOtpNumber()
	{
		final Random random = new Random();
		return String.valueOf(random.nextInt(1000000));
	}

	/**
	 * @param b2bCustomerData
	 * @return
	 */
	private B2BUnitModel getFinanceB2BUnit(final B2BCustomerData b2bCustomerData)
	{
		final B2BCustomerModel parentEmployee = userService.getUserForUID(b2bCustomerData.getFinaceCustomerId(),
				B2BCustomerModel.class);
		return parentEmployee.getDefaultB2BUnit();

	}

	/**
	 * @param b2bCustomerData
	 * @param b2bCustomerModel
	 */
	private void setEmployeeInParentEmployee(final B2BCustomerData b2bCustomerData, final B2BCustomerModel b2bCustomerModel)
	{

		final B2BCustomerModel parentEmployee = userService.getUserForUID(b2bCustomerData.getFinaceCustomerId(),
				B2BCustomerModel.class);
		if (CollectionUtils.isEmpty(parentEmployee.getSubEmployeeIds()))
		{
			parentEmployee.setSubEmployeeIds(Collections.singleton(b2bCustomerModel.getEmail()));
		}
		else
		{
			final Set<String> subEmployeeIdList = new HashSet<>(parentEmployee.getSubEmployeeIds());
			subEmployeeIdList.add(b2bCustomerModel.getEmail());
			parentEmployee.setSubEmployeeIds(subEmployeeIdList);
		}
		modelService.save(parentEmployee);

	}

	/**
	 * @param b2bCustomerModel
	 * @return
	 */
	private String getConvertedJsonCustomerData(final B2BCustomerModel b2bCustomerModel)
	{
		final JSONObject json = new JSONObject();
		json.put("emailId", b2bCustomerModel.getEmail());
		json.put("name", b2bCustomerModel.getName());
		json.put("employeeId", b2bCustomerModel.getEmployeeId());
		json.put("iqamaId", b2bCustomerModel.getIqamaId());
		return json.toString();
	}

	/**
	 * @param event
	 * @param customerModel
	 * @return
	 */
	protected AbstractCommerceUserEvent initializeEvent(final AbstractCommerceUserEvent event,
			final B2BCustomerModel customerModel)
	{
		event.setBaseStore(getBaseStoreService().getCurrentBaseStore());
		event.setSite(getBaseSiteService().getCurrentBaseSite());
		event.setCustomer(customerModel);
		event.setLanguage(getCommonI18NService().getCurrentLanguage());
		event.setCurrency(getCommonI18NService().getCurrentCurrency());
		return event;
	}

	@Override
	public void setAuthMatrixForUser(final String userId, final String approverLevel1, final String approverLevel2,
			final String approverLevel3)
	{
		final B2BCustomerModel b2bCustomerModel = userService.getUserForUID(userId, B2BCustomerModel.class);
		if (!StringUtils.isEmpty(approverLevel1))
		{
			b2bCustomerModel.setApproverLevel1(approverLevel1);
		}
		if (!StringUtils.isEmpty(approverLevel2))
		{
			b2bCustomerModel.setApproverLevel2(approverLevel2);
		}
		if (!StringUtils.isEmpty(approverLevel3))
		{
			b2bCustomerModel.setApproverLevel3(approverLevel3);
		}
		modelService.save(b2bCustomerModel);
	}

	@Override
	public void setCustomerMasterData(final CustomerFinanceMasterSetupData customerFinanceMasterSetupData, final String userId)
	{
		final B2BCustomerModel b2bCustomerModel = userService.getUserForUID(userId, B2BCustomerModel.class);
		setFinanceCustomerMasterDatatoCustomer(b2bCustomerModel, customerFinanceMasterSetupData);
		final ObjectMapper jsonMapper = new ObjectMapper();
		try
		{
			final String jsonData = jsonMapper.writeValueAsString(customerFinanceMasterSetupData);

			if (cpiConnectionUtil.sendDataToCpi(jsonData))
			{
				b2bCustomerModel.setFinanceMasterDataSentToCPI(Boolean.TRUE);
				setUserinVerificationUser(b2bCustomerModel);
				getModelService().save(b2bCustomerModel);
			}

		}
		catch (final JsonProcessingException e)
		{
			LOG.debug(e.getMessage());
			b2bCustomerModel.setFinanceMasterDataSentToCPI(Boolean.FALSE);
			getModelService().save(b2bCustomerModel);
		}
	}

	/**
	 * @param b2bCustomerModel
	 */
	private void setUserinFinanceManagerUser(final B2BCustomerModel b2bCustomerModel)
	{
		final String financeManagerUserId = configurationService.getConfiguration()
				.getString("b2bcustomer.account.financemanager.emailid");
		final B2BCustomerModel financeManagerUser = userService.getUserForUID(financeManagerUserId, B2BCustomerModel.class);
		if (CollectionUtils.isEmpty(financeManagerUser.getSubEmployeeIds()))
		{
			financeManagerUser.setSubEmployeeIds(Collections.singleton(b2bCustomerModel.getUid()));
		}
		else
		{
			final Set<String> subEmployeeIdList = new HashSet<>(financeManagerUser.getSubEmployeeIds());
			subEmployeeIdList.add(b2bCustomerModel.getUid());
			financeManagerUser.setSubEmployeeIds(subEmployeeIdList);
		}
		modelService.save(financeManagerUser);
		eventService.publishEvent(initializeEvent(new FinanceManagerEvent(), financeManagerUser));
	}

	/**
	 * @param b2bCustomerModel
	 */
	private void removeUserinVerificationUser(final B2BCustomerModel b2bCustomerModel)
	{
		final String verificationUserId = configurationService.getConfiguration()
				.getString("b2bcustomer.account.verification.emailid");
		final B2BCustomerModel verificationUser = userService.getUserForUID(verificationUserId, B2BCustomerModel.class);
		final Set<String> subEmployeeIdList = new HashSet<>(verificationUser.getSubEmployeeIds());
		subEmployeeIdList.remove(b2bCustomerModel.getUid());
		verificationUser.setSubEmployeeIds(subEmployeeIdList);
		modelService.save(verificationUser);
	}

	/**
	 * @param b2bCustomerModel
	 */
	private void setUserinVerificationUser(final B2BCustomerModel b2bCustomerModel)
	{
		final String verificationUserId = configurationService.getConfiguration()
				.getString("b2bcustomer.account.verification.emailid");
		final B2BCustomerModel verificationUser = userService.getUserForUID(verificationUserId, B2BCustomerModel.class);
		if (CollectionUtils.isEmpty(verificationUser.getSubEmployeeIds()))
		{
			verificationUser.setSubEmployeeIds(Collections.singleton(b2bCustomerModel.getUid()));
		}
		else
		{
			final Set<String> subEmployeeIdList = new HashSet<>(verificationUser.getSubEmployeeIds());
			subEmployeeIdList.add(b2bCustomerModel.getUid());
			verificationUser.setSubEmployeeIds(subEmployeeIdList);
		}
		modelService.save(verificationUser);
		eventService.publishEvent(initializeEvent(new VerificationEvent(), verificationUser));
	}

	/**
	 * @param b2bCustomerModel
	 * @param customerFinanceMasterSetupData
	 */
	private void setFinanceCustomerMasterDatatoCustomer(final B2BCustomerModel b2bCustomerModel,
			final CustomerFinanceMasterSetupData customerFinanceMasterSetupData)
	{

		final FinanceCustomerMasterDataModel financeCustomerMasterDataModel = modelService
				.create(FinanceCustomerMasterDataModel.class);
		financeCustomerMasterDataModel.setUid(b2bCustomerModel.getName() + generateOtpNumber());
		financeCustomerMasterDataModel.setCustomerBpCode(customerFinanceMasterSetupData.getCustomer().getBpCode());
		financeCustomerMasterDataModel.setCustomerAddress(customerFinanceMasterSetupData.getCustomer().getAddress());
		financeCustomerMasterDataModel.setCustomerName(customerFinanceMasterSetupData.getCustomer().getName());
		financeCustomerMasterDataModel.setCustomerVatNumber(customerFinanceMasterSetupData.getCustomer().getVatNumber());
		financeCustomerMasterDataModel.setCustomerFinancialGroup(customerFinanceMasterSetupData.getCustomer().getFinancialGroup());
		financeCustomerMasterDataModel.setCustomerCurrency(customerFinanceMasterSetupData.getCustomer().getCurrency());

		financeCustomerMasterDataModel.setSupplierBpCode(customerFinanceMasterSetupData.getSupplier().getBpCode());
		financeCustomerMasterDataModel.setSupplierAddress(customerFinanceMasterSetupData.getSupplier().getAddress());
		financeCustomerMasterDataModel.setSupplierName(customerFinanceMasterSetupData.getSupplier().getName());
		financeCustomerMasterDataModel.setSupplierVatNumber(customerFinanceMasterSetupData.getSupplier().getVatNumber());
		financeCustomerMasterDataModel.setSupplierFinancialGroup(customerFinanceMasterSetupData.getSupplier().getFinancialGroup());
		financeCustomerMasterDataModel.setSupplierCurrency(customerFinanceMasterSetupData.getSupplier().getCurrency());

		financeCustomerMasterDataModel.setItemCode(customerFinanceMasterSetupData.getItem().getItemCode());
		financeCustomerMasterDataModel.setDescription(customerFinanceMasterSetupData.getItem().getDescription());
		financeCustomerMasterDataModel.setType(customerFinanceMasterSetupData.getItem().getItemType());
		financeCustomerMasterDataModel.setUnitSet(customerFinanceMasterSetupData.getItem().getUnitSet());
		financeCustomerMasterDataModel.setItemGroup(customerFinanceMasterSetupData.getItem().getItemGroup());
		financeCustomerMasterDataModel.setInventoryUnit(customerFinanceMasterSetupData.getItem().getInventoryUnit());
		financeCustomerMasterDataModel.setCurrency(customerFinanceMasterSetupData.getItem().getCurrency());
		modelService.save(financeCustomerMasterDataModel);
		b2bCustomerModel.setFinanceMasterData(financeCustomerMasterDataModel);
		modelService.save(b2bCustomerModel);
	}

	@Override
	public FinanceCustomerMasterDataModel getCustomerMasterData(final String userId)
	{
		final B2BCustomerModel b2bCustomerModel = userService.getUserForUID(userId, B2BCustomerModel.class);
		if (null != b2bCustomerModel.getFinanceMasterData())
		{
			return b2bCustomerModel.getFinanceMasterData();
		}
		return null;
	}

	@Override
	public void updateCustomerMasterData(final CustomerFinanceMasterSetupData customerFinanceMasterSetupData, final String userId)
	{
		final B2BCustomerModel b2bCustomerModel = userService.getUserForUID(userId, B2BCustomerModel.class);
		if (null != b2bCustomerModel.getFinanceMasterData())
		{
			final FinanceCustomerMasterDataModel financeCustomerMasterDataModel = b2bCustomerModel.getFinanceMasterData();
			updateFinanceCustomerMasterDatatoCustomer(financeCustomerMasterDataModel, customerFinanceMasterSetupData);
		}
		final ObjectMapper jsonMapper = new ObjectMapper();
		try
		{
			final String jsonData = jsonMapper.writeValueAsString(customerFinanceMasterSetupData);

			if (cpiConnectionUtil.sendDataToCpi(jsonData))
			{
				b2bCustomerModel.setFinanceMasterDataSentToCPI(Boolean.TRUE);
				removeUserinVerificationUser(b2bCustomerModel);
				setUserinFinanceManagerUser(b2bCustomerModel);
				getModelService().save(b2bCustomerModel);
			}

		}
		catch (final JsonProcessingException e)
		{
			LOG.debug(e.getMessage());
			b2bCustomerModel.setFinanceMasterDataSentToCPI(Boolean.FALSE);
			getModelService().save(b2bCustomerModel);
		}
	}

	/**
	 * @param financeCustomerMasterDataModel
	 * @param customerFinanceMasterSetupData
	 */
	private void updateFinanceCustomerMasterDatatoCustomer(final FinanceCustomerMasterDataModel financeCustomerMasterDataModel,
			final CustomerFinanceMasterSetupData customerFinanceMasterSetupData)
	{
		financeCustomerMasterDataModel.setCustomerBpCode(customerFinanceMasterSetupData.getCustomer().getBpCode());
		financeCustomerMasterDataModel.setCustomerAddress(customerFinanceMasterSetupData.getCustomer().getAddress());
		financeCustomerMasterDataModel.setCustomerName(customerFinanceMasterSetupData.getCustomer().getName());
		financeCustomerMasterDataModel.setCustomerVatNumber(customerFinanceMasterSetupData.getCustomer().getVatNumber());
		financeCustomerMasterDataModel.setCustomerFinancialGroup(customerFinanceMasterSetupData.getCustomer().getFinancialGroup());
		financeCustomerMasterDataModel.setCustomerCurrency(customerFinanceMasterSetupData.getCustomer().getCurrency());

		financeCustomerMasterDataModel.setSupplierBpCode(customerFinanceMasterSetupData.getSupplier().getBpCode());
		financeCustomerMasterDataModel.setSupplierAddress(customerFinanceMasterSetupData.getSupplier().getAddress());
		financeCustomerMasterDataModel.setSupplierName(customerFinanceMasterSetupData.getSupplier().getName());
		financeCustomerMasterDataModel.setSupplierVatNumber(customerFinanceMasterSetupData.getSupplier().getVatNumber());
		financeCustomerMasterDataModel.setSupplierFinancialGroup(customerFinanceMasterSetupData.getSupplier().getFinancialGroup());
		financeCustomerMasterDataModel.setSupplierCurrency(customerFinanceMasterSetupData.getSupplier().getCurrency());

		financeCustomerMasterDataModel.setItemCode(customerFinanceMasterSetupData.getItem().getItemCode());
		financeCustomerMasterDataModel.setDescription(customerFinanceMasterSetupData.getItem().getDescription());
		financeCustomerMasterDataModel.setType(customerFinanceMasterSetupData.getItem().getItemType());
		financeCustomerMasterDataModel.setUnitSet(customerFinanceMasterSetupData.getItem().getUnitSet());
		financeCustomerMasterDataModel.setItemGroup(customerFinanceMasterSetupData.getItem().getItemGroup());
		financeCustomerMasterDataModel.setInventoryUnit(customerFinanceMasterSetupData.getItem().getInventoryUnit());
		financeCustomerMasterDataModel.setCurrency(customerFinanceMasterSetupData.getItem().getCurrency());
		modelService.save(financeCustomerMasterDataModel);
	}

	@Override
	public void setFinanceOpenBalance(final FinanceManagerOpenBalanceData financeManagerOpenBalanceData, final String userId)
	{
		final B2BCustomerModel b2bCustomerModel = userService.getUserForUID(userId, B2BCustomerModel.class);
		setFinanceOpenBalanceDatatoCustomer(b2bCustomerModel, financeManagerOpenBalanceData);
		final ObjectMapper jsonMapper = new ObjectMapper();
		try
		{
			final String jsonData = jsonMapper.writeValueAsString(financeManagerOpenBalanceData);

			if (cpiConnectionUtil.sendDataToCpi(jsonData))
			{
				b2bCustomerModel.setFinanceOpenBalanceDataSentToCPI(Boolean.TRUE);
				removeUserinFinanceManagerUser(b2bCustomerModel);
				triggerEmailToAllSubEmployees(b2bCustomerModel);
				getModelService().save(b2bCustomerModel);
			}

		}
		catch (final JsonProcessingException e)
		{
			LOG.debug(e.getMessage());
			b2bCustomerModel.setFinanceOpenBalanceDataSentToCPI(Boolean.TRUE);
			getModelService().save(b2bCustomerModel);
		}
	}

	/**
	 * @param b2bCustomerModel
	 */
	private void triggerEmailToAllSubEmployees(final B2BCustomerModel b2bCustomerModel)
	{
		if (!CollectionUtils.isEmpty(b2bCustomerModel.getSubEmployeeIds()))
		{
			b2bCustomerModel.getSubEmployeeIds().forEach(emailId -> {
				final B2BCustomerModel subEmployee = userService.getUserForUID(emailId, B2BCustomerModel.class);
				subEmployee.setEmailVerificationToken(generateSecureToken(subEmployee));
				subEmployee.setOtp(generateOtpNumber());
				modelService.save(subEmployee);
				getEventService().publishEvent(initializeEvent(new RegisterEvent(), subEmployee));
			});
		}

	}

	/**
	 * @param b2bCustomerModel
	 */
	private void removeUserinFinanceManagerUser(final B2BCustomerModel b2bCustomerModel)
	{
		final String FinanceManagerUserId = configurationService.getConfiguration()
				.getString("b2bcustomer.account.financemanager.emailid");
		final B2BCustomerModel financeManagerUser = userService.getUserForUID(FinanceManagerUserId, B2BCustomerModel.class);
		final Set<String> subEmployeeIdList = new HashSet<>(financeManagerUser.getSubEmployeeIds());
		subEmployeeIdList.remove(b2bCustomerModel.getUid());
		financeManagerUser.setSubEmployeeIds(subEmployeeIdList);
		modelService.save(financeManagerUser);

	}

	/**
	 * @param b2bCustomerModel
	 * @param financeManagerOpenBalanceData
	 */
	private void setFinanceOpenBalanceDatatoCustomer(final B2BCustomerModel b2bCustomerModel,
			final FinanceManagerOpenBalanceData financeManagerOpenBalanceData)
	{
		final FinanceManagerOpenBalanceModel financeManagerOpenBalanceModel = modelService
				.create(FinanceManagerOpenBalanceModel.class);
		financeManagerOpenBalanceModel.setUid(b2bCustomerModel.getName() + generateOtpNumber());
		financeManagerOpenBalanceModel.setItemCode(financeManagerOpenBalanceData.getItemUpload().getItemCode());
		financeManagerOpenBalanceModel.setItemDescription(financeManagerOpenBalanceData.getItemUpload().getItemDescription());
		financeManagerOpenBalanceModel.setItemGroup(financeManagerOpenBalanceData.getItemUpload().getItemGroup());
		financeManagerOpenBalanceModel.setWarehouse(financeManagerOpenBalanceData.getItemUpload().getWarehouse());
		financeManagerOpenBalanceModel.setInventoryUnit(financeManagerOpenBalanceData.getItemUpload().getInventoryUnit());
		financeManagerOpenBalanceModel.setQuantity(financeManagerOpenBalanceData.getItemUpload().getQuantity());
		financeManagerOpenBalanceModel.setPrice(financeManagerOpenBalanceData.getItemUpload().getPrice());

		financeManagerOpenBalanceModel.setDocumentDate(financeManagerOpenBalanceData.getBpTransUpload().getDocumentDate());
		financeManagerOpenBalanceModel.setDocumentNumber(financeManagerOpenBalanceData.getBpTransUpload().getDocumentNumber());
		financeManagerOpenBalanceModel.setBusinessPartner(financeManagerOpenBalanceData.getBpTransUpload().getBusinessPartner());
		financeManagerOpenBalanceModel.setInvoiceNumber(financeManagerOpenBalanceData.getBpTransUpload().getInvoiceNumber());
		financeManagerOpenBalanceModel.setCurrency(financeManagerOpenBalanceData.getBpTransUpload().getCurrency());
		financeManagerOpenBalanceModel
				.setAmountInInvoiceCurrency(financeManagerOpenBalanceData.getBpTransUpload().getAmountInInvoiceCurrency());
		financeManagerOpenBalanceModel.setAmountInHC(financeManagerOpenBalanceData.getBpTransUpload().getAmountInHC());
		financeManagerOpenBalanceModel.setBalanceAmount(financeManagerOpenBalanceData.getBpTransUpload().getBalanceAmount());
		financeManagerOpenBalanceModel.setBpTransUploadReference(financeManagerOpenBalanceData.getBpTransUpload().getReference());

		financeManagerOpenBalanceModel.setDate(financeManagerOpenBalanceData.getOpeningBalance().getDate());
		financeManagerOpenBalanceModel.setLedger(financeManagerOpenBalanceData.getOpeningBalance().getLedger());
		financeManagerOpenBalanceModel.setDr(financeManagerOpenBalanceData.getOpeningBalance().getDr());
		financeManagerOpenBalanceModel.setAmount(financeManagerOpenBalanceData.getOpeningBalance().getAmount());
		financeManagerOpenBalanceModel.setOpeningBalanceReference(financeManagerOpenBalanceData.getOpeningBalance().getReference());
		modelService.save(financeManagerOpenBalanceModel);
		b2bCustomerModel.setFinanceOpeningBalance(financeManagerOpenBalanceModel);
		modelService.save(b2bCustomerModel);
	}

	@Override
	public void updateFinanceCustomer(final B2BCustomerData b2bCustomerData)
	{
		final B2BCustomerModel b2bCustomerModel = getUserService().getUserForUID(b2bCustomerData.getEmail().toLowerCase(),
				B2BCustomerModel.class);
		final Set<B2BCustomerRoleEnum> customerRoles = new HashSet<>(b2bCustomerModel.getCustomerRole());
		customerRoles.add(enumerationService.getEnumerationValue(B2BCustomerRoleEnum.class, b2bCustomerData.getRole()));
		b2bCustomerModel.setCustomerRole(customerRoles);
		modelService.save(b2bCustomerModel);

	}


}
