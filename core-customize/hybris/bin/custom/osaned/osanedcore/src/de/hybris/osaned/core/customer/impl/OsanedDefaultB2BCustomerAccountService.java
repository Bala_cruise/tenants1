/**
 *
 */
package de.hybris.osaned.core.customer.impl;


import de.hybris.osaned.core.cpi.CPIConnectionUtil;
import de.hybris.osaned.core.customer.OsanedB2BCustomerAccountService;
import de.hybris.osaned.core.enums.B2BCustomerRoleEnum;
import de.hybris.osaned.core.exception.CPIConnectionFailureException;
import de.hybris.platform.b2b.constants.B2BConstants;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.b2b.services.B2BUnitService;
import de.hybris.platform.b2bacceleratorservices.customer.impl.DefaultB2BCustomerAccountService;
import de.hybris.platform.catalog.model.CompanyModel;
import de.hybris.platform.commercefacades.user.data.B2BCustomerData;
import de.hybris.platform.commerceservices.event.AbstractCommerceUserEvent;
import de.hybris.platform.commerceservices.event.RegisterEvent;
import de.hybris.platform.commerceservices.security.SecureToken;
import de.hybris.platform.core.model.security.PrincipalGroupModel;
import de.hybris.platform.core.model.user.UserGroupModel;
import de.hybris.platform.enumeration.EnumerationService;
import de.hybris.platform.europe1.enums.UserPriceGroup;
import de.hybris.platform.servicelayer.event.EventService;

import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.Locale;
import java.util.Random;
import java.util.Set;

import org.springframework.util.CollectionUtils;

import net.minidev.json.JSONObject;


/**
 * @author bala.v
 *
 */
public class OsanedDefaultB2BCustomerAccountService extends DefaultB2BCustomerAccountService
		implements OsanedB2BCustomerAccountService
{
	private B2BUnitService b2bUnitService;
	private EnumerationService enumerationService;
	private EventService eventService;
	private CPIConnectionUtil cpiConnectionUtil;
	private static final String CUSTOMERGROUP = "customergroup";
	private static final String PARENTUNIT = "ParentUnit";

	/**
	 * registerB2BCustomer
	 *
	 * @throws CPIConnectionFailureException
	 */
	@Override
	public void registerB2BCustomer(final B2BCustomerData b2bCustomerData) throws CPIConnectionFailureException
	{
		final B2BCustomerModel b2bCustomerModel = getModelService().create(B2BCustomerModel.class);
		b2bCustomerModel.setUid(b2bCustomerData.getEmail().toLowerCase());
		b2bCustomerModel.setEmail(b2bCustomerData.getEmail().toLowerCase());
		b2bCustomerModel.setName(b2bCustomerData.getName());
		b2bCustomerModel.setContact(b2bCustomerData.getContact());
		b2bCustomerModel.setCompany(b2bCustomerData.getCompany());
		b2bCustomerModel.setSizeRange(b2bCustomerData.getSizeRange());
		b2bCustomerModel.setPassword(getConfigurationService().getConfiguration().getString("b2bcustomer.defaultpassword"));
		b2bCustomerModel.setLoginDisabled(Boolean.TRUE);
		b2bCustomerModel.setEmailVerificationToken(generateSecureToken(b2bCustomerData));
		b2bCustomerModel.setOtp(generateOtpNumber());
		b2bCustomerModel.setSentToCPI(Boolean.FALSE);
		b2bCustomerModel.setFinanceMasterDataSentToCPI(Boolean.FALSE);
		b2bCustomerModel.setFinanceOpenBalanceDataSentToCPI(Boolean.FALSE);
		b2bCustomerModel.setOrderApprovalStatus(Boolean.TRUE);
		b2bCustomerModel.setCommercialRegistration(b2bCustomerData.getCommercialRegistration());
		final UserGroupModel b2bCustomerGroup = getUserService().getUserGroupForUID(B2BConstants.B2BCUSTOMERGROUP);
		b2bCustomerModel.setDefaultB2BUnit(createB2BUnit(b2bCustomerData));
		b2bCustomerModel.setGroups(Collections.singleton(b2bCustomerGroup));
		final Set<B2BCustomerRoleEnum> B2BCustomerRoleEnumList = new HashSet<>();
		B2BCustomerRoleEnumList.add(B2BCustomerRoleEnum.SUPERADMIN);
		b2bCustomerModel.setCustomerRole(B2BCustomerRoleEnumList);
		getModelService().save(b2bCustomerModel);
		if (getCpiConnectionUtil().sendDataToCpi(getConvertedJsonData(b2bCustomerModel)))
		{
			b2bCustomerModel.setSentToCPI(Boolean.TRUE);
			getModelService().save(b2bCustomerModel);
		}
		getEventService().publishEvent(initializeEvent(new RegisterEvent(), b2bCustomerModel));
	}

	/**
	 * @return
	 */
	protected String generateOtpNumber()
	{
		final Random random = new Random();
		return String.valueOf(random.nextInt(1000000));
	}

	/**
	 * @param b2bCustomerData
	 * @return
	 */
	protected String generateSecureToken(final B2BCustomerData b2bCustomerData)
	{
		final SecureToken secureToken = new SecureToken(b2bCustomerData.getEmail().toLowerCase(), new Date().getTime());
		return getSecureTokenService().encryptData(secureToken);
	}

	/**
	 * @param b2bCustomerModel
	 * @return String
	 */
	private String getConvertedJsonData(final B2BCustomerModel b2bCustomerModel)
	{

		final JSONObject json = new JSONObject();
		json.put("Name", b2bCustomerModel.getName());
		json.put("email", b2bCustomerModel.getEmail());
		json.put("contact", b2bCustomerModel.getContact());
		json.put("company", b2bCustomerModel.getCompany());
		final UserPriceGroup userPriceGroup = getEnumerationService().getEnumerationValue(UserPriceGroup.class,
				b2bCustomerModel.getSizeRange());
		json.put("sizeRange", getEnumerationService().getEnumerationName(userPriceGroup, Locale.ENGLISH));
		json.put("commercialRegistration", b2bCustomerModel.getCommercialRegistration());
		return json.toString();
	}

	/**
	 * @param b2bCustomerData
	 * @return
	 */
	protected B2BUnitModel createB2BUnit(final B2BCustomerData b2bCustomerData)
	{
		final B2BUnitModel b2bUnitModel = getModelService().create(B2BUnitModel.class);
		final Random random = new Random();
		final int randonumber = random.nextInt(10000);
		b2bUnitModel.setUid(b2bCustomerData.getName() + b2bCustomerData.getCompany() + randonumber);
		b2bUnitModel.setName(b2bCustomerData.getName() + b2bCustomerData.getCompany() + randonumber);
		b2bUnitModel
				.setUserPriceGroup(getEnumerationService().getEnumerationValue(UserPriceGroup.class, b2bCustomerData.getSizeRange()));
		b2bUnitModel.setGroups(getParentUnit());
		getModelService().save(b2bUnitModel);
		return b2bUnitModel;

	}

	/**
	 * @return Set<PrincipalGroupModel>
	 */
	protected Set<PrincipalGroupModel> getParentUnit()
	{
		final Set<PrincipalGroupModel> groups = new HashSet<PrincipalGroupModel>();
		final CompanyModel companyModel = getB2bUnitService().getUnitForUid(PARENTUNIT);
		final UserGroupModel customerGroup = getUserService().getUserGroupForUID(CUSTOMERGROUP);
		groups.add(companyModel);
		groups.add(customerGroup);
		return groups;
	}

	@Override
	public void setPriceRangeInB2BUnit(final String userId, final String priceRange)
	{

		final B2BCustomerModel b2bCustomerModel = getUserService().getUserForUID(userId, B2BCustomerModel.class);
		b2bCustomerModel.setSizeRange(priceRange);
		if (!CollectionUtils.isEmpty(b2bCustomerModel.getGroups()))
		{
			b2bCustomerModel.getGroups().forEach(grp -> {
				if (grp instanceof B2BUnitModel)
				{
					final B2BUnitModel b2bUnitModel = (B2BUnitModel) grp;
					b2bUnitModel.setUserPriceGroup(getEnumerationService().getEnumerationValue(UserPriceGroup.class, priceRange));
					getModelService().save(b2bCustomerModel);
					getModelService().save(b2bUnitModel);
				}
			});
		}
	}

	/**
	 * @return the b2bUnitService
	 */
	public B2BUnitService getB2bUnitService()
	{
		return b2bUnitService;
	}

	/**
	 * @param b2bUnitService
	 *           the b2bUnitService to set
	 */
	public void setB2bUnitService(final B2BUnitService b2bUnitService)
	{
		this.b2bUnitService = b2bUnitService;
	}

	protected AbstractCommerceUserEvent initializeEvent(final AbstractCommerceUserEvent event,
			final B2BCustomerModel customerModel)
	{
		event.setBaseStore(getBaseStoreService().getCurrentBaseStore());
		event.setSite(getBaseSiteService().getCurrentBaseSite());
		event.setCustomer(customerModel);
		event.setLanguage(getCommonI18NService().getCurrentLanguage());
		event.setCurrency(getCommonI18NService().getCurrentCurrency());
		return event;
	}

	/**
	 * @return the enumerationService
	 */
	public EnumerationService getEnumerationService()
	{
		return enumerationService;
	}

	/**
	 * @param enumerationService
	 *           the enumerationService to set
	 */
	public void setEnumerationService(final EnumerationService enumerationService)
	{
		this.enumerationService = enumerationService;
	}

	/**
	 * @return the eventService
	 */
	@Override
	public EventService getEventService()
	{
		return eventService;
	}

	/**
	 * @param eventService
	 *           the eventService to set
	 */
	@Override
	public void setEventService(final EventService eventService)
	{
		this.eventService = eventService;
	}

	/**
	 * @return the cpiConnectionUtil
	 */
	public CPIConnectionUtil getCpiConnectionUtil()
	{
		return cpiConnectionUtil;
	}

	/**
	 * @param cpiConnectionUtil
	 *           the cpiConnectionUtil to set
	 */
	public void setCpiConnectionUtil(final CPIConnectionUtil cpiConnectionUtil)
	{
		this.cpiConnectionUtil = cpiConnectionUtil;
	}

}
