/**
 *
 */
package de.hybris.osaned.core.customer;

import de.hybris.platform.commercefacades.user.data.B2BCustomerData;
import de.hybris.platform.commercefacades.user.data.EmployeeMasterSetupData;


/**
 * @author Bala Murugan
 *
 */
public interface OsanedHRB2BCustomerService
{
	/**
	 * @param b2bCustomerData
	 */
	public void registerHRCustomer(B2BCustomerData b2bCustomerData);

	/**
	 * @param b2bCustomerData
	 */
	public void updateHRCustomer(B2BCustomerData b2bCustomerData);

	/**
	 * @param employeeMasterSetupData
	 * @param userId
	 */
	public void sendEmployeeMasterDataSetupToCPI(EmployeeMasterSetupData employeeMasterSetupData, String userId);

	/**
	 * @param emailId
	 */
	public void retriggerEmailtoEmployee(String emailId);
}
