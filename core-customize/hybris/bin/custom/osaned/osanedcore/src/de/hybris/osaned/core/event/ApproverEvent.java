/**
 *
 */
package de.hybris.osaned.core.event;

import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commerceservices.event.AbstractCommerceUserEvent;


/**
 * @author balamurugan
 *
 */
public class ApproverEvent extends AbstractCommerceUserEvent<BaseSiteModel>
{

}
