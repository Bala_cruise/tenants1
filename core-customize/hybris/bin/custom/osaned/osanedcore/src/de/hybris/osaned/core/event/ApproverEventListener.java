/**
 *
 */
package de.hybris.osaned.core.event;

import de.hybris.platform.acceleratorservices.site.AbstractAcceleratorSiteEventListener;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commerceservices.enums.SiteChannel;
import de.hybris.platform.commerceservices.model.process.StoreFrontCustomerProcessModel;
import de.hybris.platform.processengine.BusinessProcessService;
import de.hybris.platform.servicelayer.model.ModelService;

import javax.annotation.Resource;

import org.apache.log4j.Logger;


/**
 * @author balamurugan
 *
 */
public class ApproverEventListener extends AbstractAcceleratorSiteEventListener<ApproverEvent>
{
	private final Logger LOG = Logger.getLogger(ApproverEventListener.class);
	@Resource
	private ModelService modelService;
	@Resource
	private BusinessProcessService businessProcessService;

	@Override
	protected SiteChannel getSiteChannelForEvent(final ApproverEvent event)
	{
		final BaseSiteModel site = event.getSite();
		return site.getChannel();
	}

	@Override
	protected void onSiteEvent(final ApproverEvent event)
	{
		final StoreFrontCustomerProcessModel storeFrontCustomerProcessModel = (StoreFrontCustomerProcessModel) businessProcessService
				.createProcess("approvalEmailProcess-" + event.getCustomer().getUid() + "-" + System.currentTimeMillis(),
						"approvalEmailProcess");
		storeFrontCustomerProcessModel.setCustomer(event.getCustomer());
		storeFrontCustomerProcessModel.setSite(event.getSite());
		storeFrontCustomerProcessModel.setStore(event.getBaseStore());
		storeFrontCustomerProcessModel.setCurrency(event.getCurrency());
		storeFrontCustomerProcessModel.setLanguage(event.getLanguage());
		modelService.save(storeFrontCustomerProcessModel);
		businessProcessService.startProcess(storeFrontCustomerProcessModel);
		LOG.info("started ApprovalEmailProcess");
	}

}
