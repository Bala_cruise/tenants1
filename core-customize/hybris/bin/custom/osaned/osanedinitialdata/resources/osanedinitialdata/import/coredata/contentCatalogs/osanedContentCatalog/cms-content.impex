# -----------------------------------------------------------------------
# Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
# -----------------------------------------------------------------------
#
# Import the CMS content for the site
#
$contentCatalog=osanedContentCatalog
$contentCV=catalogVersion(CatalogVersion.catalog(Catalog.id[default=$contentCatalog]),CatalogVersion.version[default=Staged])[default=$contentCatalog:Staged]

# Import config properties into impex macros
UPDATE GenericItem[processor=de.hybris.platform.commerceservices.impex.impl.ConfigPropertyImportProcessor];pk[unique=true]
$jarResourceCms=$config-jarResourceCmsValue

# Create PageTemplates
# These define the layout for pages
# "FrontendTemplateName" is used to define the JSP that should be used to render the page for pages with multiple possible layouts.
# "RestrictedPageTypes" is used to restrict templates to page types
INSERT_UPDATE PageTemplate;$contentCV[unique=true];uid[unique=true];name;frontendTemplateName;restrictedPageTypes(code);active[default=true]
;;OsanedHomePageTemplate;Osaned HomePage Template;layout/homePageLayout;CategoryPage,ContentPage
;;LoginPageTemplate;Login Page Template;;ContentPage;false;
;;HeaderPageTemplate;Header Page Template;;ContentPage;false;
;;FooterPageTemplate;Footer Page Template;;ContentPage;false;
;;SanedServicesPageTemplate;SanedServices  Page Template;;ContentPage;false
;;HRPortalPageTemplate;HRPortal Page Template;;ContentPage;false
;;HRServicePageTemplate;HRService Page Template;;ContentPage;false
;;AboutusPageTemplate;Aboutus Page Template;;ContentPage;false
;;TermsAndConditionPageTemplate;TermsAndCondition Page Template;;ContentPage;false
;;FinancePortalPageTemplate;FinancePortal Page Template;;ContentPage;false
;;FinanceManagerServicePageTemplate;FinanceManagerService Page Template;;ContentPage;false

# Add Velocity templates that are in the CMS Cockpit. These give a better layout for editing pages
# The FileLoaderValueTranslator loads a File into a String property. The templates could also be inserted in-line in this file.
#UPDATE PageTemplate;$contentCV[unique=true];uid[unique=true];velocityTemplate[translator=de.hybris.platform.commerceservices.impex.impl.FileLoaderValueTranslator]
#;;OsanedHomePageTemplate      ;$jarResourceCms/structure-view/structure_homePageTemplate.vm
#;;LoginPageTemplate         ;$jarResourceCms/structure-view/structure_loginPageTemplate.vm

#OsanedHomepage Page Templates
INSERT_UPDATE ContentSlotName;name[unique=true];template(uid,$contentCV)[unique=true][default='OsanedHomePageTemplate'];validComponentTypes(code);compTypeGroup(code)
;HomePageCarousel;;;wide
;SanedSolution;;;wide
;ValuableClient;;;wide
;NewsLetter;;;wide


#Header Page Templates
INSERT_UPDATE ContentSlotName;name[unique=true];template(uid,$contentCV)[unique=true][default='HeaderPageTemplate'];validComponentTypes(code);compTypeGroup(code)
;HeaderInfo;;;wide
;SiteLogo;;;logo
;NavigationBar;;;navigation

#Footer Page Templates
INSERT_UPDATE ContentSlotName;name[unique=true];template(uid,$contentCV)[unique=true][default='FooterPageTemplate'];validComponentTypes(code);compTypeGroup(code)
;FooterColumn1;;;wide
;FooterColumn2;;;wide
;FooterColumn3;;;wide
;FooterColumn4;;;wide
;CopyRights;;;wide

#SanedService Page Templates
INSERT_UPDATE ContentSlotName;name[unique=true];template(uid,$contentCV)[unique=true][default='SanedServicesPageTemplate'];validComponentTypes(code);compTypeGroup(code)
;SanedServicesSolution;;;wide

#HRPortal Page Templates
INSERT_UPDATE ContentSlotName;name[unique=true];template(uid,$contentCV)[unique=true][default='HRPortalPageTemplate'];validComponentTypes(code);compTypeGroup(code)
;HRPortalVideo;;;wide

#HRservice Page Templates
INSERT_UPDATE ContentSlotName;name[unique=true];template(uid,$contentCV)[unique=true][default='HRServicePageTemplate'];validComponentTypes(code);compTypeGroup(code)
;HRServiceVideo;;;wide
;HRServiceEmployeeMasterDatasetup;;;wide
;HRServicePolicySetup;;;wide
;HRServicePayrollProcess;;;wide
;HRServiceRecruitment;;;wide

#HRPortal Page Templates
INSERT_UPDATE ContentSlotName;name[unique=true];template(uid,$contentCV)[unique=true][default='AboutusPageTemplate'];validComponentTypes(code);compTypeGroup(code)
;AboutUsInfo;;;wide

#HRPortal Page Templates
INSERT_UPDATE ContentSlotName;name[unique=true];template(uid,$contentCV)[unique=true][default='HRServicePageTemplate'];validComponentTypes(code);compTypeGroup(code)
;TermsAndConditionInfo;;;wide

#HRPortal Page Templates
INSERT_UPDATE ContentSlotName;name[unique=true];template(uid,$contentCV)[unique=true][default='FinancePortalPageTemplate'];validComponentTypes(code);compTypeGroup(code)
;FinancePortalVideo;;;wide
;FinancePortalExcel;;;wide
;FinanceVerificationUser;;;wide

#HRPortal Page Templates
INSERT_UPDATE ContentSlotName;name[unique=true];template(uid,$contentCV)[unique=true][default='FinanceManagerServicePageTemplate'];validComponentTypes(code);compTypeGroup(code)
;FinanceManagerExcel;;;wide

# Create Content Slots
INSERT_UPDATE ContentSlot;$contentCV[unique=true];uid[unique=true];name;active
;;HeaderInfoSlot;Header Info Slot;true
;;SiteLogoSlot;Default Site Logo Slot;true
;;NavigationBarSlot;Navigation Bar Slot;true
;;HomePageCarouselSlot;HomePage Carousel Slot;true
;;SanedSolutionSlot;Saned Solution Slot;true
;;ValuableClientSlot;Valuable Client Slot;true
;;FooterColumn1Slot;SANED;true
;;FooterColumn2Slot;SANED Services;true
;;FooterColumn3Slot;Contact Us;true
;;FooterColumn4Slot;Follow Us;true
;;CopyRightsSlot;CopyRights Slot;true
;;SanedServicesSolutionSlot;SanedService Solution Slot;true
;;HRPortalVideoSlot;HRPortalVideo Slot;true
;;HRServiceVideoSlot;HRServiceVideo Slot;true
;;HRServiceEmployeeMasterDatasetupSlot;HRServiceEmployeeMasterDatasetup Slot;true
;;HRServicePolicySetupSlot;HRService PolicySetup Slot;true
;;HRServicePayrollProcessSlot;HRService PayrollProcess Slot;true
;;HRServiceRecruitmentSlot;HRService Recruitment Slot;true
;;NewsLetterSlot;NewsLetter Slot;true
;;AboutUsInfoSlot;AboutUsInfo Slot;true
;;TermsAndConditionInfoSlot;TermsAndConditionInfo Slot;true
;;FinancePortalVideoSlot;FinancePortalVideo Slot;true
;;FinancePortalExcelSlot;FinancePortalExcel Slot;true
;;FinanceVerificationUserSlot;FinanceVerificationUser Slot;true
;;FinanceManagerExcelSlot;FinanceManagerExcel Slot;true

INSERT_UPDATE ContentSlotForTemplate;$contentCV[unique=true];uid[unique=true];position[unique=true];pageTemplate(uid,$contentCV)[unique=true][default='OsanedHomePageTemplate'];contentSlot(uid,$contentCV)[unique=true];allowOverwrite
;;HomePageCarousel-OsanedHomePage;HomePageCarousel;;HomePageCarouselSlot;true
;;SanedSolution-OsanedHomePage;SanedSolution;;SanedSolutionSlot;true
;;ValuableClient-OsanedHomePage;ValuableClient;;ValuableClientSlot;true
;;NewsLetter-OsanedHomePage;NewsLetter;;NewsLetterSlot;true

INSERT_UPDATE ContentSlotForTemplate;$contentCV[unique=true];uid[unique=true];position[unique=true];pageTemplate(uid,$contentCV)[unique=true][default='HeaderPageTemplate'];contentSlot(uid,$contentCV)[unique=true];allowOverwrite
;;HeaderInfo-HeaderPage;HeaderInfo;;HeaderInfoSlot;true
;;SiteLogo-HeaderPage;SiteLogo;;SiteLogoSlot;true
;;NavigationBar-HeaderPage;NavigationBar;;NavigationBarSlot;true


INSERT_UPDATE ContentSlotForTemplate;$contentCV[unique=true];uid[unique=true];position[unique=true];pageTemplate(uid,$contentCV)[unique=true][default='FooterPageTemplate'];contentSlot(uid,$contentCV)[unique=true];allowOverwrite
;;FooterColumn1-FooterPage;FooterColumn1;;FooterColumn1Slot;true
;;FooterColumn2-FooterPage;FooterColumn2;;FooterColumn2Slot;true
;;FooterColumn3-FooterPage;FooterColumn3;;FooterColumn3Slot;true
;;FooterColumn4-FooterPage;FooterColumn4;;FooterColumn4Slot;true
;;CopyRights-FooterPage;CopyRights;;CopyRightsSlot;true

INSERT_UPDATE ContentSlotForTemplate;$contentCV[unique=true];uid[unique=true];position[unique=true];pageTemplate(uid,$contentCV)[unique=true][default='SanedServicesPageTemplate'];contentSlot(uid,$contentCV)[unique=true];allowOverwrite
;;SanedServicesSolution-SanedServicesPage;SanedServicesSolution;;SanedServicesSolutionSlot;true

INSERT_UPDATE ContentSlotForTemplate;$contentCV[unique=true];uid[unique=true];position[unique=true];pageTemplate(uid,$contentCV)[unique=true][default='HRPortalPageTemplate'];contentSlot(uid,$contentCV)[unique=true];allowOverwrite
;;HRPortalVideo-HRPortalPage;HRPortalVideo;;HRPortalVideoSlot;true

INSERT_UPDATE ContentSlotForTemplate;$contentCV[unique=true];uid[unique=true];position[unique=true];pageTemplate(uid,$contentCV)[unique=true][default='HRServicePageTemplate'];contentSlot(uid,$contentCV)[unique=true];allowOverwrite
;;HRServiceVideo-HRServicePage;HRServiceVideo;;HRServiceVideoSlot;true
;;HRServiceEmployeeMasterDatasetup-HRServicePage;HRServiceEmployeeMasterDatasetup;;HRServiceEmployeeMasterDatasetupSlot;true
;;HRServicePolicySetup-HRServicePage;HRServicePolicySetup;;HRServicePolicySetupSlot;true
;;HRServicePayrollProcess-HRServicePage;HRServicePayrollProcess;;HRServicePayrollProcessSlot;true
;;HRServiceRecruitment-HRServicePage;HRServiceRecruitment;;HRServiceRecruitmentSlot;true

INSERT_UPDATE ContentSlotForTemplate;$contentCV[unique=true];uid[unique=true];position[unique=true];pageTemplate(uid,$contentCV)[unique=true][default='AboutusPageTemplate'];contentSlot(uid,$contentCV)[unique=true];allowOverwrite
;;AboutUsInfo-AboutUsPage;AboutUsInfo;;AboutUsInfoSlot;true

INSERT_UPDATE ContentSlotForTemplate;$contentCV[unique=true];uid[unique=true];position[unique=true];pageTemplate(uid,$contentCV)[unique=true][default='TermsAndConditionPageTemplate'];contentSlot(uid,$contentCV)[unique=true];allowOverwrite
;;TermsAndConditionInfo-TermsAndConditionPage;TermsAndConditionInfo;;TermsAndConditionInfoSlot;true

INSERT_UPDATE ContentSlotForTemplate;$contentCV[unique=true];uid[unique=true];position[unique=true];pageTemplate(uid,$contentCV)[unique=true][default='FinancePortalPageTemplate'];contentSlot(uid,$contentCV)[unique=true];allowOverwrite
;;FinancePortalVideo-FinancePortalPage;FinancePortalVideo;;FinancePortalVideoSlot;true
;;FinancePortalExcel-FinancePortalPage;FinancePortalExcel;;FinancePortalExcelSlot;true
;;FinanceVerificationUser-FinancePortalPage;FinanceVerificationUser;;FinanceVerificationUserSlot;true

INSERT_UPDATE ContentSlotForTemplate;$contentCV[unique=true];uid[unique=true];position[unique=true];pageTemplate(uid,$contentCV)[unique=true][default='FinanceManagerServicePageTemplate'];contentSlot(uid,$contentCV)[unique=true];allowOverwrite
;;FinanceManagerExcel-financemanagerservicepage;FinanceManagerExcel;;FinanceManagerExcelSlot;true

# Create Content Pages
# Site-wide Homepage
INSERT_UPDATE ContentPage;$contentCV[unique=true];uid[unique=true];name;masterTemplate(uid,$contentCV);label;defaultPage[default='true'];approvalStatus(code)[default='approved'];homepage[default='true']
;;homepage;Home Page;OsanedHomePageTemplate;homepage
;;headerpage;Header Page;HeaderPageTemplate;headerpage
;;footerpage;Footer Page;FooterPageTemplate;footerpage
;;sanedservicespage;SanedServices Page;SanedServicesPageTemplate;sanedservicepage
;;hrportalpage;HR Protal Page;HRPortalPageTemplate;hrportalpage
;;hrservicepage;HR service Page;HRServicePageTemplate;hrservicepage
;;aboutUsPage;AboutUs Page;AboutusPageTemplate;aboutUsPage
;;termsAndConditionPage;TermsAndCondition Page;TermsAndConditionPageTemplate;termsAndConditionPage
;;financeportalpage;Finance Protal Page;FinancePortalPageTemplate;financeportalpage
;;financemanagerservicepage;FinanceManagerService Page;FinanceManagerServicePageTemplate;financemanagerservicepage

