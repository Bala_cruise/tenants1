/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.osaned.initialdata.constants;

/**
 * Global class for all OsanedInitialData constants.
 */
public final class OsanedInitialDataConstants extends GeneratedOsanedInitialDataConstants
{
	public static final String EXTENSIONNAME = "osanedinitialdata";
	public static final String OSANED = "osaned";

	private OsanedInitialDataConstants()
	{
		//empty
	}
}
